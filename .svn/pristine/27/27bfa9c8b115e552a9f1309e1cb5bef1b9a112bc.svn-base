package hci.projects.tagglinglib;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JsonHelper {
	private static final String DEBUG_TAG = "JsonHelper";
	
	// JSON Node names - Data Arrays
	// private static final String TAG_PLAYERS = "players";
	private static final String TAG_EXHIBITS = "exhibits";
	private static final String TAG_TAGLINES = "taglines";
	private static final String TAG_CATEGORIES = "categories";
	
	// JSON Node names - Exhibit
	private static final String TAG_ID = "id";
	private static final String TAG_NAME = "name";
	private static final String TAG_THUMBNAIL = "thumbnail";
	private static final String TAG_SLOTSIZE = "tagSlotsSize";
	private static final String TAG_FLOOR = "floor";
	
	// JSON Node names - Category
	// private static final String TAG_NAME = "name";
	private static final String TAG_COLOR = "color";
	private static final String TAG_DESCRIPTION = "description";
	
	// JSON Node names - Tagline
	// private static final String TAG_NAME = "name";
	private static final String TAG_POINTS = "points";
	private static final String TAG_VALEXHIBIS = "validExhibits";
	private static final String TAG_CATEGORY = "category";
	
	public JsonHelper() {
		super();
	}
	
	public void readExhibits(String json, GameHandler gH) {
		int eSlotSize;
		String eThumbnail;
		int eFloor;
		String eName;
		int eId;
		
		try {
			JSONObject jsonGameObject = new JSONObject(json);
			JSONArray jsonExhibitsArray = jsonGameObject.getJSONArray(TAG_EXHIBITS);
			
			for (int i = 0; i < jsonExhibitsArray.length(); i++) {
				JSONObject jsonExhibitObject = jsonExhibitsArray.getJSONObject(i);
				eId = jsonExhibitObject.getInt(TAG_ID);
				eName = jsonExhibitObject.getString(TAG_NAME);
				eThumbnail = jsonExhibitObject.getString(TAG_THUMBNAIL);
				eSlotSize = jsonExhibitObject.getInt(TAG_SLOTSIZE);
				eFloor = jsonExhibitObject.getInt(TAG_FLOOR);
				gH.addNewExhibit(eId, eName, eThumbnail, eSlotSize, eFloor);
				Log.d("JSON", "Exhibt OK");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.d(DEBUG_TAG, gH.getGameExhibits().toString());
	}
	
	public void readTaglines(String json, GameHandler gH) {
		String tName;
		int tId;
		int tPoints, tCategory;
		Integer[] tValidExhibits;
		
		try {
			JSONObject jsonGameObject = new JSONObject(json);
			JSONArray jsonTaglinesArray = jsonGameObject.getJSONArray(TAG_TAGLINES);
			
			for (int i = 0; i < jsonTaglinesArray.length(); i++) {
				
				JSONObject jsonTaglineObject = jsonTaglinesArray.getJSONObject(i);
				tId = jsonTaglineObject.getInt(TAG_ID);
				tName = jsonTaglineObject.getString(TAG_NAME);
				tPoints = jsonTaglineObject.getInt(TAG_POINTS);
				tCategory = jsonTaglineObject.getInt(TAG_CATEGORY);
				tValidExhibits = getIntArray(jsonTaglineObject, TAG_VALEXHIBIS);
				
				gH.addNewTag(tId, tName, tPoints, tValidExhibits, tCategory);
				Log.d("JSON", "Tagline OK");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.d(DEBUG_TAG, gH.getGameTags().toString());
	}
	
	public void readCategories(String json, GameHandler gH) {
		String cName, cColor, cDescription;
		
		try {
			JSONObject jsonGameObject = new JSONObject(json);
			JSONArray jsonCategoriesArray = jsonGameObject.getJSONArray(TAG_CATEGORIES);
			
			for (int i = 0; i < jsonCategoriesArray.length(); i++) {
				
				JSONObject jsonCategoryObject = jsonCategoriesArray.getJSONObject(i);
				cName = jsonCategoryObject.getString(TAG_NAME);
				cColor = jsonCategoryObject.getString(TAG_COLOR);
				cDescription = jsonCategoryObject.getString(TAG_DESCRIPTION);
				
				gH.addNewCategory(cName, cColor, cDescription);
				Log.d("JSON", "Category OK");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.d(DEBUG_TAG, gH.getGameTagCategories().toString());
	}
	
	public void append(String json, GameHandler gH) {
		// clear game exhibits list if any now
		gH.clear();
		// in this particular order
		readExhibits(json, gH);
		readCategories(json, gH);
		readTaglines(json, gH);
	}
	
	public Integer[] getIntArray(JSONObject jsonObj, String tag) throws JSONException {
		ArrayList<Integer> al = new ArrayList<Integer>();
		// int[] intArray = {};
		JSONArray jArray = jsonObj.getJSONArray(tag);
		for (int k = 0; k < jArray.length(); k++) {
			al.add(k, jArray.getInt(k));
		}
		
		return al.toArray(new Integer[0]);
	}
	
}
