package hci.projects.taggling;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import hci.projects.taggling.client.csmessage.ControlMessage;
import hci.projects.taggling.client.csmessage.GameMessage;
import hci.projects.taggling.client.csmessage.ScanMessage;
import hci.projects.taggling.client.csmessage.queue.CSMessageQueue;
import hci.projects.taggling.client.services.SendToServer;
import hci.projects.taggling.utils.Globals;
import hci.projects.taggling.utils.TGConstants;
import hci.projects.taggling.utils.WakeLocker;
import hci.projects.tagglinglib.GameHandler;
import hci.projects.tagglinglib.Messages.MessageExhibitUpdate;
import hci.projects.tagglinglib.Messages.MessageInitialization;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.util.Log;

/**
 * !-Client Side-!
 * 
 * @category application
 * @author vicky
 */
public class GlobalState extends Application {
	private static GlobalState singleton;
	
	public static CSMessageQueue csMessageQueue = new CSMessageQueue();
	private NewDataFromServerReceiver newDataFromServerReceiver;
	private NewDataFromPlayerReceiver newDataFromPlayerReceiver;
	private int lastId = 0;
	
	// Returns the application instance
	public static GlobalState getInstance() {
		return singleton;
	}
	
	@Override
	public final void onCreate() {
		super.onCreate();
		initialize();
		singleton = this;
		IntentFilter newDataFromServerFilter = new IntentFilter(TGConstants.INTENT_NEW_DATA_FROM_SERVER);
		try {
			unregisterReceiver(newDataFromServerReceiver);
		} catch (IllegalArgumentException e) {
		}
		newDataFromServerReceiver = new NewDataFromServerReceiver();
		registerReceiver(newDataFromServerReceiver, newDataFromServerFilter);
		
		IntentFilter newDataFromPlayerReceiverFilter = new IntentFilter(TGConstants.INTENT_NEW_DATA_FROM_PLAYER);
		try {
			unregisterReceiver(newDataFromPlayerReceiver);
		} catch (IllegalArgumentException e) {
		}
		newDataFromPlayerReceiver = new NewDataFromPlayerReceiver();
		registerReceiver(newDataFromPlayerReceiver, newDataFromPlayerReceiverFilter);
	}
	
	private void initialize() {
		Globals.getInstance().setContext(this);
		// ACRA.init(this);
		super.onCreate();
	}
	
	@Override
	public final void onTerminate() {
		try {
			unregisterReceiver(newDataFromServerReceiver);
			unregisterReceiver(newDataFromPlayerReceiver);
		} catch (IllegalArgumentException e) {
			Log.e(TGConstants.LOG_TAG, "ondestroy()" + e.toString());
		}
	}
	
	public void checkForMissedMessages() {
		NameValuePair[] nameValuePairs = new NameValuePair[2];
		nameValuePairs[0] = new BasicNameValuePair(TGConstants._FIELD_LAST_KNOWN_ID, String.valueOf(lastId));
		nameValuePairs[1] = new BasicNameValuePair(TGConstants._FIELD_USERNAME, GameHandler.getInstance().getMyPlayer().getPlayerName());
		GetLastUpdates getLastUpdates = new GetLastUpdates();
		getLastUpdates.execute(nameValuePairs);
	}
	
	/*------------*/
	// The broadcast Receiver and the action that it has to do
	public class NewDataFromPlayerReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			GameMessage csMessage = intent.getParcelableExtra(TGConstants._TAG_XMPP_OUTBOUND_CSMESSAGE);
			GlobalState.csMessageQueue.pushMessageIntoQueue(csMessage);
			_sendToServer(csMessage.toXMPPMessage());
		}
		
		private void _sendToServer(String outboundData) {
			if ((outboundData != "")) {
				Log.i(TGConstants.LOG_TAG, "xmpp:send:" + outboundData);
				NameValuePair[] nameValuePairs = new NameValuePair[1];
				nameValuePairs[0] = new BasicNameValuePair(TGConstants._TAG_XMPP_OUTBOUND_CSMESSAGE, outboundData);
				SendToServer sendToServer = new SendToServer();
				sendToServer.execute(nameValuePairs);
			}
		}
	}
	
	// The broadcast Receiver and the action that it has to do
	public class NewDataFromServerReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			WakeLocker.acquire(getApplicationContext());

			if (intent.getExtras().getParcelable(TGConstants._GAME_EXHIBITUPDATEMESSAGE) != null) {
				MessageExhibitUpdate exhUpdMsg = intent.getParcelableExtra(TGConstants._GAME_EXHIBITUPDATEMESSAGE);
				GameHandler.getInstance().updateExhibitTagSlots(exhUpdMsg.getExhibitIndex(), exhUpdMsg.getExhibitTagIndexes());
				GameHandler.getInstance().updatePlayerSlots(exhUpdMsg.getExhibitTagIndexes());
				
			} else if (intent.getExtras().getParcelable(TGConstants._GAME_SCANMESSAGE) != null) {
//				ScanMessage gameMessage = intent.getParcelableExtra(TGConstants._GAME_SCANMESSAGE);
//				// Update game model
//				GameHandler.getInstance().markObjectAsFoundByTeamMember(gameMessage.getObjectId());
//				// check if game is finished
//				Intent intentNewScanFromTeamMember = new Intent(TGConstants.INTENT_NEW_SCAN_FROM_TEAMMEMBER);
//				intentNewScanFromTeamMember.putExtra(TGConstants._GAME_SCANMESSAGE, gameMessage);
//				sendBroadcast(intentNewScanFromTeamMember);
			} else if (intent.getExtras().getParcelable(TGConstants._GAME_INITIALIZATIONMESSAGE) != null) {
				MessageInitialization initMsg = 
						new MessageInitialization(intent.getStringExtra(TGConstants._GAME_INITIALIZATIONMESSAGE));
				GameHandler.getInstance().updatePlayerSlots(initMsg.getInventoryIndexes());
				GameHandler.getInstance().initExhibitSlots(initMsg.getExhibitSlotIndexes());
				
			} else if (intent.getExtras().getParcelable(TGConstants._GAME_CONTROLMESSAGE) != null) {
				ControlMessage gameMessage = intent.getParcelableExtra(TGConstants._GAME_CONTROLMESSAGE);
				if (gameMessage.getMessage().equals(ControlMessage.STOPMESSAGE)) {
					Intent intentGameFinished = new Intent(TGConstants.INTENT_GAME_FINISHED);
					sendBroadcast(intentGameFinished);
				} else if (gameMessage.getMessage().equals(ControlMessage.STOPMESSAGE)) {
					Intent intentGameStarted = new Intent(TGConstants.INTENT_GAME_STARTED);
					sendBroadcast(intentGameStarted);
				}
				
			} else if (intent.getExtras().getParcelable(TGConstants._GCM_ON_DELETED_MESSAGES) != null) {
				int total = intent.getParcelableExtra(TGConstants._GCM_ON_DELETED_MESSAGES);
				// Update game model
				checkForMissedMessages();
			}
			WakeLocker.release();
		}
	}
	
	class GetLastUpdates extends AsyncTask<NameValuePair, Void, String> {
		
		private static final String TAG = "GetLastUpdates";
		private static final String REMOTE_REQUEST_FAILED = "RemoteRequestFailed";
		boolean result = false;
		private String response = "";
		
		@Override
		protected String doInBackground(NameValuePair... params) {
			Thread.currentThread().setName("SendToServer");
			try {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(params.length);
				for (NameValuePair nvpair : params) {
					nameValuePairs.add(nvpair);
				}
				
				HttpPost httppost = new HttpPost(TGConstants.SERVER_GET_LAST_UPDATES);
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				// FIXME: timeout
				HttpParams httpParameters = new BasicHttpParams();
				DefaultHttpClient httpclient = new DefaultHttpClient(httpParameters);
				
				HttpResponse responsePost = httpclient.execute(httppost);
				HttpEntity resEntityPost = responsePost.getEntity();
				if (resEntityPost != null) {
					try {
						response = EntityUtils.toString(resEntityPost);
					} catch (Exception e) {
						Log.e(TAG, "3 " + e.toString());
					}
				}
			} catch (ClientProtocolException cpe) {
				response = REMOTE_REQUEST_FAILED;
				Log.e(TAG, "1 " + cpe.toString());
			} catch (IOException ioe) {
				response = REMOTE_REQUEST_FAILED;
				Log.e(TAG, "2 " + ioe.toString());
			}
			return response;
		}
		
		@Override
		protected void onPostExecute(String result) {
			if (result.equals(REMOTE_REQUEST_FAILED)) {
				// Toast.makeText(getApplicationContext(),
				// getString(R.string.xmppclientservice_not_connected),
				// Toast.LENGTH_SHORT).show();
			} else {
				updateGameStatus(result);
			}
		}
		
		public void updateGameStatus(String JSONString) {
			String fromPlayerId;
			int objectId, keyId, action;
			JSONArray array;
			try {
				array = new JSONArray(JSONString);
				for (int i = 0; i < array.length(); i++) {
					JSONObject jsonObject = array.getJSONObject(i);
					GameMessage incomingGameMessage = new GameMessage(jsonObject);
					// JSONObject row = array.getJSONObject(i);
					//
					// fromPlayerId = row.getString("playerId");
					// objectId = row.getInt("objectId");
					// keyId = row.getInt("keyId");
					// action = row.getInt("action");
					// GameMessage gameMessage = new GameMessage(fromPlayerId, objectId, keyId, CSAction.intValue(action));
					GameHandler.getInstance().updateTopic(incomingGameMessage);
				}
				// check if game is finished
				if (GameHandler.getInstance().getTopics().getNumberOfFreeKeys() < 1) {
					Intent intentGameFinished = new Intent(TGConstants.INTENT_GAME_FINISHED);
					sendBroadcast(intentGameFinished);
				}
			} catch (JSONException e) {
				Log.e(TAG, "Couldn't parse JSON string " + e.toString());
			}
		}
	}
	
}