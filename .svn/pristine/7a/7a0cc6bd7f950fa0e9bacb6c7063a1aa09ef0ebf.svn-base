package hci.projects.tagglinglib.Messages;

import hci.projects.taggling.client.csmessage.GameMessage.CSAction;

import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Object for Storing Updating messages and tags
 * <i>Example:</i> 0:0,1,4,15@9,2,5,-1,-1
 * (exhibit index):(new tag indexes)@(new tags indexes in players inventory)
 * 
 * @category utility
 * @author vicky
 */
public class MessageExhibitUpdate implements Parcelable, MessageTaggling {
	private static final String TAG = "MessageExhibitUpdate";
	private int _id = 0;
	private String _fromPlayerId;
	private int _objectId;
	private int _keyId;
	private final MessageType _messageType = MessageType.EXHIBIT_UPDATE;
	private String _fromTeamId;
	
	public static final String PLAYER_SEPARATOR = "@";
	public static final String TAGS_SEPARATOR = ":";
	public static final String COMMA = ",";
	
	private int exhibitIndex;	
	private Integer[] newExhibitTagIndexes;
	private Integer[] newInventoryTagIndexes;
	
	// Constructors
	public MessageExhibitUpdate(String message) {
		// exhibitIndex
		String[] splitMsg = message.split(PLAYER_SEPARATOR);
		String[] exhibit = splitMsg[0].split(TAGS_SEPARATOR);
		exhibitIndex = Integer.valueOf(exhibit[0]);
		// exhibit tag indexes
		splitMsg = message.split("\\" + PLAYER_SEPARATOR);
		Log.d(getClass().getName(), "getExhibitIndexes: " + Arrays.toString(splitMsg));
		int iSep = splitMsg[0].indexOf(TAGS_SEPARATOR);
		String str = splitMsg[0].substring(iSep + 1);
		String[] strArr = str.split(COMMA);
		Integer[] lIndxs = new Integer[strArr.length];
		int i = 0;
		for (String s : strArr) {
			lIndxs[i] = Integer.valueOf(s);
			i++;
		}
		this.newExhibitTagIndexes = lIndxs;
		// inventoryIndexs
		splitMsg = message.split(PLAYER_SEPARATOR);
		strArr = splitMsg[1].split(COMMA);
		Integer[] lIventoryIndxs = new Integer[strArr.length];
		i = 0;
		for (String s : strArr) {
			lIventoryIndxs[i] = Integer.valueOf(s);
			i++;
		}
		this.newInventoryTagIndexes = lIventoryIndxs;
	}
	
	// Constructors
	public MessageExhibitUpdate(JSONObject jsonObject) {
		try {
			_id = jsonObject.getInt("messageId");
			_fromPlayerId = jsonObject.getString("playerId");
			_objectId = jsonObject.getInt("objectId");
			newExhibitTagIndexes = getIntArray(jsonObject, "newIds");
			newInventoryTagIndexes = getIntArray(jsonObject, "newIdsInInventory");
			
//			ArrayList<JSONObject> jaNewIds = parseJSONArray(jsonObject.getJSONArray("newIds"));
//			ArrayList<JSONObject> jaNewIdsInInventory = parseJSONArray(jsonObject.getJSONArray("newIdsInInventory"));
			// Finally
//			JSONObject[] jsons = new JSONObject[arrays.size()];
//			arrays.toArray(jsons);		
		
		} catch (JSONException e) {
			Log.e(TAG, "Couldn't parse JSON string " + e.toString());
		}
		
	}
	
	// I copied from JsonHelper -viky
	public Integer[] getIntArray(JSONObject jsonObj, String tag) throws JSONException {
		ArrayList<Integer> al = new ArrayList<Integer>();
		// int[] intArray = {};
		JSONArray jArray = jsonObj.getJSONArray(tag);
		for (int k = 0; k < jArray.length(); k++) {
			al.add(k, jArray.getInt(k));
		}
		
		return al.toArray(new Integer[0]);
	}
	
	/* Not needed -viky
	 * 
	private ArrayList<JSONObject> parseJSONArray(JSONArray jsonArray) throws JSONException {
		int size = jsonArray.length();
		ArrayList<JSONObject> arrayList = new ArrayList<JSONObject>();
		for (int i = 0; i < size; i++) {
			JSONObject another_json_object = jsonArray.getJSONObject(i);
			arrayList.add(another_json_object);
		}
		return arrayList;
		// Finally
//		JSONObject[] jsons = new JSONObject[arrayList.size()];
//		arrayList.toArray(jsons);
	}
	*/
	
	public MessageExhibitUpdate(int exhibitIndex, Integer[] indxs, Integer[] inventoryIndxs) {
		this.exhibitIndex = exhibitIndex;
		this.newExhibitTagIndexes = indxs;
		this.newInventoryTagIndexes = inventoryIndxs;
	}
	
	public MessageExhibitUpdate(Parcel in) {
		this(in.readString());
	}
	
	// Getter
	public int getExhibitIndex() {
		return exhibitIndex;
	}
	
	public Integer[] getExhibitTagIndexes() {
		return newExhibitTagIndexes;
	}
	
	public Integer[] getInventoryTagIndexes() {
		return newInventoryTagIndexes;
	}
	
	// setter
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(exhibitIndex);
		
		sb.append(TAGS_SEPARATOR);
		for (int i : newExhibitTagIndexes) {
			sb.append(i);
			sb.append(COMMA);
		}
		sb.deleteCharAt(sb.length() - 1);
		
		sb.append(PLAYER_SEPARATOR);
		for (int i : newInventoryTagIndexes) {
			sb.append(i);
			sb.append(COMMA);
		}
		sb.deleteCharAt(sb.length() - 1);
		
		return sb.toString();
	}
	
	public static final Parcelable.Creator<MessageExhibitUpdate> CREATOR = new Parcelable.Creator<MessageExhibitUpdate>() {
		public MessageExhibitUpdate createFromParcel(Parcel in) {
			return new MessageExhibitUpdate(in);
		}
		
		public MessageExhibitUpdate[] newArray(int size) {
			return new MessageExhibitUpdate[size];
		}
	};
	
	@Override
	public int describeContents() {
		return this.toString().hashCode();
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.toString());
	}

	@Override
	public String getPlayerId() {
		return this.getPlayerId();
	}

	@Override
	public MessageType getMessageType() {
		return this._messageType;
	}
}
