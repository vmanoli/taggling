package hci.projects.taggling.client;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import hci.projects.tagglinglib.GameHandler;
import hci.projects.tagglinglib.LoadData;
import hci.projects.tagglinglib.Messages.MessageExhibitUpdate;
import hci.projects.tagglinglib.Messages.MessageInitialization;
import hci.projects.tagglinglib.Messages.Messages;
import hci.projects.taggling.GlobalState;
import hci.projects.taggling.Adapters.CategoryListAdapter;
import hci.projects.taggling.Adapters.PointsTableListAdapter;
import hci.projects.taggling.Adapters.TagsListAdapter;
import hci.projects.taggling.Adapters.CategoryListAdapter.Options;
import hci.projects.tagglinglib.GameElements.Exhibit;
import hci.projects.taggling.ListLoaders.ExhibitsListLoader;
import hci.projects.taggling.ListLoaders.TagsListLoader;
import hci.projects.taggling.client.csmessage.GameMessage;
import hci.projects.taggling.client.services.SendToServer;
import hci.projects.taggling.utils.TGConstants;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends TGActivity implements ExhibitsListLoader.OnSelectedListener, PromptScanDialog.PromptScanDialogListener,
		FragmentGameOver.Listener, TagsListLoader.Listener {
	
	// GlobalState state;
	// private Handler mHandler = new Handler();
	
	// Fragments
	// FragmentLogin mLoginFragment;
	FragmentGameOver mGameOverFragment;
	ScreenWait mWaitScreen;
	
	// tags for debug logging
	final boolean ENABLE_DEBUG = true;
	final static String TAG = "TGClient";
	final static String MSG_TAG = "MessageOfClient";
	final static String LOGIC_TAG = "Taggling GameLogic";
	
	// Request codes
	final static int RC_WAITING_ROOM = 10001;
	final static int NO_SPECIFIC_EXHIBIT_SELECTED = 10002;
	final static int SPECIFIC_EXHIBIT_SELECTED = 10003;
	
	// COMMUNICATION SERVICE
	//
	// Minimum number of players for the waiting room to dismiss. Including me.
	final int MIN_PLAYERS = 0;
	// Min and max number of auto match players in the room
	final int MIN_OPPONENTS = 1, MAX_OPPONENTS = 7;
	
	ByteArrayOutputStream mMsgBuf = new ByteArrayOutputStream();
	
	// STATE VARIANTS
	//
	// The game has finished?
	public boolean isGameOver = false;
	public boolean offlinePlay = false;
	public boolean gameDataLoaded = false;
	// important! Helps not to disconnect when stopping
	// activity to scan qr codes
	public boolean explicitExit = true;
	
	// version numbers
	// int mCurVersion = -1, sCurVersion = -1;
	
	// GAME Variants
	//
	public String selected;
	
	// Player
	
	public String mPlayerName = "";
	
	// UI - ProgressBar
	ProgressBar progressBar;
	
	// UI - Screens
	public enum Screen {
		LOGIN, ENTER, GAMEPLAY, TAGSLIST, GAMEOVER, WAIT
	}
	
	/*
	 * * * * * * * * * * * * * * * * * * *
	 * ACTIVITY Callback Methods
	 * * * * * * * * * * * * * * * * * * *
	 */
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// create fragments
		mGameOverFragment = new FragmentGameOver();
		mWaitScreen = new ScreenWait();
		
		// listen to fragment events
		mGameOverFragment.setListener(this);
		
		switchToScreen(Screen.GAMEPLAY);
	}
	
	// Activity is going to the background. We have to leave the current room.
	@Override
	public void onStop() {
		Log.d(TAG, "**** got onStop");
		stopKeepingScreenOn();
		
	}
	
	// Activity just got to the foreground.
	@Override
	public void onStart() {
		// switchToScreen(R.id.screen_wait);
		keepScreenOn();
		// state = ((GlobalState) getApplicationContext());
		// GameHandler.getInstance().getResources(getResources());
		// mPlayerName = getString(R.string.player_noname);
		
		super.onStart();
	}
	
	/*
	 * @Override
	 * protected void onPostCreate(Bundle savedInstanceState) {
	 * super.onPostCreate(savedInstanceState);
	 * // Sync the toggle state after onRestoreInstanceState has occurred.
	 * mDrawerToggle.syncState();
	 * }
	 */
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.entry, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem score = menu.findItem(R.id.action_points);
		score.setTitle(Integer.toString(GameHandler.getInstance().getPlayer().getPlayerTotalPoints()));
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		AlertDialog dialog;
		switch (item.getItemId()) {
		case R.id.scan:
			openScanner(NO_SPECIFIC_EXHIBIT_SELECTED);
			return true;
		case R.id.action_inventory:
			dialog = showInventory();
			dialog.show();
			return true;
		case R.id.action_help:
			dialog = showHelp();
			dialog.show();
			return true;
		case R.id.action_points:
			dialog = showPointsTable();
			dialog.show();
			return true;
		case android.R.id.home:
			switchToScreen(Screen.LOGIN);
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		String qrCodeValue;
		switch (requestCode) {
		case NO_SPECIFIC_EXHIBIT_SELECTED:
			if (resultCode == RESULT_OK) {
				// Determine the selected exhibit
				qrCodeValue = intent.getStringExtra("SCAN_RESULT");
				for (Exhibit exhibit : GameHandler.getInstance().getGameExhibits()) {
					if (exhibit.isExhibitHexCode(qrCodeValue)) {
						if (exhibit.isLocked()) {
							Toast.makeText(this, R.string.qr_locked, Toast.LENGTH_LONG).show();
						} else {
							openTagsFragment(exhibit.getExhibitName());
							setMessageBuffer(Messages.LOCKED_EXHIBIT, qrCodeValue);
							broadcastToAll();
						}
						return;
					}
				}
				Toast.makeText(this, R.string.qr_not_accepted, Toast.LENGTH_LONG).show();
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.qr_failed, Toast.LENGTH_SHORT).show();
			}
			break;
		case SPECIFIC_EXHIBIT_SELECTED:
			if (resultCode == RESULT_OK) {
				// Determine the selected exhibit
				if (selected.equals(null)) {
					Toast.makeText(this, "No name selected", Toast.LENGTH_LONG).show();
					return;
				}
				Exhibit exhibit = GameHandler.getInstance().getExhibit(selected);
				qrCodeValue = intent.getStringExtra("SCAN_RESULT");
				if (exhibit.isExhibitHexCode(qrCodeValue)) {
					if (exhibit.isLocked()) {
						Toast.makeText(this, R.string.qr_locked, Toast.LENGTH_LONG).show();
					} else {
						openTagsFragment(exhibit.getExhibitName());
						setMessageBuffer(Messages.LOCKED_EXHIBIT, qrCodeValue);
						broadcastToAll();
					}
					return;
				}
				Toast.makeText(this, R.string.qr_not_correct, Toast.LENGTH_LONG).show();
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.qr_failed, Toast.LENGTH_SHORT).show();
			}
			break;
		default:
			super.onActivityResult(requestCode, resultCode, intent);
		}
	}
	
	/*
	 * * * * * * * * * * * * * * * * * * *
	 * COMMUNICATION Callback Methods
	 * * * * * * * * * * * * * * * * * * *
	 */
	// @Override
	// public void onRealTimeMessageReceived(RealTimeMessage rtm) {
	//
	// byte[] buf = rtm.getMessageData();
	// String msgSender = rtm.getSenderParticipantId();
	// String originalMsgStr;
	//
	// Log.d(MSG_TAG, "Message received: " + Messages.toString(buf[0]));
	//
	// switch (buf[0]) {
	//
	// case Messages.INITIALIZATION_OBJECT:
	// originalMsgStr = getOriginalMessageString(buf);
	// Log.d(LOGIC_TAG, originalMsgStr);
	// MessageInitialization initMsg = new MessageInitialization(originalMsgStr);
	// GameHandler.getInstance().clearAllSlots();
	// GameHandler.getInstance().initExhibitSlots(initMsg.getExhibitSlotIndexes());
	// GameHandler.getInstance().updatePlayerSlots(initMsg.getInventoryIndexes());
	// setMessageBuffer(Messages.READY_TO_BEGIN, "");
	// broadcastToMaster();
	// break;
	// case Messages.LOCKED_EXHIBIT:
	// originalMsgStr = getOriginalMessageString(buf);
	// Log.d(MSG_TAG, "Locking exhibit: " + originalMsgStr);
	// // set the exhibit Locked state
	// GameHandler.getInstance().getExhibitByHex(originalMsgStr).setLocked(true);
	// break;
	//
	// case Messages.GAME_OVER_ALL:
	// gameOver();
	// break;
	// default:
	// Log.d(MSG_TAG, "Message NOT recognized");
	// break;
	// }
	// }
	
	public void broadcastToAll() {
		// Send to every other participant.
		for (Participant p : mParticipants) {
			if (p.getParticipantId().equals(mMyId))
				continue;
			if (p.getStatus() != Participant.STATUS_JOINED)
				continue;
			Games.RealTimeMultiplayer.sendReliableMessage(getApiClient(), null, mMsgBuf.toByteArray(), mRoomId, p.getParticipantId());
		}
		Log.d(MSG_TAG, "...sent");
	}
	
	public void broadcastToMaster() {
		if (mGameMasterId == null) {
			Log.d(TAG, "No game master declared yet!");
			return;
		}
		Games.RealTimeMultiplayer.sendReliableMessage(getApiClient(), null, mMsgBuf.toByteArray(), mRoomId, mGameMasterId);
		
		Log.d(MSG_TAG, "...sent");
	}
	
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UI Listener Methods - Login Fragment
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UI Listener Methods - Exhibits List Loader
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	@Override
	public void onExhibitSelected(String selectedExhibitName) {
		selected = selectedExhibitName;
		
		if (isGameOver == false && GameHandler.getInstance().getExhibit(selectedExhibitName).isCompleted() == false) {
			PromptScanDialog prompt = new PromptScanDialog();
			
			Bundle args = new Bundle();
			args.putInt("DIALOG_THUMBNAIL", (GameHandler.getInstance().getExhibit(selectedExhibitName)).getExhibitThumbnail(getBaseContext()));
			args.putString("DIALOG_NAME", (GameHandler.getInstance().getExhibit(selectedExhibitName)).getExhibitName());
			prompt.setArguments(args);
			prompt.show(getFragmentManager(), null);
		} else {
			if (!GameHandler.getInstance().getExhibit(selectedExhibitName).isLocked()) {
				openTagsFragment(selectedExhibitName);
				setMessageBuffer(Messages.LOCKED_EXHIBIT, GameHandler.getInstance().getExhibit(selectedExhibitName).toHex(selectedExhibitName));
				broadcastToAll();
			} else {
				informUnreadable();
			}
		}
	}
	
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UI Listener Methods - PromptDialog
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	/**
	 * @category listener UI
	 */
	@Override
	public void onDialogPositiveClick() {
		// User touched the dialog's positive button
		openScanner(SPECIFIC_EXHIBIT_SELECTED);
		return;
	}
	
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UI Listener Methods - Tags List
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	public void onTagsListDismissed(MessageExhibitUpdate updateMsg) {
		WaitForResponseTask waitForResponseTask = new WaitForResponseTask(updateMsg);
		waitForResponseTask.execute(updateMsg);
		
		Log.d(MSG_TAG, updateMsg.toString());
		// FIXME edo prepei na ginetai apostoli tou MessageExhibitUpdate ston server
		setMessageBuffer(Messages.UPDATING_EXHIBIT, updateMsg.toString());
		broadcastToAll();
		switchToScreen(Screen.GAMEPLAY); // switch to main screen
	}
	
	public void guessingGameOver(MessageExhibitUpdate updateMsg) {
		// FIXME edo prepei na ginetai apostoli tou MessageExhibitUpdate ston server
		setMessageBuffer(Messages.UPDATING_EXHIBIT, updateMsg.toString());
		broadcastToAll();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		setMessageBuffer(Messages.GUESSING_GAME_OVER, "");
		broadcastToAll();
		switchToScreen(Screen.GAMEPLAY);
	}
	
	public enum MessageDeliveryResult {
		SUCCEEDED, FAILED, CANCELED;
	};
	
	private boolean hasReturned(MessageExhibitUpdate csmessage) {
		boolean ret = GlobalState.csMessageQueue.hasReturned(csmessage);
		
		return ret;
	}
	
	class WaitForResponseTask extends AsyncTask<MessageExhibitUpdate, Void, MessageDeliveryResult> {
		private ProgressDialog progressDialog;
		boolean running = true;
		int timer = 0;
		final int _TRY_INTERVAL = 250;
		private MessageExhibitUpdate outgoingMessage = null;
		
		public WaitForResponseTask(final MessageExhibitUpdate csMessage) {
			super();
			outgoingMessage = csMessage;
			progressDialog = ProgressDialog.show(MainActivity.this, getString(R.string.mainactivity_please_wait),
					getString(R.string.mainactivity_sending_request), false);
			progressDialog.setCancelable(true);
			progressDialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					GlobalState.csMessageQueue.cancelRequest(csMessage);
				}
			});
		}
		
		@Override
		protected void onPreExecute() {
			progressDialog.show();
			GlobalState.csMessageQueue.pushMessageIntoQueue(outgoingMessage);
			NameValuePair[] nameValuePairs = new NameValuePair[1];
			nameValuePairs[0] = new BasicNameValuePair(TGConstants._TAG_XMPP_OUTBOUND_CSMESSAGE, outgoingMessage.toXMPPMessage());
			SendToServer sendToServer = new SendToServer();
			sendToServer.execute(nameValuePairs);
		}
		
		@Override
		protected MessageDeliveryResult doInBackground(MessageExhibitUpdate... params) {
			MessageDeliveryResult messageDeliveryResult = MessageDeliveryResult.FAILED;
			
			try {
				while (running) {
					timer += _TRY_INTERVAL;
					if (timer > TGConstants._SEND_REQUEST_TIMEOUT) {
						running = false;
						csApplication.checkForMissedMessages();
						messageDeliveryResult = MessageDeliveryResult.FAILED;
						Log.i(TGConstants.LOG_TAG, "TopicActivity:timer:message delivery failed " + params[0].toString());
						
					} else if (hasReturned(params[0])) {
						running = false;
						messageDeliveryResult = MessageDeliveryResult.SUCCEEDED;
						Log.i(TGConstants.LOG_TAG, "TopicActivity:timer:message delivery succeeded" + params[0].toString());
					}
					Thread.sleep(_TRY_INTERVAL);
				}
			} catch (InterruptedException e) {
				Log.e(TAG, e.toString());
			}
			return messageDeliveryResult;
		}
		
		@Override
		protected void onPostExecute(MessageDeliveryResult messageDeliveryResult) {
			progressDialog.dismiss();
			if (messageDeliveryResult != MessageDeliveryResult.SUCCEEDED) {
				Toast.makeText(MainActivity.this, "Message failed", Toast.LENGTH_SHORT).show();//FIXME
			}
		}
		
		@Override
		protected void onCancelled() {
			running = false;
		}
	}
	
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UI Listener Methods - Connection Management
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UI Listener Methods - Game Over Screen
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	@Override
	public void onOfflineButtonClicked() {
		offlinePlay = true;
		switchToScreen(Screen.GAMEPLAY);
		Log.d(TAG, "Offline Button Clicked");
	}
	
	/**
	 * Retrieve the json string from the server
	 * 
	 * @param The
	 *                file url
	 * @return json string representation
	 */
	public String fetchJsonString(String url) {
		
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				Log.e("NET_ERR", "Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}
	
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UTILITY Methods
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	
	public void switchToScreen(Screen screen) {
		switch (screen) {
		case WAIT:
			commitFragmentTransaction(mWaitScreen);
			break;
		case GAMEOVER:
			commitFragmentTransaction(mGameOverFragment);
			break;
		case LOGIN:
			commitFragmentTransaction(mLoginFragment);
		case GAMEPLAY:
			getActionBar().show();
			// this.lockDrawerClosed(false);
			Fragment tabsFrag = new TabsFragmentExhibits();
			commitFragmentTransaction(tabsFrag);
		default:
			break;
		}
	}
	
	private void openTagsFragment(String selectedExhibitName) {
		// Create fragment and give it an argument for the selected exhibit
		//
		TagsListLoader tagListFrg = new TagsListLoader();
		
		Bundle args = new Bundle();
		args.putString(TagsListLoader.PRESSED_EXHIBIT, selectedExhibitName);
		tagListFrg.setArguments(args);
		
		commitFragmentTransaction(tagListFrg);
	}
	
	/**
	 * Helper method to commit fragment transactions in general
	 * 
	 * @param frag
	 *                The fragment to switch to
	 */
	public void commitFragmentTransaction(Fragment frag) {
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.fragment_container, frag); // in main activity
		// // Saved the day
		// // not keeping the tags fragment to backstack
		// // because you can only go there by scanning
		// if (fragmentManager.findFragmentByTag("TagsFragment") == null) {
		// transaction.addToBackStack(null);
		// }
		// transaction.addToBackStack(null);
		transaction.commitAllowingStateLoss();
	}
	
	void keepScreenOn() {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
	
	// Clears the flag that keeps the screen on.
	void stopKeepingScreenOn() {
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
	
	@Override
	public void onBackPressed() {
	}
	
	public void gameStart() {
		isGameOver = false;
		// offlinePlay = false;
		getActionBar().show();
		// this.lockDrawerClosed(false);
		// selectItem(0); // Go to Exhibits list
		switchToScreen(Screen.GAMEPLAY);
		Log.d(TAG, "Let the Game begin!");
	}
	
	public void gameOver() {
		isGameOver = true;
		
	}
	
	public AlertDialog showCategories() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle(R.string.frag_name_categories);
		alertDialogBuilder.setAdapter(
				new CategoryListAdapter(this, R.layout.category_list_item, GameHandler.getInstance().getGameTagCategories(),
						Options.DONT_SHOW_DESCRIPTION), null).setNegativeButton(R.string.go_back,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		return alertDialogBuilder.create();
	}
	
	public AlertDialog showInventory() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle(R.string.frag_name_inventory);
		alertDialogBuilder.setAdapter(new TagsListAdapter(this, R.layout.tags_list_item, GameHandler.getInstance().getPlayer().inventory), null)
				.setNegativeButton(R.string.go_back, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		return alertDialogBuilder.create();
	}
	
	public AlertDialog showHelp() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle(R.string.action_show_info);
		
		LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
		View mRoot = layoutInflater.inflate(R.layout.help_screen, null);
		
		alertDialogBuilder.setView(mRoot);
		alertDialogBuilder.setNegativeButton(R.string.go_back, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		return alertDialogBuilder.create();
	}
	
	public AlertDialog showPointsTable() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle(R.string.tab_points);
		alertDialogBuilder.setAdapter(
				new PointsTableListAdapter(this, R.layout.pointstable_list_item, GameHandler.getInstance().getPlayer().pointTable), null)
				.setNegativeButton(R.string.go_back, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		return alertDialogBuilder.create();
	}
	
	protected void openScanner(int reqCode) {
		try {
			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
			startActivityForResult(intent, reqCode);
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this, "ERROR:" + e, Toast.LENGTH_SHORT).show();
		}
	}
	
	public void informUnreadable() {
		Toast.makeText(this, "This Exhibit is Viewed by someone else right now. Try another", Toast.LENGTH_SHORT).show();
	}
	
}
