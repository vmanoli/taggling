package hci.projects.tagglinglib.Messages;

import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Object for Storing Updating messages and tags
 * <i>Example:</i> 0:0,1,4,15@9,2,5,-1,-1
 * (exhibit index):(new tag indexes)@(new tags indexes in players inventory)
 * 
 * @category utility
 * @author vicky
 */
public class MessageExhibitUpdate implements Parcelable, MessageTaggling {
	private static final String TAG = "UpdateMessage";
	
	private int _fromPlayerId;
	private int _objectId;
	private int _sessionId = 0;
	private final MessageType _messageType = MessageType.EXHIBIT_UPDATE;
	private Integer[] newExhibitTagIndexes;
	private Integer[] newInventoryTagIndexes;
	
	/* Not used any more
	 * older version of messages
	public static final String PLAYER_SEPARATOR = "@";
	public static final String TAGS_SEPARATOR = ":";
	public static final String COMMA = ",";
	*/
	
	// Constructors
	public MessageExhibitUpdate(String message) {
		// exhibitIndex
		String[] splitMsg = message.split(PLAYER_SEPARATOR);
		String[] exhibit = splitMsg[0].split(TAGS_SEPARATOR);
		_objectId = Integer.valueOf(exhibit[0]);
		// exhibit tag indexes
		splitMsg = message.split("\\" + PLAYER_SEPARATOR);
		Log.d(getClass().getName(), "getExhibitIndexes: " + Arrays.toString(splitMsg));
		int iSep = splitMsg[0].indexOf(TAGS_SEPARATOR);
		String str = splitMsg[0].substring(iSep + 1);
		String[] strArr = str.split(COMMA);
		Integer[] lIndxs = new Integer[strArr.length];
		int i = 0;
		for (String s : strArr) {
			lIndxs[i] = Integer.valueOf(s);
			i++;
		}
		this.newExhibitTagIndexes = lIndxs;
		// inventoryIndexs
		splitMsg = message.split(PLAYER_SEPARATOR);
		strArr = splitMsg[1].split(COMMA);
		Integer[] lIventoryIndxs = new Integer[strArr.length];
		i = 0;
		for (String s : strArr) {
			lIventoryIndxs[i] = Integer.valueOf(s);
			i++;
		}
		this.newInventoryTagIndexes = lIventoryIndxs;
	}
	
	// Constructors
	public MessageExhibitUpdate(JSONObject jsonObject) {
		try {
			_fromPlayerId = jsonObject.getInt(MessageTaggling.LABEL_MSG_PLAYERID);
			_objectId = jsonObject.getInt(MessageTaggling.LABEL_MSGK_OBJECTID);
			newExhibitTagIndexes = getIntArray(jsonObject, MessageTaggling.LABEL_MSG_NEW_TAGIDS_IN_EXHIBIT);
			newInventoryTagIndexes = getIntArray(jsonObject, MessageTaggling.LABEL_MSG_NEW_TAGIDS_IN_INVENTORY);
			
			// ArrayList<JSONObject> jaNewIds = parseJSONArray(jsonObject.getJSONArray("newIds"));
			// ArrayList<JSONObject> jaNewIdsInInventory = parseJSONArray(jsonObject.getJSONArray("newIdsInInventory"));
			// Finally
			// JSONObject[] jsons = new JSONObject[arrays.size()];
			// arrays.toArray(jsons);
			
		} catch (JSONException e) {
			Log.e(TAG, "Couldn't parse JSON string " + e.toString());
		}
		
	}
	
	// I copied from JsonHelper -viky
	public Integer[] getIntArray(JSONObject jsonObj, String tag) throws JSONException {
		ArrayList<Integer> al = new ArrayList<Integer>();
		// int[] intArray = {};
		JSONArray jArray = jsonObj.getJSONArray(tag);
		for (int k = 0; k < jArray.length(); k++) {
			al.add(k, jArray.getInt(k));
		}
		
		return al.toArray(new Integer[0]);
	}
	
	public JSONArray getJSONArray(Integer[] intArray) throws JSONException {
		
		JSONArray jArray = new JSONArray();
		for (int k = 0; k < intArray.length; k++) {
			jArray.put(intArray[k]);
		}
		
		return jArray;
	}
	
	/*
	 * Not needed -viky
	 * private ArrayList<JSONObject> parseJSONArray(JSONArray jsonArray) throws JSONException {
	 * int size = jsonArray.length();
	 * ArrayList<JSONObject> arrayList = new ArrayList<JSONObject>();
	 * for (int i = 0; i < size; i++) {
	 * JSONObject another_json_object = jsonArray.getJSONObject(i);
	 * arrayList.add(another_json_object);
	 * }
	 * return arrayList;
	 * // Finally
	 * // JSONObject[] jsons = new JSONObject[arrayList.size()];
	 * // arrayList.toArray(jsons);
	 * }
	 */
	
	public MessageExhibitUpdate(int _fromPlayerId, int objectId, Integer[] indxs, Integer[] inventoryIndxs) {
		this._fromPlayerId = _fromPlayerId;
		this._objectId = objectId;
		this.newExhibitTagIndexes = indxs;
		this.newInventoryTagIndexes = inventoryIndxs;
	}
	
	public MessageExhibitUpdate(Parcel in) {
		this(in.readString());
	}
	
	// Getter
	public int getObjectId() {
		return _objectId;
	}
	
	public Integer[] getExhibitTagIndexes() {
		return newExhibitTagIndexes;
	}
	
	public Integer[] getInventoryTagIndexes() {
		return newInventoryTagIndexes;
	}
	
	// setter
	public String toString() {
		JSONObject object = new JSONObject();
		try {
			object.put(MessageTaggling.LABEL_MSG_PLAYERID, _fromPlayerId);
			object.put(MessageTaggling.LABEL_MSG_ACTION, String.valueOf(_messageType));
			object.put(MessageTaggling.LABEL_MSGK_OBJECTID, String.valueOf(_objectId));
			object.put(MessageTaggling.LABEL_MSG_SESSIONID, String.valueOf(_sessionId));
			object.put(MessageTaggling.LABEL_MSG_NEW_TAGIDS_IN_EXHIBIT, getJSONArray(newExhibitTagIndexes));
			object.put(MessageTaggling.LABEL_MSG_NEW_TAGIDS_IN_INVENTORY, getJSONArray(newInventoryTagIndexes));
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		System.out.println(object);
		return object.toString();
		
		/* older message type staff

		StringBuilder sb = new StringBuilder();
		
		sb.append(_objectId);
		
		sb.append(TAGS_SEPARATOR);
		for (int i : newExhibitTagIndexes) {
			sb.append(i);
			sb.append(COMMA);
		}
		sb.deleteCharAt(sb.length() - 1);
		
		sb.append(PLAYER_SEPARATOR);
		for (int i : newInventoryTagIndexes) {
			sb.append(i);
			sb.append(COMMA);
		}
		sb.deleteCharAt(sb.length() - 1);
		
		return sb.toString();
		*/
	}
	
	public static final Parcelable.Creator<MessageExhibitUpdate> CREATOR = new Parcelable.Creator<MessageExhibitUpdate>() {
		public MessageExhibitUpdate createFromParcel(Parcel in) {
			return new MessageExhibitUpdate(in);
		}
		
		public MessageExhibitUpdate[] newArray(int size) {
			return new MessageExhibitUpdate[size];
		}
	};
	
	@Override
	public int describeContents() {
		return this.toString().hashCode();
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.toString());
	}
	
	@Override
	public int getPlayerId() {
		return this.getPlayerId();
	}
	
	@Override
	public MessageType getMessageType() {
		return this._messageType;
	}
	
	@Override
	public int getMessageKey() {
		return this.getObjectId();
	}
	
	@Override
	public int getSessionId() {
		return _sessionId;
	}
}
