package hci.projects.tagglinglib.Messages;

import hci.projects.tagglinglib.GameHandler;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * @category all
 *           Example:
 *           int[][] multi = new int[][]{
 *           { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
 *           { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
 *           { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
 *           { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
 *           { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
 *           };
 *           FORMAT: (Exhibit Slots Indexes)@(Player Inventory Indexes)
 *           Exhibit Slots Indexes: (Exhibit one)|(Exhibit two)|...
 *           Exhibit One: (Exhibit Index):(Tag Index one),(Tag Index two),...
 */
public class MessageInitialization implements Parcelable {
	public final static String TAG = "MessageInitialization";
	// public static final String PLAYER_SEPARATOR = "@";
	// public static final String EXHIBIT_SEPARATOR = "|";
	// public static final String TAG_SEPARATOR = ":";
	// public static final String COMMA = ",";
	//
	// public static final String ESCAPE = "\\";
	
	public static final String LABEL_MSG_SESSIONID = "sessionId";
	public static final String LABEL_MSG_INIT_TAGIDS_IN_EXHIBIT = "newExhibitIds";
	public static final String LABEL_MSG_INIT_TAGIDS_IN_INVENTORY = "newIdsInInventory";
	public static final String LABEL_MSG_INIT_OBJECTID = "objectId";
	public static final String LABEL_MSG_INIT_PLAYER = "playerId";
	public static final String LABEL_MSG_INIT_TAGIDS = "tagIds";
	
	// private int _sessionId;
	
	private HashMap<Integer, ArrayList<Integer>> initIndexes;
	private Integer[] myPlayerInitialTags;
	
	private String message;
	
	//
	// Constructors
	//
	
	public MessageInitialization(Parcel in) throws JSONException {
		this(new JSONObject(in.readString()));
	}
	
	// public MessageInitialization(String message) {
	// this.message = message;
	//
	// // Split Message into a 2D array of integers
	// //
	// String[] splitMsg = message.split(ESCAPE+PLAYER_SEPARATOR);
	//
	// String[] strArr = splitMsg[0].split(ESCAPE+EXHIBIT_SEPARATOR);
	// // 1
	// Log.d(TAG, Arrays.toString(strArr));
	//
	// Integer[][] multiD = new Integer[strArr.length][];
	// // 2
	// Log.d(TAG, Arrays.deepToString(multiD));
	//
	// for (String s : strArr) {
	// String[] exhArr = s.split(TAG_SEPARATOR);
	// // 3
	// Log.d(TAG, Arrays.toString(exhArr));
	//
	// int eIndex = Integer.valueOf(exhArr[0]);
	// // 4
	// Log.d(TAG, exhArr[0]);
	//
	// String[] tagArr = exhArr[1].split(COMMA);
	// multiD[eIndex] = new Integer[ tagArr.length ];
	// int i=0;
	// for (String si : tagArr) {
	// multiD[eIndex][i] = Integer.valueOf(si);
	// i++;
	// }
	// }
	// Log.d(TAG, Arrays.deepToString(multiD));
	// initIndexes = multiD;
	//
	// // Get player initial tags
	// //
	// String[] tagArr = splitMsg[1].split(COMMA);
	// Integer[] invIndxs = new Integer[tagArr.length];
	// int i=0;
	// for (String si : tagArr) {
	// invIndxs[i] = Integer.valueOf(si);
	// i++;
	// }
	// myPlayerInitialTags = invIndxs;
	// }
	
	public MessageInitialization(JSONObject jsonObject) {
		/*
		 * Example json
		 * { "sessionId" : 0,
		 * "newExhibitIds" : [
		 * { "objectId" : 3000, "tagIds" : [1, 4, 8] },
		 * { "objectId" : 3001, "tagIds" : [2, 5, 9] },
		 * { "objectId" : 3002, "tagIds" : [3, 6, 10] }
		 * ],
		 * "newIdsInInventory" : [
		 * { "playerId" : 1000, "tagIds" : [1, 4, 8] },
		 * { "playerId" : 1001, "tagIds" : [2, 5, 9] },
		 * { "playerId" : 1002, "tagIds" : [3, 6, 10] }
		 * ],
		 * }
		 */
		int myPlayerId;
		int _objectId;
		myPlayerId = GameHandler.getInstance().getMyPlayer().getPlayerId();
		myPlayerInitialTags = new Integer[1];
		try {
			// _sessionId = jsonObject.getInt(LABEL_MSG_SESSIONID);
			
			JSONArray jsonArrayExhibits = jsonObject.getJSONArray(LABEL_MSG_INIT_TAGIDS_IN_EXHIBIT);
			for (int k = 0; k < jsonArrayExhibits.length(); k++) {
				JSONObject jsonExhibit = (JSONObject) jsonArrayExhibits.getJSONObject(k);
				_objectId = jsonExhibit.getInt(LABEL_MSG_INIT_OBJECTID);
				initIndexes[_objectId] = getIntArray(jsonExhibit, LABEL_MSG_INIT_TAGIDS);
			}
			JSONArray jsonArrayInventories = jsonObject.getJSONArray(LABEL_MSG_INIT_TAGIDS_IN_EXHIBIT);
			for (int k = 0; k < jsonArrayInventories.length(); k++) {
				JSONObject jsonPlayer = (JSONObject) jsonArrayInventories.getJSONObject(k);
				int playerId = jsonPlayer.getInt(LABEL_MSG_INIT_PLAYER);
				if (playerId == myPlayerId) {
					myPlayerInitialTags = getIntArray(jsonObject, LABEL_MSG_INIT_TAGIDS_IN_INVENTORY);
				}
			}
			
		} catch (JSONException e) {
			Log.e(TAG, "Couldn't parse JSON string " + e.toString());
		}
		
	}
	
	// I copied from JsonHelper -viky
	public ArrayList<Integer> getIntArray(JSONObject jsonObj, String tag) throws JSONException {
		ArrayList<Integer> al = new ArrayList<Integer>();
		
		// int[] intArray = {};
		JSONArray jArray = jsonObj.getJSONArray(tag);
		if (jArray != null) {
			for (int k = 0; k < jArray.length(); k++) {
				al.add(k, jArray.getInt(k));
			}
		}
		return al;
	}
	
	//
	// Getters
	//
	/**
	 * Split Message into a 2D array of integers
	 * 
	 * @return 2d int array
	 */
	public Integer[][] getExhibitSlotIndexes() {
		return this.initIndexes;
	}
	
	public Integer[] getInventoryIndexes() {
		return this.myPlayerInitialTags;
	}
	
	/**
	 * Construct string Message of the indexes given
	 * for the Server!
	 * Exhibits First Player second
	 * 
	 * @return String
	 */
	public String toString() {
		
		// TODO Nomizo de xreiazetai kan giati auto to dhmiourgei mono o server, kanenas client
		// if (!indexMap.containsKey(KEY_EXHIBITS) || !indexMap.containsKey(KEY_PLAYERS)) {
		// return "";
		// }
		// StringBuilder sb = new StringBuilder();
		//
		// // append the Exhibit Tags
		// // initIndexes = indexMap.get(KEY_EXHIBITS);
		// for (int exhibitIndx = 0; exhibitIndx < initIndexes.length; exhibitIndx++) {
		// sb.append(exhibitIndx);
		// sb.append(TAG_SEPARATOR);
		// for (int tagIndx : initIndexes[exhibitIndx]) {
		// sb.append(tagIndx);
		// sb.append(COMMA);
		// }
		// sb.deleteCharAt(sb.length()-1);
		// sb.append(EXHIBIT_SEPARATOR);
		// }
		// sb.deleteCharAt(sb.length()-1);
		//
		// sb.append(PLAYER_SEPARATOR);
		//
		// // append the player tags
		// for (int tagIndx : myPlayerInitialTags) {
		// sb.append(tagIndx);
		// sb.append(COMMA);
		// }
		// sb.deleteCharAt(sb.length()-1);
		//
		// return sb.toString();
		
		return message;
	}
	
	public static final Parcelable.Creator<MessageInitialization> CREATOR = new Parcelable.Creator<MessageInitialization>() {
		public MessageInitialization createFromParcel(Parcel in) {
			try {
				return new MessageInitialization(in);
			} catch (JSONException e) {
				Log.e(TAG, "Error while constructing from parcel" + e.toString());
				// FIXME rewrite to avoid null
				return null;
			}
		}
		
		public MessageInitialization[] newArray(int size) {
			return new MessageInitialization[size];
		}
	};
	
	@Override
	public int describeContents() {
		return this.toString().hashCode();
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.toString());
	}
	
}
