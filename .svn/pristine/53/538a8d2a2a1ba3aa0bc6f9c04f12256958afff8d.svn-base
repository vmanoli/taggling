package hci.projects.taggling.client;

import hci.projects.taggling.GlobalState;
import hci.projects.taggling.R;
import hci.projects.taggling.Adapters.CategoryListAdapter;
import hci.projects.taggling.Adapters.CategoryListAdapter.Options;
import hci.projects.taggling.Adapters.TagsListAdapter;
import hci.projects.taggling.ListLoaders.ExhibitsListLoader;
import hci.projects.taggling.ListLoaders.TagsListLoader;
import hci.projects.taggling.client.services.SendToServer;
import hci.projects.taggling.utils.TGConstants;
import hci.projects.tagglinglib.GameHandler;
import hci.projects.tagglinglib.GameElements.Exhibit;
import hci.projects.tagglinglib.Messages.MessageExhibitLock;
import hci.projects.tagglinglib.Messages.MessageExhibitUnlock;
import hci.projects.tagglinglib.Messages.MessageExhibitUpdate;
import hci.projects.tagglinglib.Messages.MessageTaggling;

import java.io.ByteArrayOutputStream;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends TGActivity implements ExhibitsListLoader.OnSelectedListener, PromptScanDialog.PromptScanDialogListener,
		FragmentGameOver.Listener, TagsListLoader.Listener {
	
	// GlobalState state;
	// private Handler mHandler = new Handler();
	
	// Fragments
	// FragmentLogin mLoginFragment;
	FragmentGameOver mGameOverFragment;
	ScreenWait mWaitScreen;
	
	// tags for debug logging
	final boolean ENABLE_DEBUG = true;
	final static String TAG = "TGClient";
	final static String MSG_TAG = "MessageOfClient";
	final static String LOGIC_TAG = "Taggling GameLogic";
	
	// Request codes
	final static int RC_WAITING_ROOM = 10001;
	final static int NO_SPECIFIC_EXHIBIT_SELECTED = 10002;
	final static int SPECIFIC_EXHIBIT_SELECTED = 10003;
	
	// COMMUNICATION SERVICE
	// Minimum number of players for the waiting room to dismiss. Including me.
	final int MIN_PLAYERS = 0;
	// Min and max number of auto match players in the room
	final int MIN_OPPONENTS = 1, MAX_OPPONENTS = 7;
	
	ByteArrayOutputStream mMsgBuf = new ByteArrayOutputStream();
	
	// STATE VARIANTS
	//
	// The game has finished?
	public boolean isGameOver = false;
	public boolean offlinePlay = false;
	
	// GAME Variants
	private Exhibit currentlySelectedExhibit;
	
	// UI - ProgressBar
	ProgressBar progressBar;
	
	// UI - Screens
	public enum Screen {
		LOGIN, ENTER, GAMEPLAY, TAGSLIST, GAMEOVER, WAIT
	}
	
	/*
	 * * * * * * * * * * * * * * * * * * *
	 * ACTIVITY Callback Methods
	 * * * * * * * * * * * * * * * * * * *
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// create fragments
		mGameOverFragment = new FragmentGameOver();
		mWaitScreen = new ScreenWait();
		
		// listen to fragment events
		mGameOverFragment.setListener(this);
		
		switchToScreen(Screen.GAMEPLAY);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.entry, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem score = menu.findItem(R.id.action_points);
		score.setTitle(Integer.toString(GameHandler.getInstance().getPlayer().getPlayerTotalPoints()));
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		AlertDialog dialog;
		switch (item.getItemId()) {
		case R.id.scan:
			openScanner(NO_SPECIFIC_EXHIBIT_SELECTED);
			return true;
		case R.id.action_inventory:
			dialog = showInventory();
			dialog.show();
			return true;
		case R.id.action_help:
			dialog = showHelp();
			dialog.show();
			return true;
//		case R.id.action_points:
//			dialog = showPointsTable();
//			dialog.show();
//			return true;
		case android.R.id.home:
			switchToScreen(Screen.LOGIN);
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		String qrCodeValue;
		switch (requestCode) {
		case NO_SPECIFIC_EXHIBIT_SELECTED:
			if (resultCode == RESULT_OK) {
				// Determine the selected exhibit
				qrCodeValue = intent.getStringExtra("SCAN_RESULT");
				for (Exhibit exhibit : GameHandler.getInstance().getGameExhibits()) {
					if (exhibit.isExhibitHexCode(qrCodeValue)) {
						if (exhibit.isLockableBy(GameHandler.getInstance().getPlayer().getPlayerId())) {
							MessageExhibitLock newMessageExhibitLock = new MessageExhibitLock(GameHandler.getInstance()
									.getPlayer().getPlayerId(), exhibit.getExhibitId());
							WaitForResponseTask waitForResponseTask = new WaitForResponseTask(newMessageExhibitLock);
							waitForResponseTask.execute(newMessageExhibitLock);
						} else {
							Toast.makeText(this, R.string.qr_locked, Toast.LENGTH_LONG).show();
						}
						return;
					}
				}
				Toast.makeText(this, R.string.qr_not_accepted, Toast.LENGTH_LONG).show();
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.qr_failed, Toast.LENGTH_SHORT).show();
			}
			break;
		case SPECIFIC_EXHIBIT_SELECTED:
			if (resultCode == RESULT_OK) {
				// Determine the selected exhibit
				if (currentlySelectedExhibit.getExhibitId() == Exhibit._NO_EXHIBIT_ID) {
					Toast.makeText(this, "No name selected", Toast.LENGTH_LONG).show(); //FIXME
				} else {
					qrCodeValue = intent.getStringExtra("SCAN_RESULT");
					if (currentlySelectedExhibit.getExhibitQRTagValue().equals(qrCodeValue)) {
						if (currentlySelectedExhibit.isLockableBy(GameHandler.getInstance().getPlayer().getPlayerId())) {
							MessageExhibitLock newMessageExhibitLock = new MessageExhibitLock(GameHandler.getInstance()
									.getPlayer().getPlayerId(), currentlySelectedExhibit.getExhibitId());
							WaitForResponseTask waitForResponseTask = new WaitForResponseTask(newMessageExhibitLock);
							waitForResponseTask.execute(newMessageExhibitLock);
						} else {
							Toast.makeText(this, R.string.qr_locked, Toast.LENGTH_LONG).show();
						}
						return;
					}
				}
				Toast.makeText(this, R.string.qr_not_correct, Toast.LENGTH_LONG).show();
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.qr_failed, Toast.LENGTH_SHORT).show();
			}
			break;
		default:
			super.onActivityResult(requestCode, resultCode, intent);
		}
	}
	
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UI Listener Methods - Exhibits List Loader
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	@Override
	public void onExhibitSelected(int selectedExhibitId) {
		currentlySelectedExhibit = GameHandler.getInstance().getGameExhibits().getExhibitById(selectedExhibitId);
		if (currentlySelectedExhibit.isLockableBy(GameHandler.getInstance().getPlayer().getPlayerId())) {
			PromptScanDialog prompt = new PromptScanDialog();
			
			Bundle args = new Bundle();
			args.putInt("DIALOG_THUMBNAIL", (GameHandler.getInstance().getGameExhibits().getExhibitById(selectedExhibitId)).getExhibitThumbnail(getBaseContext()));
			args.putString("DIALOG_NAME", (GameHandler.getInstance().getGameExhibits().getExhibitById(selectedExhibitId)).getExhibitName());
			prompt.setArguments(args);
			prompt.show(getFragmentManager(), null);
		} else {
			informUnreadable();
		}
	}
	
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UI Listener Methods - PromptDialog
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	/**
	 * @category listener UI
	 */
	@Override
	public void onDialogPositiveClick() {
		// User touched the dialog's positive button
		openScanner(SPECIFIC_EXHIBIT_SELECTED);
		return;
	}
	
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UI Listener Methods - Tags List
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	public void onTagsListConfirmed(MessageExhibitUpdate updateMsg) {
		WaitForResponseTask waitForResponseTask = new WaitForResponseTask(updateMsg);
		waitForResponseTask.execute(updateMsg);
	}
	
	@Override
	public void onTagsListDissmised(Exhibit exhibit) {
		AlertDialog dialog = dismissTagslist(exhibit);
		dialog.show();		
	}

	public void sendLockExhibitMessage(Exhibit exhibitToLock) {
		MessageExhibitLock newLockMessage = new MessageExhibitLock(GameHandler.getInstance().getPlayer().getPlayerId(), exhibitToLock.getExhibitId());
		WaitForResponseTask waitForResponseTask = new WaitForResponseTask(newLockMessage);
		waitForResponseTask.execute(newLockMessage);
		
	}
	
	// public void guessingGameOver(MessageExhibitUpdate updateMsg) {
	// // FIXME edo prepei na ginetai apostoli tou MessageExhibitUpdate ston server
	// setMessageBuffer(Messages.UPDATING_EXHIBIT, updateMsg.toString());
	// broadcastToAll();
	// try {
	// Thread.sleep(100);
	// } catch (InterruptedException e) {
	// e.printStackTrace();
	// }
	// setMessageBuffer(Messages.GUESSING_GAME_OVER, "");
	// broadcastToAll();
	// switchToScreen(Screen.GAMEPLAY);
	// }
	
	public enum MessageDeliveryResult {
		SUCCEEDED, FAILED, CANCELED;
	};
	
	private boolean hasReturned(MessageTaggling tgmessage) {
		boolean ret = GlobalState.messageQueue.hasReturned(tgmessage);
		
		return ret;
	}
	
	class WaitForResponseTask extends AsyncTask<MessageTaggling, Void, MessageDeliveryResult> {
		private ProgressDialog progressDialog;
		boolean running = true;
		int timer = 0;
		final int _TRY_INTERVAL = 250;
		private MessageTaggling outgoingMessage = null;
		
		public WaitForResponseTask(final MessageTaggling tgMessage) {
			super();
			outgoingMessage = tgMessage;
			progressDialog = ProgressDialog.show(MainActivity.this, getString(R.string.mainactivity_please_wait),
					getString(R.string.mainactivity_sending_request), false);
			progressDialog.setCancelable(true);
			progressDialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					GlobalState.messageQueue.cancelRequest(tgMessage);
				}
			});
		}
		
		@Override
		protected void onPreExecute() {
			progressDialog.show();
			GlobalState.messageQueue.pushMessageIntoQueue(outgoingMessage);
			NameValuePair[] nameValuePairs = new NameValuePair[1];
			nameValuePairs[0] = new BasicNameValuePair(TGConstants._TAG_XMPP_OUTBOUND_CSMESSAGE, outgoingMessage.toString());
			SendToServer sendToServer = new SendToServer();
			sendToServer.execute(nameValuePairs);
		}
		
		@Override
		protected MessageDeliveryResult doInBackground(MessageTaggling... params) {
			MessageDeliveryResult messageDeliveryResult = MessageDeliveryResult.FAILED;
			
			try {
				while (running) {
					timer += _TRY_INTERVAL;
					if (timer > TGConstants._SEND_REQUEST_TIMEOUT) {
						progressDialog.dismiss();
						running = false;
						csApplication.checkForMissedMessages();
						messageDeliveryResult = MessageDeliveryResult.FAILED;
						Log.i(TGConstants.LOG_TAG, "TopicActivity:timer:message delivery failed " + params[0].toString());
						
					} else if (hasReturned(params[0])) {
						progressDialog.dismiss();
						running = false;
						messageDeliveryResult = MessageDeliveryResult.SUCCEEDED;
						switch (outgoingMessage.getMessageType()) {
						case EXHIBIT_LOCK:
							MainActivity.this.runOnUiThread(new Runnable() {
								public void run() {
									openTagsFragment(((MessageExhibitLock) outgoingMessage).getObjectId());
								}
							});
							break;
						case EXHIBIT_UPDATE:
							MainActivity.this.runOnUiThread(new Runnable() {
								public void run() {
									switchToScreen(Screen.GAMEPLAY); // switch to main screen;
								}
							});
							break;
						case EXHIBIT_UNLOCK:
							MainActivity.this.runOnUiThread(new Runnable() {
								public void run() {
									switchToScreen(Screen.GAMEPLAY); // switch to main screen;
								}
							});
							break;
						}
					}
					Thread.sleep(_TRY_INTERVAL);
				}
			} catch (InterruptedException e) {
				Log.e(TAG, e.toString());
			}
			return messageDeliveryResult;
		}
		
		@Override
		protected void onPostExecute(MessageDeliveryResult messageDeliveryResult) {
			progressDialog.dismiss();
			if (messageDeliveryResult != MessageDeliveryResult.SUCCEEDED) {
				Toast.makeText(MainActivity.this, R.string.message_failed, Toast.LENGTH_SHORT).show();
			}
			if (messageDeliveryResult == MessageDeliveryResult.SUCCEEDED) {
				if (MainActivity.this != null) {
					(MainActivity.this).invalidateOptionsMenu();
				}
			}
		}
		
		@Override
		protected void onCancelled() {
			running = false;
		}
	}
	
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UI Listener Methods - Connection Management
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UI Listener Methods - Game Over Screen
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	@Override
	public void onOfflineButtonClicked() {
		offlinePlay = true;
		switchToScreen(Screen.GAMEPLAY);
		Log.d(TAG, "Offline Button Clicked");
	}
		
	/*
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * UTILITY Methods
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	
	public void switchToScreen(Screen screen) {
		switch (screen) {
		case WAIT:
			commitFragmentTransaction(mWaitScreen);
			break;
		case GAMEOVER:
			commitFragmentTransaction(mGameOverFragment);
			break;
		case GAMEPLAY:
			getActionBar().show();
			// this.lockDrawerClosed(false);
			Fragment tabsFrag = new TabsFragmentExhibits();
			commitFragmentTransaction(tabsFrag);
		default:
			break;
		}
	}
	
	private void openTagsFragment(int selectedExhibitId) {
		// Create fragment and give it an argument for the selected exhibit
		//
		TagsListLoader tagListFrg = new TagsListLoader();
		
		Bundle args = new Bundle();
		args.putInt(TagsListLoader.PRESSED_EXHIBIT, selectedExhibitId);
		tagListFrg.setArguments(args);
		
		commitFragmentTransaction(tagListFrg);
	}
	
	/**
	 * Helper method to commit fragment transactions in general
	 * 
	 * @param frag
	 *                The fragment to switch to
	 */
	public void commitFragmentTransaction(Fragment frag) {
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.fragment_container, frag); // in main activity
		// // Saved the day
		// // not keeping the tags fragment to backstack
		// // because you can only go there by scanning
		// if (fragmentManager.findFragmentByTag("TagsFragment") == null) {
		// transaction.addToBackStack(null);
		// }
		// transaction.addToBackStack(null);
		transaction.commitAllowingStateLoss();
	}
	
	@Override
	public void onBackPressed() {
	}
	
	public void gameStart() {
		isGameOver = false;
		// offlinePlay = false;
		getActionBar().show();
		// this.lockDrawerClosed(false);
		// selectItem(0); // Go to Exhibits list
		// switchToScreen(Screen.GAMEPLAY);
		Log.d(TAG, "Let the Game begin!");
	}
	
	public void gameOver() {
		isGameOver = true;
		
	}
	
	public AlertDialog showCategories() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle(R.string.frag_name_categories);
		alertDialogBuilder.setAdapter(
				new CategoryListAdapter(this, R.layout.category_list_item, GameHandler.getInstance().getGameTagCategories(),
						Options.DONT_SHOW_DESCRIPTION), null).setNegativeButton(R.string.go_back,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		return alertDialogBuilder.create();
	}
	
	public AlertDialog showInventory() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle(R.string.frag_name_inventory);
		alertDialogBuilder.setAdapter(new TagsListAdapter(this, R.layout.tags_list_item, GameHandler.getInstance().getPlayer().getPlayerInventory()), null)
				.setNegativeButton(R.string.go_back, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		return alertDialogBuilder.create();
	}
	
	public AlertDialog showHelp() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle(R.string.action_show_info);
		
		LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
		View mRoot = layoutInflater.inflate(R.layout.help_screen, null);
		
		alertDialogBuilder.setView(mRoot);
		alertDialogBuilder.setNegativeButton(R.string.go_back, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		return alertDialogBuilder.create();
	}
	
	public AlertDialog dismissTagslist(final Exhibit exhibit) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setMessage(R.string.dialog_dismiss_tagslist)
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						
						//FIXME steile unlock message
						MessageExhibitUnlock newMessageExhibitUnlock = new MessageExhibitUnlock(GameHandler.getInstance()
								.getPlayer().getPlayerId(), exhibit.getExhibitId());
						WaitForResponseTask waitForResponseTask = new WaitForResponseTask(newMessageExhibitUnlock);
						waitForResponseTask.execute(newMessageExhibitUnlock);
					}
				})	
				.setNegativeButton(R.string.go_back, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		return alertDialogBuilder.create();
	}
	
//	public AlertDialog showPointsTable() {
//		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//		alertDialogBuilder.setTitle(R.string.tab_points);
//		alertDialogBuilder.setAdapter(
//				new PointsTableListAdapter(this, R.layout.pointstable_list_item, GameHandler.getInstance().getPlayer().getPointsTable()), null)
//				.setNegativeButton(R.string.go_back, new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog, int id) {
//						dialog.cancel();
//					}
//				});
//		return alertDialogBuilder.create();
//	}
	
	protected void openScanner(int reqCode) {
		try {
			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
			startActivityForResult(intent, reqCode);
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this, "ERROR:" + e, Toast.LENGTH_SHORT).show();
		}
	}
	
	public void informUnreadable() {
		Toast.makeText(this, R.string.mainactivity_exhibit_locked, Toast.LENGTH_SHORT).show();
	}
	
}
