package hci.projects.taggling.ListLoaders;

import hci.projects.taggling.Adapters.TagsListAdapter;
import hci.projects.tagglinglib.Messages.MessageExhibitUpdate;
import hci.projects.taggling.R;
import hci.projects.tagglinglib.GameElements.Exhibit;
import hci.projects.tagglinglib.GameElements.Tag;
import hci.projects.tagglinglib.GameHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * @author vicky
 * @category all
 */
public class TagsListLoader extends ListFragment implements View.OnClickListener {
	
	// Communication Methods
	Listener mCallback;
	
	public interface Listener {
		public void onTagsListDismissed(MessageExhibitUpdate exhibitUpdate);
		
	}
	
	public final static String PRESSED_EXHIBIT = "hci.projects.taggling.PASSED_EXHIBIT";
	private Exhibit mExhibit;
	private int selectedExhibitId;
	private View mList;
	
	private Activity mActivity;
	
	@Override
	public void onAttach(Activity activity) {
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (Listener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement Listener method");
		}
		
		// Used onAttach to instantiate the exhibit_index value after fragment
		// changes
		super.onAttach(activity);
		setHasOptionsMenu(true);
		mActivity = activity;
		
		// Get the Arguments from Exhibits List
		//
		selectedExhibitId = this.getArguments().getInt(PRESSED_EXHIBIT);
		mExhibit = GameHandler.getInstance().getExhibitById(selectedExhibitId);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		ImageView exhibitImg;
		int exhibitImgRes = 0;
		
		exhibitImgRes = mExhibit.getExhibitThumbnail(mActivity.getBaseContext());
		
		// Set the inflater
		//
		mList = inflater.inflate(R.layout.tags_list_view, null);
		
		// Set the activity title according to exhibit name
		//
		mActivity.getActionBar().setTitle(mExhibit.getExhibitName());
		
		exhibitImg = (ImageView) mList.findViewById(R.id.tags_exhibit_thumb);
		exhibitImg.setImageResource(exhibitImgRes);
		
		mList.findViewById(R.id.button_tags_back).setOnClickListener(this);
		
		// Set the Adapter
		//
		ListAdapter arrAdd = new TagsListAdapter(mActivity, R.layout.tags_list_item, mExhibit.exhibitTagSlots);
		setListAdapter(arrAdd);
		
		// mCallback.lockDrawerClosed(true);
		
		return mList;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.tags_menu, menu);
		menu.findItem(R.id.scan).setVisible(false);
	}
	
	/**
	 * On list item clicked : Show dialog to confirm Remove tag
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
		if (mExhibit.isCompleted()) {
			Toast toast = Toast.makeText(mActivity, R.string.toast_exhibit_completed, Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		} else {
			Object tagSelected = this.getListAdapter().getItem(position);
			
			if ((Tag) tagSelected == null) {
				AlertDialog addDialog = addTagDialog();
				addDialog.show();
			} else {
				AlertDialog removeDialog = removeTagDialog((Tag) tagSelected);
				removeDialog.show();
			}
		}
	}
	
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.button_tags_back) {
			mCallback.onTagsListDismissed(createUpdateMsg());
		}
	}
	
	public MessageExhibitUpdate createUpdateMsg() {
		// create tags indexes
		Integer[] iTags = GameHandler.getInstance().getExhibitSlotsUpdate(mExhibit);
		
		// create inventory indexes
		Integer[] iTagsInv = GameHandler.getInstance().getInventoryUpdate();
		
		MessageExhibitUpdate exUpdt = new MessageExhibitUpdate(GameHandler.getInstance().getMyPlayer().getPlayerId(), mExhibit.getExhibitId(), iTags, iTagsInv);
		
		return exUpdt;
	}
	
	/**
	 * <b>Add</b> Alert dialog showing a list of the player's tags to choose
	 * which to add
	 */
	public AlertDialog addTagDialog() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
		alertDialogBuilder.setTitle(R.string.dialog_add_title);
		alertDialogBuilder.setAdapter(new TagsListAdapter(mActivity, R.layout.tags_list_item, GameHandler.getInstance().getPlayer().inventory),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int position) {
						if (GameHandler.getInstance().getPlayer().inventory.get(position) != null) {
							addTagToExhibit(position);
						}
						
					}
				}).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		return alertDialogBuilder.create();
	}
	
	/**
	 * Adds a tag to the exhibit from player inventory <b>and</b> reloads the
	 * tag list
	 * 
	 * @param position
	 */
	private void addTagToExhibit(int position) {
		
		// Add tag to exhibit and get points
		int tagpoints = mExhibit.attachTag(GameHandler.getInstance().getPlayer().inventory.get(position));
		// Remove tag from inventory
		GameHandler.getInstance().getPlayer().inventory.remove(position);
		// Add points to player
		GameHandler.getInstance().getPlayer().addPoints(mExhibit.getExhibitName(), tagpoints);
		
		// Reload the list
		ListAdapter arrAdd = new TagsListAdapter(mActivity, R.layout.tags_list_item, mExhibit.exhibitTagSlots);
		setListAdapter(arrAdd);
		
		// Inform with Toast for points
		Toast.makeText(mActivity, tagpoints + " points", Toast.LENGTH_SHORT).show();
		
		// Update points action bar icon
		mActivity.invalidateOptionsMenu();
		
		// // Check if the Game is Over
		// if (mExhibit.isCompleted()) {
		// if (GameHandler.getInstance().isGameCompleted()) {
		//
		// mCallback.guessingGameOver(createUpdateMsg());
		// Toast.makeText(mActivity, "! GAME OVER !", Toast.LENGTH_LONG)
		// .show();
		// }
		// }
		
	}
	
	/**
	 * <b>Remove</b> Alert dialog asking the user confirmation to remove the tag
	 * 
	 * @param tag
	 * @return AlertDialog
	 */
	public AlertDialog removeTagDialog(final Tag tag) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
		alertDialogBuilder.setTitle(R.string.dialog_remove_title);
		alertDialogBuilder.setMessage(R.string.dialog_remove).setCancelable(false)
				.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						removeTagFromExhibit(tag);
					}
				}).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		return alertDialogBuilder.create();
	}
	
	/**
	 * Removes selected tag from the Exhibit list to the inventory <b>and</b>
	 * reloads the tag list
	 * 
	 * @param tagToRemove
	 */
	private void removeTagFromExhibit(Tag tagToRemove) {
		
		if (GameHandler.getInstance().getPlayer().inventory.contains(null)) { // Check if inventory
											// is full
			// Remove tag from Exhibit
			mExhibit.removeTag(tagToRemove);
			// Move tag to Inventory
			GameHandler.getInstance().getPlayer().inventory.add(tagToRemove);
			// Reload Tag list
			ListAdapter arrAdd = new TagsListAdapter(mActivity, R.layout.tags_list_item, mExhibit.exhibitTagSlots);
			setListAdapter(arrAdd);
			// Inform with Toast
			Toast.makeText(mActivity, R.string.toast_moved, Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(mActivity, R.string.no_slots_available, Toast.LENGTH_LONG).show();
		}
	}
}
