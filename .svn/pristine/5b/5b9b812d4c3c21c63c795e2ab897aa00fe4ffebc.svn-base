package hci.projects.taggling.utils;

import hci.projects.tagglinglib.GameElements.Player;
import android.graphics.Color;

public final class TGConstants {
	
	public static final Boolean TEST = false;
	
	public static final int _NO_OBJECT_ID = -1; // object in gallery is not yet
	// scanned
	public static final int _NO_OBJECT_SELECTED = -2;
//	public static final DummyObject _NO_OBJECT = new DummyObject(_NO_OBJECT_ID);
	public static final int _NO_KEY_ID = -1;
//	public static final Key _NO_KEY = new Key(_NO_KEY_ID, _NO_OBJECT_ID, 0, "no text", "no hint");
//	public static final KeyList _NO_KEYLIST = new KeyList(_NO_KEY);
	
	public static final int _NO_TOPIC_ID = -1;
	public static final String _NO_TOPIC_NAME = "No name";
	public static final String _NO_TOPIC_DESCRIPTION = "No description";
//	public static Topic _NO_TOPIC = new Topic(_NO_TOPIC_ID, _NO_TOPIC_NAME, _NO_TOPIC_DESCRIPTION, null, _NO_KEYLIST);
	
	public static final int _NO_PLAYER_COLOR = Color.LTGRAY;
	public static final int _NO_PLAYER_ID = -1;
	public static final String _NO_PLAYER_NAME = "No player";
	public static final String _NO_PLAYER_TEAM = "No team";
//	public static final Player _NO_PLAYER = new Player(_NO_PLAYER_ID, _NO_PLAYER_COLOR, _NO_PLAYER_NAME, _NO_PLAYER_TEAM);
	
	// Broadcasters
	public static final String INTENT_NEW_DATA_FROM_PLAYER = "NEW_DATA_FROM_PLAYER";
	public static final String INTENT_NEW_DATA_FROM_SERVER = "NEW_DATA_FROM_SERVER";
	public static final String INTENT_GAME_FINISHED = "GAME_FINISHED";
	public static final String INTENT_GAME_STARTED = "GAME_STARTED";
	public static final String INTENT_NEW_SCAN_FROM_TEAMMEMBER = "NEW_SCAN_FROM_TEAMMEMBER";
	
	// _TAG_??? is for labels that get passed with intents
	public static final String _TAG_USER = "username";
	public static final String _TAG_PASS = "password";
	public static final String _TAG_ROOM = "muc-room";
	public static final String _TAG_XMPPUSER = "xmppusername";
	public static final String _TAG_XMPPPASS = "xmpppassword";
	public static final String _TAG_XMPP_ROOMLIST = "roomlist";
	public static final String _TAG_TOPIC_ID = "topicId";
	public static final String _TAG_XMPP_OUTBOUND_CSMESSAGE = "m";
	public static final String _TAG_XMPP_OUTBOUND_SCANMESSAGE = "s";
	public static final String _TAG_XMPP_OUTBOUND_POSITIONMESSAGE = "p";
	
	public static final String _GAME_ACTIONMESSAGE = "csmsg";
	public static final String _GAME_EXHIBITUPDATEMESSAGE = "exhupd";
	public static final String _GAME_INITIALIZATIONMESSAGE = "initmsg";
	public static final String _GAME_LOCKEDMESSAGE = "lockmsg";
	public static final String _GAME_SCANMESSAGE = "scanmsg";
	public static final String _GAME_CONTROLMESSAGE = "ctrlmsg";
	public static final String _TAG_GEO_CURRENT_OBJECT_ID = "geocode";
	
	public static final long SPLASH_DISPLAY_TIME = 2000; // how long to display
	// the splash screen
	// (ms)
	public static final String _URL_BASE = "http://dl.dropbox.com/u/197409/CityScrabble/carnival/";
	public static final String _URL_IMAGES = _URL_BASE + "";
	
	// TEST_??? is only for debugging/testing
	// public static final String TEST_GAME_XML = _URL_BASE + "cityscrabble";
	// public static final String _TEST_USERNAME = "chris";
	public static final String _XML_TAG_TOPIC = "Topic";
	public static final String _XML_TAG_TOPICTITLE = "TopicTitle";
	public static final String _XML_TAG_TOPICDESCRIPTION = "TopicDescription";
	public static final String _XML_TAG_TOPICIMAGE = "TopicImage";
	public static final String _XML_TAG_KEYTEXT = "KeyText";
	public static final String _XML_TAG_KEYHINT = "KeyHint";
	public static final String _CSMESSAGE_DELIMETER = "@";
	
	public static final int _TILE_WIDTH = 25;
	public static final CharSequence _QRCODE_PREFIX = "http://150.140.188.242/t/";
	public static final int _TEST_NUM_OF_KEYS = 4;
	public static final int _SEND_REQUEST_TIMEOUT = 10000; // in ms
	public static final int _SESSION_NO_OCCUPANTS = -1;
	public static final String _VCARD_PLAYER_COLOR = "color";
	public static final String _MASTER_XMPPID = "muc";
	public static final String _MASTER_ID = "gamemaster";
	public static final int _MASTER_COLOR = 0;
	public static final String _MASTER_NAME = "Game master";
	public static final int _ROSTER_DIALOG_ID = 0;
	public static final String _TAG_SESSION_NAME = "sessionname";
	public static final String LOG_TAG = "LOG";
	public static final int REQUEST_CODE_GAME_FINISH = 5;
	public static final int REQUEST_CODE_QR_SCAN = 0;
	public static final int XMPP_PACKET_TIMEOUT = 5000;
	public static int _MAX_HISTORY_SIZE = 0; // how many missed messages to
	// retrieve
	public static String _GCM_ON_REGISTERED_MESSAGE = "registered";
	public static String _GCM_ON_UNREGISTERED_MESSAGE = "unregistered";
	public static String _GCM_ON_ERROR = "errorMessage";
	public static String _GCM_ON_DELETED_MESSAGES = "deletedMessages";
	
	// Google Cloud Messaging
	// give your server registration url here
	public static final String SERVER = "http://150.140.188.242/gcm-server";
	public static final String ACTIVE_SESSIONS_SERVER_URL = SERVER + "/active_sessions.php";
	public static final String LOGIN_SERVER_URL = SERVER + "/auth.php";
	public static final String LOGOUT_SERVER_URL = SERVER + "/logout.php";
	public static final String SERVER_GET_INITIAL_TAGS_DISTRIBUTION = SERVER + "/get-initial-tags-distribution.php";
	public static final String SERVER_GET_TEAM_SCORE_LIST = SERVER + "/get-team-score-list.php";
	public static final String SERVER_URL = SERVER + "/cityscrabble.php";
	public static final String SERVER_GET_LAST_UPDATES = SERVER + "/get-last-updates.php";
	
	// Google project id
	public static final String SENDER_ID = "998778277852-j6gqaonfas3hllubam5688t5aq1qhh9o.apps.googleusercontent.com";
	
	public static final String INTENT_GCM_SERVICE_MESSAGE = "chris.projects.cityscrabble.DISPLAY_MESSAGE";
	
	public static final String EXTRA_MESSAGE = "message";
	public static final String _FIELD_LANG = "lang";
	public static final String _FIELD_USERNAME = "username";
	public static final String _FIELD_PASSWORD = "password";
	public static final String _FIELD_REGID = "gcm_regid";
	public static final String _FIELD_ANDROIDID = "androidid";
	public static final String _FIELD_LAST_KNOWN_ID = "lastknownid";
	public static final String _XML_TAG_OBJECT = "Object";
	public static final String _XML_TAG_OBJECT_TITLE = "ObjectTitle";
	public static final String _XML_TAG_OBJECT_HINT = "ObjectHint";
	public static final String _XML_TAG_OBJECT_DESCRIPTION = "ObjectDescription";
	public static final String _XML_TAG_OBJECT_COLLECTTYPE = "CollectType";
	public static final String _XML_TAG_OBJECT_COLLECTDATA = "CollectData";
	public static final String _XML_TAG_OBJECT_LOCATION = "ObjectLocation";
	public static final String _XML_TAG_OBJECT_IMAGE = "ObjectImage";
	public static final String _XML_TAG_OBJECT_ALTERNATE_DESCRIPTION = "ObjectAlternateDescription";
	public static final String _XML_TAG_TOPICID = "TopicId";
	public static final String _XML_TAG_TOPICKEY = "Key";
	public static final String _XML_TAG_TOPICKEYID = "KeyId";
	public static final String _XML_TAG_TOPICKEYOBJECTID = "ObjectId";
	public static final String _XML_TAG_TOPICKEYVALUE = "KeyValue";
	public static final String _XML_TAG_OBJECT_ID = "ObjectId";
	public static final String _XML_TAG_OBJECTCOLLECTTYPE_QR = "QR";
	public static final String _XML_TAG_OBJECTCOLLECTTYPE_GEO = "GEO";
	public static final String _TAG_NEWSCANFROMTEAMMEMBER = "NewScanFromTeamMember";
	
	public static final long MINIMUM_DISTANCECHANGE_FOR_UPDATE = 15; // in Meters
	public static final long MINIMUM_TIME_BETWEEN_UPDATE = 30000; // in Milliseconds
}
