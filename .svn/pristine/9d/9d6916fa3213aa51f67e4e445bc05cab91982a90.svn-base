/**
 * 
 */
package hci.projects.tagglinglib.GameElements;

import java.util.ArrayList;

import org.json.JSONObject;

import android.graphics.Color;
import android.util.Log;

/**
 * @author vicky
 */
public class Player {
	public static final int __DEFAULT_MAXSLOTS__ = 7;
	public static final int _NO_PLAYER_ID = -1;
	
	private int playerId;
	private String playerName;
	// private int playerTotalPoints;
	private Team playerTeam;
	private int maxSlots;
	public Inventory inventory;
	public PointsTable pointTable;
	
	private final String TAG = "Player";
	
	/**
	 * constructor with team setter
	 * 
	 * @param playerId
	 * @param playerName
	 * @param playerTotalPoints
	 * @param playerTeam
	 */
//	public Player(int playerId, String playerName, Team playerTeam, int maxSlots) {
//		this.playerId = playerId;
//		this.playerName = playerName;
//		// this.playerTotalPoints = 0;
//		this.playerTeam = playerTeam;
//		this.maxSlots = maxSlots;
//		this.inventory = new Inventory(maxSlots);
//		this.pointTable = new PointsTable();
//	}
//	
	public Player(JSONObject jsonString) {
		try {
			this.playerId = jsonString.getInt("playerId");
			this.playerName = jsonString.getString("playerName");
			this.playerTeam = new Team(jsonString.getString("playerTeam"), Color.parseColor(jsonString.getString("teamColor")));
			this.maxSlots = __DEFAULT_MAXSLOTS__;
			this.inventory = new Inventory(maxSlots);
			this.pointTable = new PointsTable();
		} catch (Exception e) { // FIXME: Test what happens if the parsing is aborted
			Log.e(TAG, "Couldn't parse player JSON string: " + jsonString.toString());
		}
	}
	
	// /**
	// * Constructor with NAME ONLY. Team is null. maxSlots is default.
	// *
	// * @param playerName
	// */
	// public Player(String playerName) {
	// this.playerName = playerName;
	// // this.playerTotalPoints = 0;
	// this.playerTeam = null;
	// this.maxSlots = __DEFAULT_MAXSLOTS__;
	// this.inventory = new Inventory(maxSlots);
	// this.pointTable = new PointsTable();
	// }
	//
	// /**
	// * @param playerName
	// * the playerName to set
	// */
	// public void setPlayerName(String playerName) {
	// this.playerName = playerName;
	// }
	//
	// /**
	// * @param playerTeam
	// * the playerTeam to set
	// */
	// public void setPlayerTeam(Team playerTeam) {
	// this.playerTeam = playerTeam;
	// }
	
	/**
	 * @return the playerId
	 */
	public int getPlayerId() {
		return playerId;
	}
	
	/**
	 * @return the playerName
	 */
	public String getPlayerName() {
		return playerName;
	}
	
	public int getMaxSlots() {
		// TODO Auto-generated method stub
		return maxSlots;
	}
	
	/**
	 * @return the Player total points
	 */
	public int getPlayerTotalPoints() {
		return pointTable.getTotalPoints();
	}
	
	/**
	 * Get the player's points gained only out of the specific exhibit
	 * 
	 * @param the
	 *                exhibit name
	 * @return the playerPoints
	 */
	public int getPlayerPoints(String exhibitName) {
		int playerPoints = 0;
		for (PointsExhibitPair pair : pointTable) {
			if (pair.exhibitName.equals(exhibitName)) {
				playerPoints = playerPoints + pair.points;
			}
		}
		return playerPoints;
	}
	
	/**
	 * @return the playerTeam
	 */
	public Team getPlayerTeam() {
		return playerTeam;
	}
	
	/**
	 * Adding points gained to the specific exhibit in the table
	 * 
	 * @param name
	 * @param p
	 * @return False if a new pair was created, True if the points were added to
	 *         an existing one
	 */
	public boolean addPoints(String name, int p) {
		if (!pointTable.isEmpty()) {
			for (PointsExhibitPair pair : pointTable) {
				if (pair.exhibitName.equals(name)) {
					pair.addPointsGained(p);
					return true;
				}
			}
		}
		pointTable.add(new PointsExhibitPair(name, p));
		return false;
	}
	
	@SuppressWarnings("serial")
	public class Inventory extends ArrayList<Tag> {
		//FIXME
		@Override
		public boolean add(Tag tag) {
			int i = indexOf(null);
			if (i == -1) {
				return false;
			} else {
				set(i, tag);
				return true;
			}
		}
		
		@Override
		public boolean remove(Object tag) {
			int i = indexOf(tag);
			if (i == -1) {
				return false;
			} else {
				set(i, null);
				return true;
			}
		}
		
		@Override
		public Tag remove(int position) {
			set(position, null);
			return null;
		}
		
		@Override
		public void clear() {
			for (Tag tag : this) {
				remove(tag);
			}
		}
		
		public int countEmptySlots() {
			int c = 0;
			for (Tag tag : inventory) {
				if (tag == null) {
					c++;
				}
			}
			return c;
		}
		
		/**
		 * Constructor that instantiates an ArrayList with initial capacity for
		 * better memory allocation and initializes it with null elements
		 * 
		 * @param slotsCapacity
		 * @return
		 */
		public Inventory(int slotsCapacity) {
			super(slotsCapacity);
			for (int i = 0; i < slotsCapacity; i++) {
				super.add(null);
			}
		}
	}
	
	/**
	 * Class PointsTable for storing pairs
	 */
	@SuppressWarnings("serial")
	public class PointsTable extends ArrayList<PointsExhibitPair> {
		private int getTotalPoints() {
			int totalPoints = 0;
			if (this.isEmpty()) {
				return 0;
			} else {
				for (PointsExhibitPair pep : this) {
					totalPoints += pep.points;
				}
			}
			return totalPoints;
		}
	}
	
	/**
	 * Class of "Exhibit - Points gained" pairs
	 * 
	 * @param String
	 *                exhibitName
	 * @param int points
	 */
	public class PointsExhibitPair {
		public String exhibitName;
		public int points;
		
		public PointsExhibitPair(String name, int p) {
			this.exhibitName = name;
			this.points = p;
		}
		
		public void addPointsGained(int gainedPoints) {
			points = points + gainedPoints;
		}
	}
	
}
