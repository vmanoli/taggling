package hci.projects.tagglinglib.GameElements;

import java.util.ArrayList;

public class Tag {
	public static final int _NO_TAG_ID = -1;
	
	/**
	 * The name of the tag
	 */
	private String tagName;
	
	/**
	 * The id of the tag
	 */
	private int tagId;
	
	/**
	 * The points that this tag is worth. Points are awarded only if the tag has
	 * been attached an exhibit in the {@link #tagValidExhibitsList}
	 */
	private int tagPoints;
	
	/**
	 * The list of exhibits this tag can be attached to and give points to the
	 * player
	 */
	private ArrayList<Exhibit> tagValidExhibitsList;
	
	/**
	 * The {@link TagCategory} that this tag belongs to
	 */
	private TagCategory tagCategory;
	
	/**
	 * The hint to display
	 */
	private Hint hint;
	
	/**
	 * Full constructor
	 * 
	 * @param tagId
	 * @param tagName
	 * @param tagPoints
	 * @param tagValidExhibitsList
	 * @param tagCategory
	 */
	public Tag(int tagId, String tagName, int tagPoints, ArrayList<Exhibit> tagValidExhibitsList, TagCategory tagCategory, Hint tagHint) {
		this.tagId = tagId;
		this.tagName = tagName;
		this.tagPoints = tagPoints;
		this.tagValidExhibitsList = tagValidExhibitsList;
		this.tagCategory = tagCategory;
		this.hint = tagHint;
	}
	
	/**
	 * Constructor with one exhibit only
	 * 
	 * @param tagId
	 * @param tagName
	 * @param tagPoints
	 * @param tagValidExhibit
	 * @param tagCategory
	 */
	public Tag(int tagId, String tagName, int tagPoints, Exhibit tagValidExhibit, TagCategory tagCategory, Hint tagHint) {
		this.tagId = tagId;
		this.tagName = tagName;
		this.tagPoints = tagPoints;
		this.tagValidExhibitsList = new ArrayList<Exhibit>();
		tagValidExhibitsList.add(tagValidExhibit);
		this.tagCategory = tagCategory;
		this.hint = tagHint;
	}
	
	/**
	 * Constructor with no exhibits. Testing Case Only!
	 * 
	 * @param tagName
	 * @param tagPoints
	 * @param tagCategory
	 */
	//FIXME
	public Tag(String tagName, int tagPoints, TagCategory tagCategory) {
		super();
		this.tagName = tagName;
		this.tagPoints = tagPoints;
		this.tagValidExhibitsList = new ArrayList<Exhibit>();
		this.tagCategory = tagCategory;
	}
	
	public String getTagName() {
		return tagName;
	}
	
	public int getTagId() {
		return tagId;
	}
	
	public int getTagPoints() {
		return tagPoints;
	}
	
	public TagCategory getTagCategory() {
		return tagCategory;
	}
	
	public Hint getHint() {
		return hint;
	}
	
	public boolean hasHint() {
		return hint.isAvailable();
	}
	
	public boolean isNull() {
		if (tagId == -1) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unused")
	private ArrayList<Exhibit> getTagValidExhibitsList() {
		return tagValidExhibitsList;
	}
	
	public boolean addValidExhibit(Exhibit exhibit) {
		for (Exhibit e : tagValidExhibitsList) {
			if (exhibit.equals(e))
				return false;
		}
		return this.tagValidExhibitsList.add(exhibit);
	}
	
	/**
	 * Converts Color object of tagCategory to an integer
	 * 
	 * @return An integer
	 */
	public int getTagColor() {
		int tagColorValue;
		tagColorValue = tagCategory.getTagCategoryColor();
		return tagColorValue;
	}
	
	/**
	 * Checks if this tag is valid for the exhibitToCheck.
	 * 
	 * @param exhibitToCheck
	 * @return true/false
	 */
	public boolean isValidForExhibit(Exhibit exhibitToCheck) {
		for (Exhibit exhibit : tagValidExhibitsList) {
			if (exhibitToCheck.equals(exhibit))
				return true;
		}
		return false;
	}
	
}
