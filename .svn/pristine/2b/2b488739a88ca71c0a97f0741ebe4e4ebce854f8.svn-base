package hci.projects.taggling.utils;

import hci.projects.taggling.client.R;
import hci.projects.taggling.client.csmessage.ControlMessage;
import hci.projects.taggling.client.csmessage.GameMessage;
import hci.projects.taggling.client.csmessage.ScanMessage;
import hci.projects.tagglinglib.GameHandler;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.util.Log;

import com.google.android.gcm.GCMRegistrar;
import com.google.android.maps.MapView;

public class Utils {
	private static final int COLOR_FLOAT_TO_INT_FACTOR = 255;
	
	public static int RGBToColor(final float pRed, final float pGreen, final float pBlue) {
		return Color.rgb((int) (pRed * COLOR_FLOAT_TO_INT_FACTOR), (int) (pGreen * COLOR_FLOAT_TO_INT_FACTOR),
				(int) (pBlue * COLOR_FLOAT_TO_INT_FACTOR));
	}
	
	public static String usernameToSha1(String username) {
		try {
			MessageDigest md = MessageDigest.getInstance("sha-1");
			md.update(username.getBytes("utf-8"));
			
			byte[] digest = md.digest();
			BigInteger bi = new BigInteger(1, digest);
			return String.format((Locale) null, "%0" + (digest.length * 2) + "x", bi).toLowerCase();
		} catch (NoSuchAlgorithmException e) {
			// never catch this
			return "";
		} catch (UnsupportedEncodingException e) {
			// never catch this
			return "";
		}
	}
	
	/**
	 * So, Google Maps uses a Mercator projection. This means that the further you get from the equator the more distorted the distances
	 * become. According to this discussion, the proper way to convert for distances is something like:
	 * 
	 * @param meters
	 * @param map
	 * @param latitude
	 * @return
	 */
	public static int metersToRadius(float meters, MapView map, double latitude) {
		return (int) (map.getProjection().metersToEquatorPixels(meters) * (1 / Math.cos(Math.toRadians(latitude))));
	}
	
	/**
	 * Broadcasts the new message to all listeners.
	 * <p>
	 * This method is defined in the common helper because it's used both by the UI and the background service.
	 * 
	 * @param context
	 *                application's context.
	 * @param incomingGameMessage
	 *                message to be displayed.
	 */
	public static void broadcastMessage(Context context, GameMessage incomingGameMessage) {
		Intent newIntent;
		newIntent = new Intent(TGConstants.INTENT_NEW_DATA_FROM_SERVER);
		newIntent.putExtra(TGConstants._GAME_ACTIONMESSAGE, incomingGameMessage);
		context.sendBroadcast(newIntent);
	}
	
	public static void broadcastMessage(Context context, ScanMessage incomingScanMessage) {
		Intent newIntent;
		newIntent = new Intent(TGConstants.INTENT_NEW_DATA_FROM_SERVER);
		newIntent.putExtra(TGConstants._GAME_SCANMESSAGE, incomingScanMessage);
		context.sendBroadcast(newIntent);
	}
	
	public static void broadcastMessage(Context context, ControlMessage incomingControlMessage) {
		Intent newIntent;
		newIntent = new Intent(TGConstants.INTENT_NEW_DATA_FROM_SERVER);
		newIntent.putExtra(TGConstants._GAME_CONTROLMESSAGE, incomingControlMessage);
		context.sendBroadcast(newIntent);
	}
	
	public static void broadcastMessage(Context context, String messageType, String incomingMessage) {
		Intent newIntent;
		if (messageType.equals(TGConstants._GCM_ON_DELETED_MESSAGES)) {
			// we want to force the game to update its status, so we treat this as a game message
			newIntent = new Intent(TGConstants.INTENT_NEW_DATA_FROM_SERVER);
		} else {
			newIntent = new Intent(TGConstants.INTENT_GCM_SERVICE_MESSAGE);
		}
		newIntent.putExtra(messageType, incomingMessage);
		context.sendBroadcast(newIntent);
	}
	
	static final String key = "magic_key";
	
	public static String encryptString(String str) {
		StringBuffer sb = new StringBuffer(str);
		
		int lenStr = str.length();
		int lenKey = key.length();
		
		//
		// For each character in our string, encrypt it...
		for (int i = 0, j = 0; i < lenStr; i++, j++) {
			if (j >= lenKey)
				j = 0; // Wrap 'round to beginning of key string.
				
			//
			// XOR the chars together. Must cast back to char to avoid compile error.
			//
			sb.setCharAt(i, (char) (str.charAt(i) ^ key.charAt(j)));
		}
		
		return sb.toString();
	}
	
	public static String decryptString(String str) {
		//
		// To 'decrypt' the string, simply apply the same technique.
		try {
			str = URLDecoder.decode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		str = "00" + encryptString(str);
		int length = str.length();
		str = str.substring(length - 3, length);
		str = str.replaceAll("^0*", "");
		return str;
	}
	
	public static void setRegisteredOnServer(Context context, Boolean registered) {
		GCMRegistrar.setRegisteredOnServer(context, registered);
		if (registered) {
			Prefs.setPrefsName(GameHandler.getInstance().getMyPlayer().getName());
//			Log.d("Utils", "Prefs name set to " + Game.getInstance().getMyPlayerName());
		} else {
			Prefs.setPrefsName(context.getString(R.string.app_name));
//			Log.d("Utils", "Prefs name set to " + context.getString(R.string.app_name));
		}
	}
	
	public static boolean isTablet(Context context) {
		    boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
		    boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
		    return (xlarge || large);
		}
	
}
