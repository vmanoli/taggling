package hci.projects.tagglinglib.GameElements;

import hci.projects.taggling.utils.TGConstants;

import java.math.BigInteger;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;

public class Exhibit {
	public static final int __INVALID_TAG_POINTS__ = -2;
	public static final int __COMPLETED_BONUS_POINTS__ = 100;
	
	public static final boolean LOCKED = true;
	public static final boolean UNLOCKED = false;
	public static final int _NO_EXHIBIT_ID = -1;
	
	/**
	 * The name of the exhibit
	 */
	private String exhibitName;
	
	/**
	 * Unique Id of the exhibit
	 */
	private int exhibitId = _NO_EXHIBIT_ID;
	
	/**
	 * The hex code produced from Name
	 * for qr coding use
	 */
	private String exhibitQRTagValue;
	
	/**
	 * The artist name
	 */
	private String exhibitArtist;
	
	/**
	 * The playerId of the player who has locked this exhibit, otherwise _NO_PLAYER_ID
	 */
	private int lockingPlayerId = TGConstants._NO_PLAYER_ID;
	
	/**
	 * The drawable id of the thumbnail for this exhibit
	 */
	private String exhibitThumbnail;
	
	/**
	 * Maximum number of tags that this exhibit can hold
	 */
	private int exhibitTagSlotsSize;
	
	/**
	 * On which floor of the building you can find the exhibit
	 */
	private int exhibitFloor;
	
	/**
	 * Color of the exhibit is grey if inactive
	 */
	private int exhibitColor;
	
	public ExhibitTagSlots exhibitTagSlots;
	
	/**
	 * Public constructor
	 * 
	 * @param exhibitName
	 * @param exhibitThumbnail
	 * @param exhibitTagSlotsSize
	 * @param exhibitFloor
	 */
	public Exhibit(int exhibitId, String exhibitName, String exhibitArtist, String exhibitThumbnail, int exhibitTagSlotsSize, int exhibitFloor) {
		this.exhibitId = exhibitId;
		this.exhibitName = exhibitName;
		this.exhibitArtist = exhibitArtist;
		this.lockingPlayerId = TGConstants._NO_PLAYER_ID;
		this.exhibitThumbnail = exhibitThumbnail;
		this.exhibitTagSlotsSize = exhibitTagSlotsSize;
		this.exhibitFloor = exhibitFloor;
		this.exhibitTagSlots = new ExhibitTagSlots(exhibitTagSlotsSize);
		this.exhibitColor = Color.BLACK;
		// this.exhibitHexName = toHex();
		this.exhibitQRTagValue = String.format("%040x", new BigInteger(1, this.exhibitName.getBytes()));
	}
	
	/**
	 * @return the exhibitName
	 */
	public String getExhibitName() {
		return exhibitName;
	}
	
	/**
	 * @return the exhibitId
	 */
	public int getExhibitId() {
		return exhibitId;
	}
	
	public String getExhibitArtist() {
		return exhibitArtist;
	}
	
	/**
	 * @return the exhibitThumbnail Recourse Identifier
	 */
	public int getExhibitThumbnail(Context ctx) {
		int resID = ctx.getResources().getIdentifier(exhibitThumbnail, "drawable", ctx.getPackageName());
		return resID;
	}
	
	/**
	 * @return the exhibitFloor
	 */
	public int getExhibitFloor() {
		return exhibitFloor;
	}
	
	/**
	 * @return the exhibitTagSlotsSize
	 */
	public int getExhibitTagSlotsSize() {
		return exhibitTagSlotsSize;
	}
	
	/**
	 * @return the exhibitColor
	 */
	public int getExhibitColor() {
		return exhibitColor;
	}
	
	/**
	 * @param exhibitColor
	 *                the exhibitColor to set
	 */
	public void setExhibitColor(int exhibitColor) {
		this.exhibitColor = exhibitColor;
	}
	
	/***
	 * Checks if there are slots available or null spots
	 * to attach more tags
	 * 
	 * @return true/false
	 */
	public boolean hasAvailableSlots() {
		if (exhibitTagSlots.isEmpty()) {
			return false;
		}
		for (Tag tag : exhibitTagSlots) {
			if (tag.isNull()) {
				return true;
			}
		}
		return false;
	}
	
	/***
	 * Checks if the attached tags are valid. Does not check if exhibit is
	 * complete
	 * 
	 * @return true/false
	 */
	private boolean allTagsAreValid() {
		if (exhibitTagSlots.isEmpty()) {
			return false;
		} else {
			for (Tag tag : exhibitTagSlots) {
				if (tag.isNull()) {
					return false;
				} else {
					if (!tag.isValidForExhibit(this)) {
						return false;
					}
				}
			}
			return true;
		}
	}
	
	/**
	 * Checks if the exhibit is completed and all tags are valid
	 * 
	 * @return
	 */
	public boolean isCompleted() {
		if (allTagsAreValid() && !hasAvailableSlots()) {
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * Checks if this is the Exhibit with the passed hexCode
	 * 
	 * @param hexCode
	 * @return true/false
	 */
	public boolean isExhibitHexCode(String hexCode) {
		if (exhibitQRTagValue.equals(hexCode)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Check if theExhibit is locked and not viewable
	 * 
	 * @return the {@link Player#playerId} of the {@link Player} who has locked this Object. Otherwise, {@link TGConstants#_NO_PLAYER_ID}
	 */
	public int isLocked() {
		return lockingPlayerId;
	}
	
	/**
	 * Check if theExhibit is lockable by {@link Player#playerId}
	 * 
	 * @return true or false
	 */
	public boolean isLockableBy(int playerId) {
		if ((this.lockingPlayerId == playerId) || (this.lockingPlayerId == TGConstants._NO_PLAYER_ID)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Set the lock State of view
	 * 
	 * @param the
	 *                {@link Player#playerId} of the {@link Player} who has locked this Object
	 */
	public void setLocked(int lockingPlayerId) {
		this.lockingPlayerId = lockingPlayerId;
	}
	
	/**
	 * Attaches tag to this exhibit. Return points, 0 if tag is not valid.
	 * 
	 * @param tag
	 * @return
	 */
	public int attachTag(Tag tag) {
		int points = exhibitTagSlots.addTagForPoints(tag, this);
		if (this.isCompleted()) {
			this.exhibitColor = Color.LTGRAY;
			points = points + __COMPLETED_BONUS_POINTS__;
		}
		return points;
	}
	
	/**
	 * Removes tag from this exhibit
	 * 
	 * @param tag
	 * @return true if operation successful
	 */
	public boolean removeTag(Tag tag) {
		return exhibitTagSlots.remove(tag);
	}
	
	/**
	 * An ArrayList of the Tags the Exhibit holds for the moment
	 */
	@SuppressWarnings("serial")
	public class ExhibitTagSlots extends ArrayList<Tag> {
		
		@Override
		public boolean add(Tag tag) {
			int i = indexOf(TGConstants._NO_TAG);
			if (i == -1) {
				return false;
			} else {
				set(i, tag);
				return true;
			}
		}
		
		@Override
		public boolean remove(Object tag) {
			int i = indexOf(tag);
			if (i == -1) {
				return false;
			} else {
				set(i, TGConstants._NO_TAG);
				return true;
			}
		}
		
		@Override
		public void clear() {
			for (int i = 0; i < this.size(); i++) {
				set(i, TGConstants._NO_TAG);
			}
		}
		
		/**
		 * Constructor that instantiates an ArrayList with initial capacity for
		 * better memory allocation and initializes it with null elements
		 * 
		 * @param slotsCapacity
		 */
		public ExhibitTagSlots(int slotsCapacity) {
			super(slotsCapacity);
			for (int i = 0; i < slotsCapacity; i++) {
				super.add(TGConstants._NO_TAG);
			}
		}
		
		/**
		 * Adds a tag on ExhibitTagSlots and checks if it is valid for the
		 * exhibit
		 * 
		 * @param tag
		 * @param exhibitToCheck
		 * @return The points the player gets for adding a valid tag or zero.
		 */
		public int addTagForPoints(Tag tag, Exhibit exhibitToCheck) {
			add(tag);
			if (tag.isValidForExhibit(exhibitToCheck)) {
				return tag.getTagPoints();
			} else {
				return __INVALID_TAG_POINTS__;
			}
		}
	}
	
	public String getExhibitQRTagValue() {
		return this.exhibitQRTagValue;
	}
}
