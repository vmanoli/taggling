package hci.projects.tagglinglib;

import hci.projects.tagglinglib.GameElements.Exhibit;
import hci.projects.tagglinglib.GameElements.Player;
import hci.projects.tagglinglib.GameElements.Tag;
import hci.projects.tagglinglib.GameElements.TagCategory;
import hci.projects.tagglinglib.Messages.Messages;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.res.Resources;
import android.util.Log;

public class GameHandler {
	private static final String DEBUG_TAG = "GameHandler";
	
	private static GameHandler instance;
	
	private Player mPlayer;
	private ArrayList<Exhibit> gameExhibits;
	private ArrayList<TagCategory> gameTagCategories;
	private ArrayList<Tag> gameTags;
	
	public MyColors colors;
	
	public static synchronized GameHandler getInstance() {
		if (instance == null) {
			instance = new GameHandler();
		}
		return instance;
	}
	
	public GameHandler() {
		this.gameExhibits = new ArrayList<Exhibit>();
		this.gameTagCategories = new ArrayList<TagCategory>();
		this.gameTags = new ArrayList<Tag>();
		this.colors = new MyColors();
	}
	
	public void addMyPlayer(Player player) {
		mPlayer = player;
	}
	
	public Player getMyPlayer() {
		return mPlayer ;
	}
	
	public void getResources(Resources res) {
		this.colors.init(res);
	}
	
	// FIXME i should delete
	public void createCategories() {
		
		TagCategory gameCategory1 = new TagCategory(
				Messages.getString("GlobalState.5"), colors.getColor("cyan"), "Περιγράφει τον καλλιτέχνη του εκθέματος"); //$NON-NLS-1$
		TagCategory gameCategory2 = new TagCategory(
				Messages.getString("GlobalState.6"), colors.getColor("pink"), "Περιγράφει την τεχνική με την οποία δημιουργήθηκε το έργο"); //$NON-NLS-1$
		
		gameTagCategories.add(gameCategory1);
		gameTagCategories.add(gameCategory2);
		
	}
	
	/*
	 * * * GETTERS * * * * * * * * * * * * * * * * * *
	 * *
	 * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	
	public ArrayList<Exhibit> getGameExhibits() {
		return gameExhibits;
	}
	
	public ArrayList<Tag> getGameTags() {
		return gameTags;
	}
	
	public ArrayList<TagCategory> getGameTagCategories() {
		return gameTagCategories;
	}
	
	/**
	 * Gets a specific exhibit from the list
	 * 
	 * @param selectedExhibitName
	 * @return Exhibit or null if it doesn't exist
	 */
	public Exhibit getExhibit(String selectedExhibitName) {
		// Determine the selected exhibit
		//
		for (Exhibit e : gameExhibits) {
			if (e.getExhibitName().equals(selectedExhibitName)) {
				return e;
			}
		}
		return null;
	}

	/**
	 * Gets the exhibit with the specific HexCode
	 * 
	 * @return Exhibit or null if it doesn't exist
	 */
	public Exhibit getExhibitByHex(String hexCode) {
		for (Exhibit e : gameExhibits) {
			if (e.isExhibitHexCode(hexCode)) {
				return e;
			}
		}
		return null;
	}
	

	/**
	 * Gets the exhibit with the specific oid
	 * 
	 * @return Exhibit or null if it doesn't exist
	 */
	public Exhibit getExhibitById(int exhibitId) {
		for (Exhibit e : gameExhibits) {
			if (e.getExhibitId()==exhibitId) {
				return e;
			}
		}
		return null;
	}
	
	public Player getPlayer() {
		return mPlayer;
	}
	
	/*
	 * * * JSON HELPER - ADDERS * * * * * * * * * * * * * * * * * *
	 * Adding new game elements as extracted by the json content *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	
	public boolean addNewExhibit(int eId, String eName, String eThumbnail, int eSlotSize, int eFloor) {
		// check valid arguments
		if (eName == null || eThumbnail == null || eSlotSize <= 0 || eFloor < 1 || eFloor > 3) { // floor range = 1, 2, 3
			return false;
		} else {
			return gameExhibits.add(new Exhibit(eId, eName, eThumbnail, eSlotSize, eFloor));
		}
	}
	
	public boolean addNewCategory(String cName, String cColorString, String cDescription) {
		// check valid arguments
		return gameTagCategories.add(new TagCategory(cName, colors.getColor(cColorString), cDescription));
	}
	
	public boolean addNewTag(int tId, String tName, int tPoints, Integer[] tValidList, int tCategory) {
		int l = gameExhibits.size();
		
		// check valid arguments
		if (tName == null || tPoints < 0 || tValidList.length == 0 || // check valid exhibit list is not empty
				tCategory >= gameTagCategories.size()) // check category number is a valid index
		{
			return false;
		}
		
		// check constructor case
		if (tValidList.length == 1) {
			// Use constructor with one exhibit
			return gameTags.add(new Tag(tId, tName, tPoints, gameExhibits.get(tValidList[0]), gameTagCategories.get(tCategory)));
		} else {
			// Use constructor with list of exhibits
			ArrayList<Exhibit> tempExhibitsList = new ArrayList<Exhibit>();
			for (int i : tValidList) {
				// check exhibit number is a valid index
				if (i < 0 || i >= l) {
					Log.e(DEBUG_TAG + " TAG ADDER", "OutofBounds tValidList index");
				} else {
					tempExhibitsList.add(gameExhibits.get(i));
				}
			}
			return gameTags.add(new Tag(tId, tName, tPoints, tempExhibitsList, gameTagCategories.get(tCategory)));
		}
	}
	
	/*
	 * * * MESSAGE HELPER - UPDATERS * * * * * * * * * * * * * * * * *
	 * Updating game elements as instructed by the messages received *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	
	/*
	 * Update the tags in an ExhibitSlot with the new tags it should hold
	 * according to the indexes sent by a MessageUpdate
	 */
	public String updateExhibitTagSlots(int exhibitIndex, Integer[] iTags) {
		Log.d(DEBUG_TAG, "Exhibit: " + Integer.toString(exhibitIndex) + " iTags: " + Arrays.toString(iTags));
		if (exhibitIndex < 0 || exhibitIndex > (gameExhibits.size() - 1)) {
			Log.e(DEBUG_TAG, "NotFoundExhibit");
			return "NotFoundExhibit";
		}
		Exhibit e = gameExhibits.get(exhibitIndex);
		e.exhibitTagSlots.clear();
		for (int i : iTags) {
			if (i > gameTags.size() - 1)
				continue;
			if (i == -1) {
				e.exhibitTagSlots.add(null);
				continue;
			}
			e.exhibitTagSlots.add(gameTags.get(i));
		}
		e.setLocked(false);
		Log.d(DEBUG_TAG, e.toString());
		return e.getExhibitName();
	}
	
	/*
	 * Update the Player Inventory with the new tags it should hold
	 * according to the indexes sent by a MessageUpdate
	 */
	public void updatePlayerSlots(Integer[] iTags) {
		Log.d(DEBUG_TAG, mPlayer.inventory.toString());
		Log.d(DEBUG_TAG, Arrays.toString(iTags));
		// valid check
		if (iTags.length > mPlayer.inventory.size()) {
			Log.e(DEBUG_TAG, "Int array NOT VALID: Size > Inventory");
			return;
		}
		// clear inventory
		mPlayer.inventory.clear();
		// parse iTags and add
		for (int i : iTags) {
			if (i < 0 || i > (gameTags.size() - 1)) // valid check
				continue;
			mPlayer.inventory.add(gameTags.get(i));
		}
	}
	
	/*
	 * Get an array of the tags included in the Players Inventory
	 * right now to sent them over to others via a MessageUpdate
	 */
	public Integer[] getInventoryUpdate() {
		Integer[] iTags = new Integer[mPlayer.inventory.size()];
		int i = 0;
		for (Tag tag : mPlayer.inventory) {
			iTags[i] = gameTags.indexOf(tag);
			i++;
		}
		return iTags;
	}
	
	/*
	 * Sets tags to the exhibit slots
	 * as specified by the initialization message
	 * on every game session started
	 */
	public void initExhibitSlots(Integer[][] multiArr) {
		for (int row = 0; row < multiArr.length; row++) {
			if (row < 0 || row > (gameExhibits.size() - 1))
				continue;
			placeTagsToExhibit(row, multiArr[row]);
		}
	}
	
	/*
	 * * * GENERAL UTILITIES * * * * * * * * * * * * * * * * * *
	 * Functions that help with some core functions of the game *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	
	/**
	 * For initialization purposes
	 * 
	 * @param iExhibit
	 *                index
	 * @param iTags
	 *                index array
	 */
	public void placeTagsToExhibit(int iExhibit, Integer[] iTags) {
		// check valid arguments
		for (int i : iTags) {
			if (i < 0 || i > (gameTags.size() - 1))
				continue;
			gameExhibits.get(iExhibit).exhibitTagSlots.add(gameTags.get(i));
		}
	}
	
	/**
	 * Is it the END of the game?
	 * Check if the ALL the exhibits in the game are completed
	 * 
	 * @return true/false
	 */
	public boolean isGameCompleted() {
		for (Exhibit e : gameExhibits) {
			if (!e.isCompleted()) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Clears all the previous initializations.
	 * Slots of exhibits set to null
	 */
	public void clearAllSlots() {
		for (Exhibit e : gameExhibits) {
			e.exhibitTagSlots.clear();
		}
		// TODO also for the player inventory?
	}
	
	/*
	 * Clears the GameHandler of all previously added data
	 * in the private game elements
	 */
	public void clear() {
		mPlayer = new Player("Giakoumis");
		gameExhibits = new ArrayList<Exhibit>();
		gameTagCategories = new ArrayList<TagCategory>();
		gameTags = new ArrayList<Tag>();
	}
}
