package hci.projects.tagglinglib.GameElements;

import hci.projects.taggling.utils.TGConstants;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

public class Exhibit {
	public static final int __INVALID_TAG_POINTS__ = -1;
	public static final int __COMPLETED_BONUS_POINTS__ = 10;
	
	public static final boolean LOCKED = true;
	public static final boolean UNLOCKED = false;
	public static final int _NO_EXHIBIT_ID = -1;
	public static final String _NO_EXHIBIT_NAME = "";
	public static final String _NO_EXHIBIT_QR_TAG_VALUE = "";
	public static final String _NO_EXHIBIT_ARTIST = "";
	public static final int _NO_EXHIBIT_FLOOR = -1;
	public static final int _NO_EXHIBIT_COLOR = Color.BLACK;
	public static final Exhibit _NO_EXHIBIT = new Exhibit(_NO_EXHIBIT_ID, _NO_EXHIBIT_NAME, _NO_EXHIBIT_ARTIST, "", 0, _NO_EXHIBIT_FLOOR,
			_NO_EXHIBIT_QR_TAG_VALUE);
	public static final int __EXHIBIT_MAXSLOTS__ = 5;
	
	/**
	 * @author chris
	 *         An Exhibit can can have tags attached to it. A tag can be either correct (valid), incorrect or no tag.
	 */
	public enum TagStatus {
		VALID, NONVALID, NULL
	}
	
	/**
	 * The name of the exhibit
	 */
	private String exhibitName = _NO_EXHIBIT_NAME;
	
	/**
	 * Unique Id of the exhibit
	 */
	private int exhibitId = _NO_EXHIBIT_ID;
	
	/**
	 * The hex code produced from Name
	 * for qr coding use
	 */
	private String exhibitQRTagValue = _NO_EXHIBIT_QR_TAG_VALUE;
	
	/**
	 * The artist name
	 */
	private String exhibitArtist = _NO_EXHIBIT_ARTIST;
	
	/**
	 * The playerId of the player who has locked this exhibit, otherwise _NO_PLAYER_ID
	 */
	private int lockingPlayerId = TGConstants._NO_PLAYER_ID;
	
	/**
	 * The drawable id of the thumbnail for this exhibit
	 */
	private String exhibitThumbnail; // FIXME?
	
	/**
	 * Maximum number of tags that this exhibit can hold
	 */
	private int exhibitTagSlotsSize;
	
	/**
	 * On which floor of the building you can find the exhibit
	 */
	private int exhibitFloor = _NO_EXHIBIT_FLOOR;
	
	/**
	 * Color of the exhibit is grey if inactive
	 */
	private int exhibitColor = _NO_EXHIBIT_COLOR;
	
	private ExhibitTagSlots exhibitTagSlots;
	
	/**
	 * Public constructor
	 * 
	 * @param exhibitName
	 * @param exhibitThumbnail
	 * @param exhibitTagSlotsSize
	 * @param exhibitFloor
	 */
	public Exhibit(int exhibitId, String exhibitName, String exhibitArtist, String exhibitThumbnail, int exhibitTagSlotsSize, int exhibitFloor,
			String exhibitIdentity) {
		this.exhibitId = exhibitId;
		this.exhibitName = exhibitName;
		this.exhibitArtist = exhibitArtist;
		this.lockingPlayerId = TGConstants._NO_PLAYER_ID;
		this.exhibitThumbnail = exhibitThumbnail;
		this.exhibitTagSlotsSize = exhibitTagSlotsSize;
		this.exhibitFloor = exhibitFloor;
		this.exhibitTagSlots = new ExhibitTagSlots(exhibitTagSlotsSize);
		this.exhibitColor = Color.BLACK;
		this.exhibitQRTagValue = exhibitIdentity;
	}
	
	/**
	 * @return the exhibitName
	 */
	public String getExhibitName() {
		return exhibitName;
	}
	
	/**
	 * @return the exhibitId
	 */
	public int getExhibitId() {
		return exhibitId;
	}
	
	public String getExhibitArtist() {
		return exhibitArtist;
	}
	
	/**
	 * @return the exhibitThumbnail Recourse Identifier
	 */
	public int getExhibitThumbnail(Context ctx) {
		// FIXME
		int resID = ctx.getResources().getIdentifier(exhibitThumbnail, "drawable", ctx.getPackageName());
		return resID;
	}
	
	/**
	 * @return the exhibitFloor
	 */
	public int getExhibitFloor() {
		return exhibitFloor;
	}
	
	/**
	 * @return the exhibitTagSlotsSize
	 */
	public int getExhibitTagSlotsSize() {
		return exhibitTagSlotsSize;
	}
	
	/**
	 * @return the exhibitColor
	 */
	public int getExhibitColor() {
		return exhibitColor;
	}
	
	/**
	 * @param exhibitColor
	 *                the exhibitColor to set
	 */
	public void setExhibitColor(int exhibitColor) {
		this.exhibitColor = exhibitColor;
	}
	
	/*
	 * Update the tags in an ExhibitSlot with the new tags it should hold
	 * according to the indexes sent by a MessageUpdate
	 */
	public void setTags(TagList newTagList) {
		exhibitTagSlots.clear();
		this.exhibitTagSlots.clearAndAddAll(newTagList);
		this.setLocked(TGConstants._NO_PLAYER_ID);// FIXME: Γιατί;
	}
	
	/**
	 * @return the exhibitTagSlots
	 */
	public ExhibitTagSlots getExhibitTagSlots() {
		return exhibitTagSlots;
	}
	
	/*
	 * Get an array of the tags included in the exhibit's slots
	 * right now to sent them over to others via a MessageUpdate
	 */
	public Integer[] getExhibitSlotsUpdate() {
		Integer[] iTags = new Integer[getExhibitTagSlots().size()];
		int i = 0;
		for (Tag tag : getExhibitTagSlots()) {
			iTags[i] = tag.getTagId();
			i++;
		}
		Log.d("ExhibitUpdate", Arrays.toString(iTags));
		
		return iTags;
	}
	
	/***
	 * Checks if there are slots available or null spots
	 * to attach more tags
	 * 
	 * @return true/false
	 */
	public boolean hasAvailableSlots() {
		if (exhibitTagSlots.isEmpty()) {// FIXME: Δεν έπρεπε να είναι ανάποδα;
			return false;
		}
		for (Tag tag : exhibitTagSlots) {
			if (tag.isNull()) {
				return true;
			}
		}
		return false;
	}
	
	/***
	 * Checks if the attached tags are valid. Does not check if exhibit is
	 * complete
	 * 
	 * @return true/false
	 */
	private boolean allTagsAreValid() {
		if (exhibitTagSlots.isEmpty()) {// FIXME: Δεν έπρεπε να είναι ανάποδα;
			return false;
		} else {
			for (Tag tag : exhibitTagSlots) {
				if (tag.isNull()) {
					return false;
				} else if (!tag.isValidForExhibit(this)) {
					return false;
				}
			}
			return true;
		}
	}
	
	/**
	 * Checks if the exhibit is completed and all tags are valid
	 * 
	 * @return
	 */
	public boolean isCompleted() {
		if (allTagsAreValid() && !hasAvailableSlots()) {
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * Checks if this is the Exhibit with the passed hexCode
	 * 
	 * @param hexCode
	 * @return true/false
	 */
	public boolean isExhibitHexCode(String hexCode) {
		if (exhibitQRTagValue.equals(hexCode)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Check if theExhibit is locked and not viewable
	 * 
	 * @return the {@link Player#playerId} of the {@link Player} who has locked this Object. Otherwise, {@link TGConstants#_NO_PLAYER_ID}
	 */
	public int isLocked() {
		return lockingPlayerId;
	}
	
	/**
	 * Check if theExhibit is lockable by {@link Player#playerId}
	 * 
	 * @return true or false
	 */
	public boolean isLockableBy(int playerId) {
		if ((this.lockingPlayerId == playerId) || (this.lockingPlayerId == TGConstants._NO_PLAYER_ID)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Set the lock State of view
	 * 
	 * @param the
	 *                {@link Player#playerId} of the {@link Player} who has locked this Object
	 */
	public void setLocked(int lockingPlayerId) {
		this.lockingPlayerId = lockingPlayerId;
	}
	
	/**
	 * Attaches tag to this exhibit. Return points, 0 if tag is not valid.
	 * 
	 * @param tag
	 * @return
	 */
	public int attachTag(Tag tag) {
		if (tag == null) {
			tag = Tag._NO_TAG;
		}
		int points = exhibitTagSlots.addTagForPoints(tag, this);
		if (this.isCompleted()) {
			this.exhibitColor = Color.LTGRAY;
			points = points + __COMPLETED_BONUS_POINTS__;
		}
		return points;
	}
	
	/**
	 * Removes tag from this exhibit
	 * 
	 * @param tag
	 * @return true if operation successful
	 */
	public int removeTag(Tag tag) {
		if (tag == null) {
			tag = Tag._NO_TAG;
		}
		return exhibitTagSlots.removeTagForPoints(tag, this);
		// exhibitTagSlots.remove(tag);
	}
	
	/**
	 * An ArrayList of the Tags the Exhibit holds for the moment
	 */
	@SuppressWarnings("serial")
	public class ExhibitTagSlots extends TagList {
		
		@Override
		public boolean add(Tag tag) {
			int i = indexOf(Tag._NO_TAG);
			if (i == -1) {
				return false;
			} else {
				set(i, tag);
				return true;
			}
		}
		
		@Override
		public boolean remove(Object tag) {
			int i = indexOf(tag);
			if (i == -1) {
				return false;
			} else {
				set(i, Tag._NO_TAG);
				return true;
			}
		}
		
		@Override
		public void clear() {
			for (int i = 0; i < this.size(); i++) {
				set(i, Tag._NO_TAG);
			}
		}
		
		/**
		 * Constructor that instantiates an ArrayList with initial capacity for
		 * better memory allocation and initializes it with null elements
		 * 
		 * @param slotsCapacity
		 */
		public ExhibitTagSlots(int slotsCapacity) {
			super(slotsCapacity);
			for (int i = 0; i < slotsCapacity; i++) {
				super.add(Tag._NO_TAG);
			}
		}
		
		/**
		 * Adds a tag on ExhibitTagSlots and checks if it is valid for the
		 * exhibit
		 * 
		 * @param tag
		 * @param exhibitToCheck
		 * @return The points the player gets for adding a valid tag or zero.
		 */
		protected int addTagForPoints(Tag tag, Exhibit exhibitToCheck) {
			add(tag);
			if (tag.isValidForExhibit(exhibitToCheck)) {
				return tag.getTagPoints();
			} else {
				return __INVALID_TAG_POINTS__;
			}
		}
		
		/**
		 * Adds a tag on ExhibitTagSlots and checks if it is valid for the
		 * exhibit
		 * 
		 * @param tag
		 * @param exhibitToCheck
		 * @return The points the player gets for adding a valid tag or zero.
		 */
		protected int removeTagForPoints(Tag tag, Exhibit exhibitToCheck) {
			remove(tag);
			if (tag.isValidForExhibit(exhibitToCheck)) {
				return tag.getTagPoints();
			} else {
				return __INVALID_TAG_POINTS__;
			}
		}
	}
	
	public String getExhibitQRTagValue() {
		return this.exhibitQRTagValue;
	}
	
	public String toString() {
		return this.exhibitName + "(" + String.valueOf(exhibitId) + ");";
	}
}
