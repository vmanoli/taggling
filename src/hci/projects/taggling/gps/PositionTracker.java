package hci.projects.taggling.gps;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import hci.projects.taggling.utils.TGConstants;

public class PositionTracker {
	
	private static final String TAG = "PositionTracker";

	private IGPSActivity main;
	
	// Helper for GPS-Position
	private LocationListener mlocListener;
	private LocationManager mlocManager;
	
	private boolean isRunning;
	private Criteria crta;
	
	public PositionTracker(IGPSActivity main) {
		this.main = main;
		
		// GPS Position
		mlocListener = new MyLocationListener();
		startTracking();
	}
	
	private void startTracking() {
		mlocManager = (LocationManager) ((Activity) this.main).getSystemService(Context.LOCATION_SERVICE);
		crta = getLocationManagerCriteria();
		String provider = mlocManager.getBestProvider(crta, true);
		if (provider != null) {
			mlocManager.requestLocationUpdates(mlocManager.getBestProvider(crta, true), TGConstants.MINIMUM_TIME_BETWEEN_UPDATE,
					TGConstants.MINIMUM_DISTANCECHANGE_FOR_UPDATE, mlocListener);
			this.isRunning = true;
			Log.i(TAG, "start tracking position");
		} else {
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			((Activity) this.main).startActivity(intent);
		}
	}
	
	private Criteria getLocationManagerCriteria() {
		int sdk = Integer.valueOf(Build.VERSION.SDK);
		Criteria crta = new Criteria();
		
		if (sdk >= 9)
			crta.setAccuracy(Criteria.ACCURACY_MEDIUM);
		else
			crta.setAccuracy(Criteria.ACCURACY_COARSE);
		crta.setAltitudeRequired(false);
		crta.setBearingRequired(false);
		crta.setCostAllowed(false);
		return crta;
	}
	
	public void stopTracking() {
		if (isRunning) {
			Log.i(TAG, "stop tracking position");
			mlocManager.removeUpdates(mlocListener);
			this.isRunning = false;
		}
	}
	
	public void resumeTracking() {
		startTracking();
		this.isRunning = true;
	}
	
	public boolean isRunning() {
		return this.isRunning;
	}
	
	public class MyLocationListener implements LocationListener {
		
		private final String TAG = MyLocationListener.class.getSimpleName();
		
		@Override
		public void onLocationChanged(Location loc) {
			PositionTracker.this.main.locationChanged(loc.getLongitude(), loc.getLatitude(), loc.getAccuracy());
		}
		
		@Override
		public void onProviderDisabled(String provider) {
			PositionTracker.this.main.displayGPSSettingsDialog();
		}
		
		@Override
		public void onProviderEnabled(String provider) {
			
		}
		
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			
		}
		
	}
	
}