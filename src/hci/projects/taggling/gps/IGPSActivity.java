package hci.projects.taggling.gps;

public interface IGPSActivity {
	public void locationChanged(double longitude, double latitude, float accuracy);
	
	public void displayGPSSettingsDialog();
}