package hci.projects.taggling.utils;

import hci.projects.taggling.R;
import android.content.Context;
import android.content.SharedPreferences;

public class Prefs {
	public static String prefsName;
	private static final String TAG = "Prefs";
	private static final String _loginUsername = "loginUsername";
	private static final String _loginPassword = "loginPassword";
	private static final String _loginRememberMe = "loginRememberMe";
	private static final String _pastScannedIds = "pastScannedIds"; //FIXME Αν αλλαζει ο χρήστης βλεπει να αποθηκευμένα του παλιού
	private static final String _lastSelectedObjectId = "lastSelectedObjectId";
	
	public static void setPrefsName(String _prefsName) {
		Prefs.prefsName = _prefsName;
	}
	
	// public static void setPlayername(String username) {
	// setString(TGConstants._TAG_USER, username);
	// }
	//
	// public static String getPlayername() {
	// return get().getString(TGConstants._TAG_USER, null);
	// }
	
	public static void setSessionId(int sessionId) {
		setInt(TGConstants._TAG_SESSION_ID, sessionId);
	}
	
	public static int getSessionId() {
		return get().getInt(TGConstants._TAG_SESSION_ID, 0);
	}
	
	public static String getLoginUserName() {
		return get().getString(_loginUsername, null);
	}
	
	public static void setLoginUserName(String loginUsername) {
		setString(_loginUsername, loginUsername);
	}
	
	public static String getLoginPassword() {
		return get().getString(_loginPassword, null);
	}
	
	public static void setLoginPassword(String loginPassword) {
		setString(_loginPassword, loginPassword);
	}
	
	public static Boolean getLoginRememberMe() {
		return get().getBoolean(_loginRememberMe, false);
	}
	
	public static void setLoginRememberMe(Boolean loginRememberMe) {
		setBoolean(_loginRememberMe, loginRememberMe);
	}
	
	// public static void setPastScannedIds(ArrayList<Integer> pastScannedIds) {
	// saveArray(pastScannedIds, _pastScannedIds);
	// }
	
	public static void setLastSelectedObjectId(int objectId) {
		setInt(_lastSelectedObjectId, objectId);
	}
	
	public static int getLastSelectedObjectId() {
		return get().getInt(_lastSelectedObjectId, 3);
	}
	
	public static void setObjectIdAsScanned(int objectId) {
		String pastScannedIds = getPastScannedIds();
		setString(_pastScannedIds, pastScannedIds + "," + String.valueOf(objectId));
	}
	
	public static String getPastScannedIds() {
		return get().getString(_pastScannedIds, "");
	}
	
	public static SharedPreferences get() {
		SharedPreferences sharedPreferences = null;
		if (prefsName != null) {
			sharedPreferences = Globals.getInstance().getContext().getSharedPreferences(prefsName, Context.MODE_PRIVATE);
		} else {
			sharedPreferences = Globals.getInstance().getContext().getSharedPreferences(Globals.getInstance().getContext().getString(R.string.app_name), Context.MODE_PRIVATE);
		}
		return sharedPreferences;
	}
	
	private static void setString(String key, String value) {
		if (key != null) {
			SharedPreferences prefs = Prefs.get();
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString(key, value);
			editor.commit();
		}
	}
	
	private static void setInt(String key, int value) {
		if (key != null) {
			SharedPreferences prefs = Prefs.get();
			SharedPreferences.Editor editor = prefs.edit();
			editor.putInt(key, value);
			editor.commit();
		}
	}
	
	private static void setBoolean(String key, boolean value) {
		if (key != null) {
			SharedPreferences prefs = Prefs.get();
			SharedPreferences.Editor editor = prefs.edit();
			editor.putBoolean(key, value);
			editor.commit();
		}
	}
}