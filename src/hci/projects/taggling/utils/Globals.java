package hci.projects.taggling.utils;

import java.io.File;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Globals {
	
	private static Globals globals;
	
	private String currentSession;
	
	public Context context;
	private float scale;
	private int density;
	
	private Globals() {
	}
	
	public static Globals getInstance() {
		if (globals == null) {
			globals = new Globals();
		}
		return globals;
	}
	
	public Context getContext() {
		return context;
	}
	
	public void setContext(Context context) {
		this.context = context;
	}
	
	public boolean isConnected() {
		ConnectivityManager connec = (ConnectivityManager) Globals.getInstance().context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connec.getActiveNetworkInfo();
		if (networkInfo == null)
			return false;
		if (networkInfo.isConnected()) {
			return true;
		} else {
			return false;
		}
		// for (int i = 0; i < networks.length; i++) {
		// if (networks[i].isConnected()) {
		// return true;
		// }
		// }
		// return isConnected;
	}
	
	public File getApplicationStorageDir() {
		File cacheDir;
		String sdState = android.os.Environment.getExternalStorageState();
		if (sdState.equals(android.os.Environment.MEDIA_MOUNTED)) {
			cacheDir = context.getExternalCacheDir();
		} else
			cacheDir = context.getCacheDir();
		if (!cacheDir.exists()) {
			cacheDir.mkdirs();
		}
		return cacheDir;
		
		// File SDCardRoot = Environment.getExternalStorageDirectory();
		// File appDirectory = new File(SDCardRoot,
		// context.getString(R.string.app_name));
		// return appDirectory;
	}
	
	public String getCurrentSessionName() {
		if (currentSession != null)
			return currentSession.split("@")[0];
		else
			return "no session";
	}
	
	public void setCurrentSession(String session) {
		currentSession = session;
	}
	
	public void setDisplayScale(float scale) {
		boolean isTablet = Utils.isTablet(context);
		if (isTablet) {
			scale = scale * 2.5f;
		}
		this.scale = scale;
	}
	
	public float getDisplayScale() {
		return scale;
	}
	
	public void setDisplayDensity(int density) {
		this.density = density;
	}
	
	public int getDisplayDensity() {
		return density;
	}
}