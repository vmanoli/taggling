package hci.projects.taggling.ListLoaders;

import hci.projects.taggling.R;
import hci.projects.taggling.Adapters.CategoryListAdapter;
import hci.projects.taggling.Adapters.CategoryListAdapter.Options;
import hci.projects.tagglinglib.GameHandler;
import android.os.Build;
import android.os.Bundle;
import android.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;

public class CategoryListLoader extends ListFragment {
	private View mList;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			   getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
			}
		
		// Set the inflater
		mList = inflater.inflate(R.layout.categories_list_view, null);
		
		// Set the activity title according to exhibit name
		//
		getActivity().getActionBar().setTitle(R.string.frag_name_categories);



		ListAdapter arrAdd = new CategoryListAdapter(getActivity(), R.layout.category_list_item, 
				GameHandler.getInstance().getGameTagCategories(), Options.SHOW_DESCRIPTION);
		setListAdapter(arrAdd);
		setHasOptionsMenu(true);
		return mList;
	}

//	@Override
//    public void onPrepareOptionsMenu(Menu menu) {

//		MenuItem hiddenMenuItem = menu.findItem(R.id.action_categories);
//	    hiddenMenuItem.setVisible(false);
//    }

//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//		case android.R.id.home:
//			NavUtils.navigateUpFromSameTask(this);
//			return true;
//		case R.id.action_show_player_points:
//			int points = ((GlobalState) getApplicationContext()).getPlayer()
//					.getPlayerPoints();
//			Toast.makeText(this, "����� " + points + " �������",
//					Toast.LENGTH_LONG).show();
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}


}
