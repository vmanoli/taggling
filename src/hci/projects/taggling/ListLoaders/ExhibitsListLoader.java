package hci.projects.taggling.ListLoaders;

/**
 * @author viky
 */

import java.util.ArrayList;

import hci.projects.taggling.R;
import hci.projects.taggling.Adapters.ExhibitListAdapter;
import hci.projects.tagglinglib.GameHandler;
import hci.projects.tagglinglib.GameElements.Exhibit;
import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class ExhibitsListLoader extends ListFragment {
	public final static String PRESSED_EXHIBIT = "hci.projects.taggling.PASSED_EXHIBIT";
	private static final String TAG = "FLOOR_TAB_PRESSED";
	private View mList;
	int floor = 1;
	ArrayList<Exhibit> tempList = new ArrayList<Exhibit>();
	
	// Communication Methods
	OnSelectedListener mCallback;
	
	// Container Activity must implement this interface
	public interface OnSelectedListener {
		public void onExhibitSelected(int selectedExhibitId);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (OnSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnExhibitSelectedListener");
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// GlobalState state = (GlobalState) getActivity().getApplicationContext();
		
		// Determine the tab pressed to show the right floor exhibits
		String floor_string = getArguments().getString(TAG);
		floor = whichFloor(floor_string);
		
		// Create a list of the exhibits of the selected floor
		for (Exhibit e : GameHandler.getInstance().getGameExhibits()) {
			if (e.getExhibitFloor() == floor) {
				tempList.add(e);
			}
		}
		
		// Inflate the View with the ListAdapter
		mList = inflater.inflate(R.layout.exhibits_list_view, null);
		ListAdapter arrAdd = new ExhibitListAdapter(getActivity(), R.layout.exhibits_list_item, tempList);
		setListAdapter(arrAdd);
		
		return mList;
	}
	
	/**
	 * On exhibit clicked open it's tags list view
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
		Object o = this.getListAdapter().getItem(position);
		int selectedExhibitId = ((Exhibit) o).getExhibitId();
		
		// Send the event to the host activity
		mCallback.onExhibitSelected(selectedExhibitId);
		
	}
	
	/**
	 * Determine which floor was selected via tab
	 * 
	 * @return int floor
	 */
	private int whichFloor(String floor_string) {
		
		if (floor_string.equals("orofos1")) {
			return 1;
		}
		if (floor_string.equals("orofos2")) {
			return 2;
		}
		if (floor_string.equals("orofos3")) {
			return 3;
		}
		return 1;
	}
	
}
