//package hci.projects.taggling.ListLoaders;
//
//import hci.projects.taggling.R;
//import hci.projects.taggling.Adapters.PointsTableListAdapter;
//import hci.projects.tagglinglib.GameHandler;
//import android.app.ListFragment;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ListAdapter;
//
//public class SimpleListLoader_PointsTable extends ListFragment {
//	private View mList;
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		
//		// Set the inflater
//		mList = inflater.inflate(R.layout.simple_list_view, null);
//
//
//		ListAdapter arrAdd = new PointsTableListAdapter(getActivity(), R.layout.pointstable_list_item,
//				GameHandler.getInstance().getPlayer().getPointsTable());
//		setListAdapter(arrAdd);
//
//		return mList;
//	}
//}
