///**
// * 
// */
//package hci.projects.taggling.Adapters;
//
//import hci.projects.taggling.R;
//import hci.projects.tagglinglib.GameElements.Player.PointsExhibitPair;
//import java.util.List;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.TextView;
///**
// * @author viky
// *
// */
//public class PointsTableListAdapter extends ArrayAdapter<PointsExhibitPair> {
//	//private Context ctx;
//	private List<PointsExhibitPair> pairList;
//	private int pointsPairViewItem;
//	
//	/**
//	 * 
//	 * @param context
//	 * @param pointsPairViewItem
//	 * @param pairList
//	 */
//	public PointsTableListAdapter(Context context, int pointsPairViewItem, List<PointsExhibitPair> pairList) {
//		super(context, pointsPairViewItem, pairList);
//    	//this.ctx = context;
//		this.pairList = pairList;
//		this.pointsPairViewItem = pointsPairViewItem;
//	}
//	
//	/**
//	 * 
//	 * @param exhibitName
//	 * @param pointsTaken
//	 *
//	 */
//	public static class ViewHolder {
//		public TextView exhibitName;
//		public TextView pointsTaken;
//	}
//	
//	@Override
//	public View getView(int position, View v, ViewGroup parent) {
//		ViewHolder viewHolder;
//		PointsExhibitPair pep = pairList.get(position);
//		if (v == null) {
//			LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//			v = li.inflate(pointsPairViewItem, parent, false);
//			
//			viewHolder = new ViewHolder();
//			viewHolder.exhibitName = (TextView) v.findViewById(R.id.points_exhibit_name);
//			viewHolder.pointsTaken = (TextView) v.findViewById(R.id.points_number);
//			v.setTag(viewHolder);
//		} else {
//			viewHolder = (ViewHolder) v.getTag();
//		}
//		
//		
//		if (pep != null) {
//			viewHolder.exhibitName.setText(pep.exhibitName);
//			viewHolder.pointsTaken.setText(Integer.toString(pep.points));
//		} 
//		return v;
//	}
//}