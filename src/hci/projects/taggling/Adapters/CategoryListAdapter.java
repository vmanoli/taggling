/**
 * 
 */
package hci.projects.taggling.Adapters;

import hci.projects.taggling.R;
import hci.projects.tagglinglib.GameElements.TagCategory;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
/**
 * @author viky
 */
public class CategoryListAdapter extends ArrayAdapter<TagCategory> {
	@SuppressWarnings("unused")
	private Context ctx;
	private List<TagCategory> tagCatList;
	private int listViewItem;
	private Options option;
	
	public enum Options {
		SHOW_DESCRIPTION, DONT_SHOW_DESCRIPTION, NULL
	}
	/**
	 * 
	 * @param context
	 * @param listViewItem
	 * @param tagList
	 */
	public CategoryListAdapter(Context context, int listViewItem, List<TagCategory> tagCatList, Options option) {
		super(context, listViewItem, tagCatList);
    	this.ctx = context;
		this.tagCatList = tagCatList;
		this.listViewItem = listViewItem;
		this.option = option;
	}
	
	/**
	 * 
	 * @param categoryName
	 * @param tagTextColor
	 *
	 */
	public static class ViewHolder {
		public TextView categoryName;
		public TextView categoryDescription;
		public View categoryColor;
	}
	
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		ViewHolder viewHolder;
		TagCategory tagCategory = tagCatList.get(position);
		if (v == null) {
			LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = li.inflate(listViewItem, parent, false);
			
			viewHolder = new ViewHolder();
			viewHolder.categoryName = (TextView) v.findViewById(R.id.category_name);
			viewHolder.categoryDescription = (TextView) v.findViewById(R.id.category_description);
			viewHolder.categoryColor = (View) v.findViewById(R.id.category_color);
			v.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) v.getTag();
		}
		
		
		if (tagCategory != null) {
			viewHolder.categoryName.setText(tagCategory.getTagCategoryName());
			viewHolder.categoryColor.setBackgroundColor(tagCategory.getTagCategoryColor());
			if (option == Options.SHOW_DESCRIPTION) {
				viewHolder.categoryDescription.setText(tagCategory.getTagCategoryDescription());
			} else {
				viewHolder.categoryDescription.setVisibility(View.GONE);
			}
		}
		return v;
	}
}