package hci.projects.taggling.Adapters;

import java.util.List;

import hci.projects.taggling.GlobalState;
import hci.projects.taggling.R;
import hci.projects.tagglinglib.GameElements.Exhibit;
import hci.projects.tagglinglib.GameElements.Tag;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ExhibitListAdapter extends ArrayAdapter<Exhibit> {
	private static final String TAG = "ExhibitListAdapter";
	private Context ctx;
	private List<Exhibit> exhibitList;
	private int exhbitListViewItem;
	GlobalState state = ((GlobalState) getContext().getApplicationContext());
	
	/**
	 * Standard Constructor
	 * 
	 * @param context
	 * @param exhbitListViewItem
	 * @param exhibitList
	 */
	public ExhibitListAdapter(Context context, int exhbitListViewItem, List<Exhibit> exhibitList) {
		super(context, exhbitListViewItem, exhibitList);
		this.ctx = context;
		this.exhibitList = exhibitList;
		this.exhbitListViewItem = exhbitListViewItem;
	}
	
	private static final int MAX_SLOT_SIZE = 10;
	
	public static class ViewHolder {
		public ImageView exhibitThumb;
		public TextView exhibitName;
		public TextView exhibitArtist;
		public TextView exhibitCompleted;
		// public TextView exhibitPoints;
		public LinearLayout tagSlotsVerticalLayout;
		public View tagSlotLine_0;
		public View tagSlotLine_1;
		public View tagSlotLine_2;
		public View tagSlotLine_3;
		public View tagSlotLine_4;
		public View tagSlotLine_5;
		public View tagSlotLine_6;
		public View tagSlotLine_7;
		public View tagSlotLine_8;
		public View tagSlotLine_9;
	}
	
	//
	// public static class TagSlotView extends View {
	//
	// public TagSlotView(Context context, Exhibit.TagStatus tagStatus) {
	// super(context);
	// switch (tagStatus) {
	// case VALID:
	// this.setBackgroundResource(R.color.tag_valid);
	// break;
	// case NULL:
	// this.setBackgroundResource(R.color.tag_null);
	// break;
	// case NONVALID:
	// this.setBackgroundResource(R.color.tag_invalid);
	// break;
	// }
	// }
	//
	// }
	
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		ViewHolder viewHolder;
		Exhibit exhibit = exhibitList.get(position);
		
		if ((v == null)) {
			LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = li.inflate(exhbitListViewItem, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.exhibitName = (TextView) v.findViewById(R.id.exhibit_name);
			viewHolder.exhibitArtist = (TextView) v.findViewById(R.id.exhibit_artist);
			viewHolder.exhibitThumb = (ImageView) v.findViewById(R.id.exhibit_thumb);
			viewHolder.tagSlotsVerticalLayout = (LinearLayout) v.findViewById(R.id.exhibit_bar);
			// viewHolder.tagSlotLines = new View[MAX_SLOT_SIZE];
			viewHolder.tagSlotLine_0 = (View) v.findViewById(R.id.exhibit_bar_line_0);
			viewHolder.tagSlotLine_1 = (View) v.findViewById(R.id.exhibit_bar_line_1);
			viewHolder.tagSlotLine_2 = (View) v.findViewById(R.id.exhibit_bar_line_2);
			viewHolder.tagSlotLine_3 = (View) v.findViewById(R.id.exhibit_bar_line_3);
			viewHolder.tagSlotLine_4 = (View) v.findViewById(R.id.exhibit_bar_line_4);
			viewHolder.tagSlotLine_5 = (View) v.findViewById(R.id.exhibit_bar_line_5);
			viewHolder.tagSlotLine_6 = (View) v.findViewById(R.id.exhibit_bar_line_6);
			viewHolder.tagSlotLine_7 = (View) v.findViewById(R.id.exhibit_bar_line_7);
			viewHolder.tagSlotLine_8 = (View) v.findViewById(R.id.exhibit_bar_line_8);
			viewHolder.tagSlotLine_9 = (View) v.findViewById(R.id.exhibit_bar_line_9);
			
			viewHolder.exhibitCompleted = (TextView) v.findViewById(R.id.exhibit_completed);
			
			// Now set the tag
			v.setTag(viewHolder);
		} else {
			// In case the view item exists, get the View from the tag
			viewHolder = (ViewHolder) v.getTag();
		}
		/*
		 * public TextView exhibitName;
		 * public TextView exhibitArtist;
		 * public ImageView exhibitThumb;
		 * public TextView exhibitCompleted;
		 * public LinearLayout tagSlotsVerticalLayout;
		 * public View[] tagSlotLine;
		 */
		if (exhibit != null) {
			viewHolder.exhibitName.setText(exhibit.getExhibitName());
			viewHolder.exhibitArtist.setText(exhibit.getExhibitArtist());
			// viewHolder.exhibitName.setTextColor(exhibit.getExhibitColor());
			if (exhibit.isCompleted()) {
				viewHolder.exhibitCompleted.setVisibility(View.VISIBLE);
				viewHolder.exhibitName.setTextColor(Color.LTGRAY);
				viewHolder.exhibitArtist.setTextColor(Color.LTGRAY);
			}
			viewHolder.exhibitThumb.setImageResource(exhibit.getExhibitThumbnail(ctx));
			
			int exhibitTagSlotSize = exhibit.getExhibitTagSlotsSize();
			for (int i = 0; i < MAX_SLOT_SIZE; i++) {
				if (i < exhibitTagSlotSize) {
					Tag tag = exhibit.getExhibitTagSlots().get(i);
					if ((tag != null) && (!tag.isNull())) {
						if (tag.isValidForExhibit(exhibit)) {
							viewHolder.tagSlotsVerticalLayout.getChildAt(i).setBackgroundResource(R.color.tag_valid);
							viewHolder.tagSlotsVerticalLayout.getChildAt(i).setVisibility(View.VISIBLE);
							// viewHolder.tagSlotsVerticalLayout.addView(new TagSlotView(getContext(), Exhibit.TagStatus.VALID),
							// viewHolder.tagSlotLine[i].getLayoutParams());
						} else {
							viewHolder.tagSlotsVerticalLayout.getChildAt(i).setBackgroundResource(R.color.tag_invalid);
							viewHolder.tagSlotsVerticalLayout.getChildAt(i).setVisibility(View.VISIBLE);
							// viewHolder.tagSlotsVerticalLayout.addView(new TagSlotView(getContext(), Exhibit.TagStatus.NONVALID),
							// viewHolder.tagSlotLine[i].getLayoutParams());
						}
					} else {
						viewHolder.tagSlotsVerticalLayout.getChildAt(i).setBackgroundResource(R.color.tag_null);
						viewHolder.tagSlotsVerticalLayout.getChildAt(i).setVisibility(View.VISIBLE);
						// viewHolder.tagSlotsVerticalLayout.addView(new TagSlotView(getContext(), Exhibit.TagStatus.NULL),
						// viewHolder.tagSlotLine[i].getLayoutParams());
					}
				} else {
					viewHolder.tagSlotsVerticalLayout.getChildAt(i).setVisibility(View.GONE);
				}
			}
		}
		
		return v;
	}
}
