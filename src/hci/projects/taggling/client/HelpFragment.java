package hci.projects.taggling.client;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import hci.projects.taggling.R;

public class HelpFragment extends Fragment {

	private View mRoot;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRoot = inflater.inflate(R.layout.help_screen, null);

		getActivity().getActionBar().setTitle(R.string.action_show_info);

		/*// Set the info text
		((TextView) mRoot.findViewById(R.id.info_text_2))
				.setText(R.string.info_2);

		// Set the info text
		//
		((TextView) mRoot.findViewById(R.id.info_text_3))
				.setText(R.string.info_3);*/

		return mRoot;
	}

}
