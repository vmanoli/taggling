package hci.projects.taggling.client;

import hci.projects.taggling.GlobalState;
import hci.projects.taggling.R;
import hci.projects.taggling.utils.Prefs;
import hci.projects.taggling.utils.TGConstants;
import hci.projects.tagglinglib.GameHandler;
import hci.projects.tagglinglib.LoadData;
import hci.projects.tagglinglib.Messages.MessageInitialization;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class SessionActivity extends Activity {
	public static final String TAG = "SessionList";
	// private Button sessionCreate;
	// FIXME: loginProgressDialog leads to exception if login is cancelled:
	// http://stackoverflow.com/questions/2745061/java-lang-illegalargumentexception-view-not-attached-to-window-manager
	private ProgressDialog loginProgressDialog;
	GlobalState csapplication;
	// Asyntask
	AsyncTask<Void, Void, Void> mRegisterTask;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		csapplication = (GlobalState) this.getApplication();
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		// getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		mLockScreenRotation();
		setContentView(R.layout.sessionactivity);
		
		JoinSessionTask joinSessionTask = new JoinSessionTask();
		joinSessionTask.execute();
	}
	
	@Override
	public void onBackPressed() {
		Log.d(TAG, "onBackPressed Called");
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.session_exit)).setCancelable(false)
				.setPositiveButton(getString(android.R.string.yes), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent startActivityIntent = new Intent();
						Prefs.setSessionId(0);
						startActivityIntent.setClass(SessionActivity.this, LoginActivity.class);
						startActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						SessionActivity.this.startActivity(startActivityIntent);
						SessionActivity.this.finish();
						dialog.dismiss();
					}
				}).setNegativeButton(getString(android.R.string.no), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	public void onResume() {
		super.onResume();
		Log.i(TGConstants.LOG_TAG, "SessionActivity:onResume");
	}
	
	protected void mLockScreenRotation() {
		// Stop the screen orientation changing during an event
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	
	private class JoinSessionTask extends AsyncTask<String, Void, Boolean> {
		// Boolean result = false;
		int sessionId = 0;
		
		@Override
		protected void onPreExecute() {
			loginProgressDialog = ProgressDialog.show(SessionActivity.this, "", getString(R.string.session_logging_in_please_wait), true);
			loginProgressDialog.setCancelable(false);
			loginProgressDialog.show();
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			return true;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			if (result) {
				LoadGameDefinition loadGameDefinition = new LoadGameDefinition();
				loadGameDefinition.execute();
			} else {
				Toast.makeText(SessionActivity.this, getString(R.string.session_join_failed), Toast.LENGTH_SHORT).show();
				loginProgressDialog.dismiss();
			}
		}
		
		@Override
		protected void onCancelled() {
			boolean running = true;
			running = false;
		}
	}
	
	private class LoadGameDefinition extends AsyncTask<String, Void, Boolean> {
		Boolean result = false;
		
		@Override
		protected Boolean doInBackground(String... params) {
			LoadData.local(getApplicationContext(), GameHandler.getInstance());
			result = true; // FIXME
			return result;
		}
		
		protected void onPostExecute(Boolean result) {
			if (result) {
				NameValuePair[] nameValuePairs = new NameValuePair[1];
				nameValuePairs[0] = new BasicNameValuePair(TGConstants._FIELD_PLAYER_ID, GameHandler.getInstance().getPlayer().getPlayerName());
				GetInitialTagsDistribution getInitialTagsDistribution = new GetInitialTagsDistribution();
				getInitialTagsDistribution.execute(nameValuePairs);
			} else {
				Toast.makeText(SessionActivity.this, R.string.session_failed_to_load, Toast.LENGTH_SHORT).show();
				if ((loginProgressDialog != null) && (loginProgressDialog.isShowing())) {
					loginProgressDialog.dismiss();
				}
			}
		}
	}
	
	private class GetInitialTagsDistribution extends AsyncTask<NameValuePair, Void, String> {
		boolean _running = true;
		String response;
		
		/***
		 * 1st parameter is username
		 */
		@Override
		protected String doInBackground(NameValuePair... params) {
			Thread.currentThread().setName("GetInitialTagsDistribution");
			return response;
		}
		
		@Override
		protected void onPostExecute(String result) {
			GlobalState.getInstance().isGameRunning = true;
			if (_running) {
				csapplication.checkForMissedMessages();
				new Handler().postDelayed(new Runnable() {
					
					public void run() {
						if ((loginProgressDialog != null) && (loginProgressDialog.isShowing())) {
							loginProgressDialog.dismiss();
						}
						
						Intent startActivityIntent = new Intent();
						startActivityIntent.setClass(SessionActivity.this, MainActivity.class);
						startActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						SessionActivity.this.startActivity(startActivityIntent);
						
						// overridePendingTransition(R.anim.activityfadein, R.anim.splashfadeout);
					}
				}, TGConstants.SPLASH_DISPLAY_TIME);
			}
		}
		
		@Override
		protected void onCancelled() {
			_running = false;
			loginProgressDialog.dismiss();
			Toast.makeText(getBaseContext(), R.string.login_failed, Toast.LENGTH_SHORT).show();
		}
	}
}