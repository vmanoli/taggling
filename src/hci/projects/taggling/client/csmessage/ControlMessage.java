package hci.projects.taggling.client.csmessage;

import hci.projects.tagglinglib.Messages.MessageTaggling;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/***
 * Represents a game message message is in the format action@playerid@keyid@objectid
 * 
 * @author chris
 */
public class ControlMessage implements Parcelable {
	private static final String TAG = "ControlMessage";
	public static final String STARTMESSAGE = "START";
	public static final String STOPMESSAGE = "STOP";
	public static final String UNDEFINED = "UNDEFINED";
	private String _controlMessage = UNDEFINED;
	
	public ControlMessage(String controlMessage) {
		_controlMessage = controlMessage;
	}
	
	
	public String getMessage() {
		return _controlMessage;
	}
	
	public ControlMessage(Parcel parcel) {
		_controlMessage = parcel.readString();
	}
	
	public static final Parcelable.Creator<ControlMessage> CREATOR = new Parcelable.Creator<ControlMessage>() {
		public ControlMessage createFromParcel(Parcel in) {
			return new ControlMessage(in);
		}
		
		public ControlMessage[] newArray(int size) {
			return new ControlMessage[size];
		}
	};
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(_controlMessage);
	}
	
	public String toString() {
		return _controlMessage;
	}

	@Override
	public int describeContents() {
		return _controlMessage.hashCode();
	}
	
}
