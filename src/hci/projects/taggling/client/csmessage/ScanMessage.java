package hci.projects.taggling.client.csmessage;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import hci.projects.taggling.utils.TGConstants;

/***
 * Represents a game message message is in the format action@playerid@keyid@objectid
 * 
 * @author chris
 */
public class ScanMessage implements Parcelable {
	private static final String TAG = "ScanMessage";
	private String _fromPlayerId;
	private int _objectId;

	// /***
	// * Create a CSMessage object from an XMPP String XMPP Message
	// action@playerid@keyid@objectid
	// *
	// * @param XMPPmessage
	// */
	// public CSMessage(String XMPPmessage) {
	// if (XMPPmessage != null) {
	// String tokens[] = XMPPmessage.split(TGConstants._CSMESSAGE_DELIMETER);
	// if (tokens.length == 4) {
	// _fromPlayerId = tokens[0];
	// _objectId = Integer.parseInt(tokens[1]);
	// _keyId = Integer.parseInt(tokens[2]);
	// _action = CSAction.intValue(Integer.parseInt(tokens[3]));
	// }
	// }
	// }

	/**
	 * Create a CSMessage from a JSON array string
	 * 
	 * @param JSONString
	 */
	public ScanMessage(JSONObject jsonObject) {
		try {
			_fromPlayerId = jsonObject.getString("playerId");
			_objectId = jsonObject.getInt("objectId");
		} catch (JSONException e) {
			Log.e(TAG, "Couldn't parse JSON string " + e.toString());
		}
	}

	/***
	 * Create a CSMessage
	 * 
	 * @param fromPlayerId
	 * @param objectId
	 * @param keyId
	 * @param action
	 */
	public ScanMessage(String fromPlayerId, int objectId) {
		_fromPlayerId = fromPlayerId;
		_objectId = objectId;
	}

	/***
	 * Create an XMPPMessage from this object
	 * 
	 * @return message
	 */
	public String toXMPPMessage() {
		return _fromPlayerId + TGConstants._CSMESSAGE_DELIMETER
				+ String.valueOf(_objectId);
	}

	public int getObjectId() {
		return _objectId;
	}

	public String getPlayerId() {
		return _fromPlayerId;
	}

	public ScanMessage(Parcel parcel) {
		_fromPlayerId = parcel.readString();
		_objectId = parcel.readInt();
	}

	public static final Parcelable.Creator<ScanMessage> CREATOR = new Parcelable.Creator<ScanMessage>() {
		public ScanMessage createFromParcel(Parcel in) {
			return new ScanMessage(in);
		}

		public ScanMessage[] newArray(int size) {
			return new ScanMessage[size];
		}
	};

	@Override
	public int describeContents() {
		return toXMPPMessage().hashCode();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(_fromPlayerId);
		dest.writeInt(_objectId);
	}

	public String toString() {
		return _fromPlayerId + TGConstants._CSMESSAGE_DELIMETER
				+ String.valueOf(_objectId);
	}

}
