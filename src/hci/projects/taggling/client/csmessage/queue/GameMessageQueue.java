package hci.projects.taggling.client.csmessage.queue;

import java.util.HashMap;

import hci.projects.taggling.client.csmessage.GameMessage;
import hci.projects.tagglinglib.Messages.MessageExhibitUpdate;
import hci.projects.tagglinglib.Messages.MessageTaggling;

public class GameMessageQueue extends HashMap<Integer, QueueMessage> {
	private static final long serialVersionUID = -8053057074290333702L;
	
	// private HashMap<Integer, CSQueueMessage> csMessageQueue = new HashMap<Integer, CSQueueMessage>();
	
	/**
	 * Checks if message has been returned from server
	 * 
	 * @param tgMessage
	 * @return
	 */
	public boolean hasReturned(MessageTaggling tgMessage) {
		if (this.containsKey(tgMessage.getMessageKey())) {
			// this will return true only if our message has been returned 
			return (this.get(tgMessage.getMessageKey()).hasReturned);
		} else {
			// this means that the queue was purged from this message, so.. 
			return true;
		}
	}
	
	public void pushMessageIntoQueue(MessageTaggling tgMessage) {
		if (!this.containsKey(tgMessage.getMessageKey())) {
			this.put(tgMessage.getMessageKey(), new QueueMessage(tgMessage, QueueMessage.HAS_NOT_RETURNED));
		} // This will hold the message until we get confirmation (see receive)
	}
	
	/**
	 * Removes a message from the queue
	 * 
	 * @param tgMessage
	 */
	public void cancelRequest(MessageTaggling tgMessage) {
		try {
			this.remove(tgMessage.getMessageKey());
		} catch (Exception e) {
			// Log.e(TAG, "Error while removing canceled message from queue " +
			// e.toString());
		}
	}
}
