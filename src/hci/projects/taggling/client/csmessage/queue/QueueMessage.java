package hci.projects.taggling.client.csmessage.queue;

import hci.projects.tagglinglib.Messages.MessageTaggling;

public class QueueMessage {
	public static final Boolean HAS_NOT_RETURNED = false;
	public static final Boolean HAS_RETURNED = true;
	
	private MessageTaggling messageTaggling;
	public Boolean hasReturned = false;
	
	public Boolean hasReturned() {
		return hasReturned;
	}
	
	public int getPlayerId() {
		return messageTaggling.getPlayerId();
	}
	
	public QueueMessage(MessageTaggling messageTaggling, Boolean hasReturned) {
		this.messageTaggling = messageTaggling;
		this.hasReturned = hasReturned;
	}
}