package hci.projects.taggling.client.csmessage;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import hci.projects.taggling.utils.TGConstants;

/***
 * Represents a game message message is in the format action@playerid@keyid@objectid
 * 
 * @author chris
 */
public class GameMessage implements Parcelable {
	private static final String TAG = "CSMessage";
	private int _id = 0;
	private String _fromPlayerId;
	private int _objectId;
	private int _keyId;
	private CSAction _action;
	private String _fromTeamId;
	
	// /***
	// * Create a CSMessage object from an XMPP String XMPP Message action@playerid@keyid@objectid
	// *
	// * @param XMPPmessage
	// */
	// public CSMessage(String XMPPmessage) {
	// if (XMPPmessage != null) {
	// String tokens[] = XMPPmessage.split(TGConstants._CSMESSAGE_DELIMETER);
	// if (tokens.length == 4) {
	// _fromPlayerId = tokens[0];
	// _objectId = Integer.parseInt(tokens[1]);
	// _keyId = Integer.parseInt(tokens[2]);
	// _action = CSAction.intValue(Integer.parseInt(tokens[3]));
	// }
	// }
	// }
	
	/**
	 * Create a CSMessage from a JSON array string
	 * 
	 * @param JSONString
	 */
	public GameMessage(JSONObject jsonObject) {
		try {
			_id = jsonObject.getInt("messageId");
			_fromPlayerId = jsonObject.getString("playerId");
			_objectId = jsonObject.getInt("objectId");
			_keyId = jsonObject.getInt("keyId");
			_action = CSAction.intValue(jsonObject.getInt("action"));
			_fromTeamId = jsonObject.getString("teamId");
		} catch (JSONException e) {
			Log.e(TAG, "Couldn't parse JSON string " + e.toString());
		}
	}
	
	/***
	 * Create a CSMessage
	 * 
	 * @param fromPlayerId
	 * @param objectId
	 * @param keyId
	 * @param action
	 */
	public GameMessage(String fromPlayerId, int objectId, int keyId, CSAction action) {
		_fromPlayerId = fromPlayerId;
		_objectId = objectId;
		_keyId = keyId;
		_action = action;
	}
	
	/***
	 * Create an XMPPMessage from this object
	 * 
	 * @return message
	 */
	public String toXMPPMessage() {
		return _fromPlayerId + TGConstants._CSMESSAGE_DELIMETER + String.valueOf(_objectId) + TGConstants._CSMESSAGE_DELIMETER + String.valueOf(_keyId)
				+ TGConstants._CSMESSAGE_DELIMETER + String.valueOf(_action.ordinal());
	}

	public int getGameMessageId() {
		return _id;
	}
	
	public CSAction getAction() {
		return _action;
	}
	
	public int getObjectId() {
		return _objectId;
	}
	
	public int getKeyId() {
		return _keyId;
	}
	
	public String getPlayerId() {
		return _fromPlayerId;
	}	

	public String getTeamId() {
		return _fromTeamId;
	}
	
	public GameMessage(Parcel parcel) {
		_fromPlayerId = parcel.readString();
		_objectId = parcel.readInt();
		_keyId = parcel.readInt();
		_action = CSAction.intValue(parcel.readInt());
		_fromTeamId = parcel.readString();
	}
	
	public static final Parcelable.Creator<GameMessage> CREATOR = new Parcelable.Creator<GameMessage>() {
		public GameMessage createFromParcel(Parcel in) {
			return new GameMessage(in);
		}
		
		public GameMessage[] newArray(int size) {
			return new GameMessage[size];
		}
	};
	
	@Override
	public int describeContents() {
		return toXMPPMessage().hashCode();
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(_fromPlayerId);
		dest.writeInt(_objectId);
		dest.writeInt(_keyId);
		dest.writeInt(_action.ordinal());
		dest.writeString(_fromTeamId);
	}
	
	public enum CSAction {
		ASSOCIATE, RELEASE;
		static Map<Integer, CSAction> intMap = new HashMap<Integer, CSAction>(2);
		static {
			for (CSAction e : CSAction.values()) {
				intMap.put(e.ordinal(), e);
			}
		}
		
		public static CSAction intValue(int key) {
			return intMap.get(key);
		}
	}
	
	public String toString() {
		return _fromPlayerId + TGConstants._CSMESSAGE_DELIMETER + String.valueOf(_objectId) + TGConstants._CSMESSAGE_DELIMETER + String.valueOf(_keyId)
				+ TGConstants._CSMESSAGE_DELIMETER + _action.name();
	}
	
	/**
	 * Checks if message is properly formatted
	 * 
	 * @return
	 */
	public boolean isValid() {
		if (_action != null)
			return true;
		else
			return false;
	}


}
