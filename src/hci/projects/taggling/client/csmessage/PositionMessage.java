package hci.projects.taggling.client.csmessage;

import java.util.Calendar;
import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;
import hci.projects.taggling.gps.ParcelableGeoPoint;
import hci.projects.taggling.utils.TGConstants;

/***
 * Represents a message message is in the format playerid@position@timestamp
 * 
 * @author chris
 */
public class PositionMessage implements Parcelable {
	private static final String TAG = "ScanMessage";
	private String _fromPlayerId;
	private ParcelableGeoPoint _position;
	private Long _timestamp;
	private float _accuracy;
	
	// private String _timestamp;
	
	/***
	 * Create a CSMessage
	 * 
	 * @param fromPlayerId
	 * @param objectId
	 * @param keyId
	 * @param action
	 */
	public PositionMessage(String fromPlayerId, ParcelableGeoPoint position, float accuracy) {
		_fromPlayerId = fromPlayerId;
		_position = position;
		_timestamp = System.currentTimeMillis();
		_accuracy = accuracy;
	}
	
	/***
	 * Create an XMPPMessage from this object
	 * 
	 * @return message
	 */
	public String toXMPPMessage() {
		return _fromPlayerId + TGConstants._CSMESSAGE_DELIMETER + _position.toString() + TGConstants._CSMESSAGE_DELIMETER
				+ ((Long) (_timestamp / 1000)).toString() + TGConstants._CSMESSAGE_DELIMETER + String.valueOf(_accuracy);
	}
	
	public ParcelableGeoPoint getParcelableGeoPoint() {
		return _position;
	}
	
	public String getPlayerId() {
		return _fromPlayerId;
	}
	
	public PositionMessage(Parcel parcel) {
		_fromPlayerId = parcel.readString();
		_position = parcel.readParcelable(null);
		_timestamp = parcel.readLong();
	}
	
	public static final Parcelable.Creator<ControlMessage> CREATOR = new Parcelable.Creator<ControlMessage>() {
		public ControlMessage createFromParcel(Parcel in) {
			return new ControlMessage(in);
		}
		
		public ControlMessage[] newArray(int size) {
			return new ControlMessage[size];
		}
	};
	
	@Override
	public int describeContents() {
		return toXMPPMessage().hashCode();
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(_fromPlayerId);
		dest.writeParcelable(((Parcelable) _position), 0);
		dest.writeLong(_timestamp);
	}
	
	public String toString() {
		final Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(_timestamp);
		Date date = cal.getTime();
		date.toGMTString();
		return _fromPlayerId + TGConstants._CSMESSAGE_DELIMETER + String.valueOf(_position) + TGConstants._CSMESSAGE_DELIMETER + date.toGMTString();
	}
}
