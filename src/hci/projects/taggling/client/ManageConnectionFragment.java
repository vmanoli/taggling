package hci.projects.taggling.client;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.ToggleButton;
import hci.projects.taggling.R;

public class ManageConnectionFragment extends Fragment {

	private View mRoot;
	private boolean connectedRoom;
	private boolean connectedGoogleClient;
	
	ManageConnectionListener mListener = null;
	
	public interface ManageConnectionListener {
		public boolean statusSignedIn();
		public boolean statusRoomConnected();
		public void onSignOutButtonClicked();
		public void onOfflineButtonClicked();
    }

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		try {
			mListener = (ManageConnectionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnToggleListener");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRoot = inflater.inflate(R.layout.connection_screen, null);

		getActivity().getActionBar().setTitle(R.string.title_connection_info);
		
		connectedRoom = mListener.statusRoomConnected();
		connectedGoogleClient = mListener.statusSignedIn();		
		
		ToggleButton toggleRoomConnection = (ToggleButton) mRoot.findViewById(R.id.conn_room);
		ToggleButton toggleGapiConnection = (ToggleButton) mRoot.findViewById(R.id.conn_gapi);
		
		toggleRoomConnection.setChecked(connectedRoom);
		toggleGapiConnection.setChecked(connectedGoogleClient);

		return mRoot;
	}
	
	public void onToggleRoomClicked(View view) {
	    // Is the toggle on?
	    boolean on = ((ToggleButton) view).isChecked();
	    
	    if (on) {
	        Toast.makeText(getActivity(), "room is checked ON", Toast.LENGTH_SHORT)
	        	.show();
	    } else {
	    	Toast.makeText(getActivity(), "room is checked OFF", Toast.LENGTH_SHORT)
	    		.show();
	    }
	}
	
	public void onToggleGapiClicked(View view) {
	    // Is the toggle on?
	    boolean on = ((ToggleButton) view).isChecked();
	    
	    if (on) {
	        // Enable vibrate
	    } else {
//	    	mListener.onSignOutButtonClicked();
	    }
	}

}
