package hci.projects.taggling.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import hci.projects.taggling.R;

//promptScanExhibit(Exhibit e)
public class PromptScanDialog extends DialogFragment {
	
	private String mtext;
	private int mImgRes;

	public interface PromptScanDialogListener {
		public void onDialogPositiveClick();
	}

	// Use this instance of the interface to deliver action events
	PromptScanDialogListener mListener;

	// Override the Fragment.onAttach() method to instantiate the
	// NoticeDialogListener
	@Override
	    public void onAttach(Activity activity) {
	        super.onAttach(activity);
	        // Verify that the host activity implements the callback interface
	        try {
	            // Instantiate the NoticeDialogListener so we can send events to the host
	            mListener = (PromptScanDialogListener) activity;
	        } catch (ClassCastException e) {
	            // The activity doesn't implement the interface, throw exception
	            throw new ClassCastException(activity.toString()
	                    + " must implement NoticeDialogListener");
	        }
	        mtext = this.getArguments().getString("DIALOG_NAME");
	        mImgRes = this.getArguments().getInt("DIALOG_THUMBNAIL");
	        
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		
		// Build the dialog and set up the button click handlers
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		// Initialize the view
		View v = getActivity().getLayoutInflater()
				.inflate(R.layout.exhibit_dialog_view, null);
		ImageView vThumb = (ImageView) v.findViewById(R.id.tags_exhibit_thumb);
		TextView vText = (TextView) v.findViewById(R.id.tags_exhibit_name);
		vThumb.setImageResource(mImgRes);
		vText.setText(mtext);

		// Set the dialog properly
		builder.setView(v);
//		builder.setMessage(R.id.login_status_message);
		builder.setPositiveButton(R.string.dialog_scanit,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						mListener.onDialogPositiveClick();
					}
				});
		builder.setNegativeButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		return builder.create();
	}

}
