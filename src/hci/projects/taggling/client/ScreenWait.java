package hci.projects.taggling.client;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import hci.projects.taggling.R;

public class ScreenWait extends Fragment {

	private View mRoot;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		activity.getActionBar().hide();
		// TODO toggle drawer
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRoot = inflater.inflate(R.layout.screen_wait, null);
		return mRoot;
	}
	
	

}
