package hci.projects.taggling.client;

import hci.projects.taggling.GlobalState;
import hci.projects.taggling.ListLoaders.SimpleListLoader_Inventory;
import hci.projects.taggling.R;
import hci.projects.tagglinglib.GameHandler;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class TabsFragmentPlayer extends Fragment implements
		OnTabChangeListener {

	public static final String TAB_INVENTORY = "inventory";
	public static final String TAB_POINTS = "pointstable";

	private View mRoot;
	private TabHost mTabHost;
	private int mCurrentTab;
	
	Activity mActivity;
	GlobalState state;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mActivity = activity;
		state = (GlobalState) mActivity.getApplicationContext();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		mRoot = inflater.inflate(R.layout.tabs_fragment, null);
		
		// Set player information area
		//
		mRoot.findViewById(R.id.player_info).setVisibility(View.VISIBLE);
		((TextView) mRoot.findViewById(R.id.player_info_slots)).setText(Integer.toString(
				GameHandler.getInstance().getPlayer().getMaxSlots()));
		((TextView) mRoot.findViewById(R.id.player_info_empty_slots)).setText(Integer.toString(
				GameHandler.getInstance().getPlayer().countInventoryEmptySlots()));
		((TextView) mRoot.findViewById(R.id.player_info_total_points))
				.setText(Integer.toString(GameHandler.getInstance().getPlayer()
						.getPlayerTotalPoints()));
		
		// Setup tabs area
		//
		mTabHost = (TabHost) mRoot.findViewById(android.R.id.tabhost);
		setupTabs();
		Fragment frg = new SimpleListLoader_Inventory();
		updateTab(frg, R.id.tab_1);
		mCurrentTab = 0;
		
		return mRoot;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);

		mTabHost.setOnTabChangedListener(this);
		mTabHost.setCurrentTab(mCurrentTab);

		mActivity.getActionBar().setTitle(mActivity.getResources().getString(
				R.string.title_activity_player_info)
				+ GameHandler.getInstance().getPlayer().getPlayerName());

		// Set the info text
		//
		// ((TextView) getActivity().findViewById(R.id.info_text_2))
		// .setText(R.string.info_exhibit);

		// manually start loading stuff in the first tab
		Fragment frg = new SimpleListLoader_Inventory();
		updateTab(frg, R.id.tab_1);
	}

	private void setupTabs() {
		mTabHost.setup(); // important!
		mTabHost.addTab(newTab(TAB_INVENTORY, R.string.tab_inventory,
				R.id.tab_1));
		mTabHost.addTab(newTab(TAB_POINTS, R.string.tab_points, R.id.tab_2));
	}

	private TabSpec newTab(String tag, int labelId, int tabContentId) {

		View indicator = LayoutInflater.from(mActivity).inflate(
				R.layout.tab,
				(ViewGroup) mRoot.findViewById(android.R.id.tabs), false);
		((TextView) indicator.findViewById(R.id.text)).setText(labelId);

		TabSpec tabSpec = mTabHost.newTabSpec(tag);
		tabSpec.setIndicator(indicator);
		tabSpec.setContent(tabContentId);
		return tabSpec;
	}

	@Override
	public void onTabChanged(String tabId) {

		if (TAB_INVENTORY.equals(tabId)) {
			Fragment frg = new SimpleListLoader_Inventory();
			updateTab(frg, R.id.tab_1);
			mCurrentTab = 0;
			return;
		}
//		if (TAB_POINTS.equals(tabId)) {
//			Fragment frg = new SimpleListLoader_PointsTable();
//			updateTab(frg, R.id.tab_2);
//			mCurrentTab = 1;
//			return;
//		}
	}

	private void updateTab(Fragment fragLoader, int placeholder) {
		getFragmentManager()
				.beginTransaction()
				.replace(placeholder, fragLoader, null)
				.commit();
	}

}
