package hci.projects.taggling.client;

import hci.projects.taggling.R;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentGameOver extends Fragment implements View.OnClickListener{

	private View mRoot;
	static String LOGTAG = "Frag_GameOver";
	
	Listener mListener = null;
	
	public interface Listener {
//		public void onSignOutButtonClicked2();
		public void onOfflineButtonClicked();
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRoot = inflater.inflate(R.layout.fragment_game_over, null);
		
		mRoot.findViewById(R.id.sign_out_button2).setOnClickListener(this);
		return mRoot;
	}

	@Override
	public void onClick(View v) {
//		switch (v.getId()) {
////		case R.id.sign_out_button2:
////			mListener.onSignOutButtonClicked2();
////			Log.d(LOGTAG, "Sign Out Button Clicked2");
////			break;
////		case R.id.button_offline_play:
////			mListener.onOfflineButtonClicked();
////			Log.d(LOGTAG, "Offline Button Clicked");
////			break;
//		}
	}
	
	public void setListener(Listener l) {
        mListener = l;
    }

}
