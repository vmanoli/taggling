package hci.projects.taggling.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import hci.projects.taggling.R;
import hci.projects.taggling.utils.Globals;
import hci.projects.taggling.utils.Prefs;
import hci.projects.taggling.utils.Utils;
import hci.projects.taggling.utils.WakeLocker;
import hci.projects.taggling.client.TGActivity;
import hci.projects.taggling.utils.TGConstants;
import hci.projects.tagglinglib.GameHandler;
import hci.projects.tagglinglib.GameElements.Player;

import com.google.android.gcm.GCMRegistrar;
import com.google.zxing.integration.android.IntentIntegrator;

public class LoginActivity extends TGActivity {
	public static final String TAG = "LoginActivity";
	private static final String AUTH_LOGIN_SUCCESS = "1";
	private static final String AUTH_LOGIN_FAILED = "2";
	private static final String AUTH_USER_CREATED = "3";
	private static final String AUTH_GCM_REGISTRATION_FAILED = "4";
	private static final String AUTH_LOGOUT_FIRST = "5";
	private static final String AUTH_GAME_NOT_STARTED = "6";
	// DoRemoteLogin doXMPPLogin;
	EditText loginName, loginPassword;
	CheckBox checkBoxRememberMe;
	Button loginButton;
//	ImageButton loginHelpButton;
	ProgressDialog loginProgressDialog;
	DoRemoteLogin doRemoteLogin;
	private String gcmRegid = "";
	
	/***
	 * A 64-bit number (as a hex string) that is randomly generated on the device's first boot and should remain constant for the lifetime of the device. (The value may change
	 * if a factory reset is performed on the device.)
	 */
	private String android_id;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		mLockScreenRotation();
		
		Prefs.setPrefsName(getString(R.string.app_name));
		PackageInfo pinfo;
		android_id = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
		
		setContentView(R.layout.loginactivity);
		checkBoxRememberMe = (CheckBox) findViewById(R.id.loginRememberMe);
		loginButton = (Button) findViewById(R.id.loginbutton);
//		loginHelpButton = (ImageButton) findViewById(R.id.loginHelpButton);
		loginName = (EditText) findViewById(R.id.loginname);
		loginPassword = (EditText) findViewById(R.id.loginpassword);
		TextView versionTv = (TextView) findViewById(R.id.textViewVersion);
		
		loginPassword.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
					InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					in.hideSoftInputFromWindow(v.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					return true;
				}
				return false;
			}
		});
		
		loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (Globals.getInstance().isConnected()) {
					if (loginName.getText().toString().equals("") || loginPassword.getText().toString().equals("")) {
						Toast.makeText(LoginActivity.this, R.string.login_provide_credentials, Toast.LENGTH_LONG).show();
					} else if (checkForBarcodeScanner()) {
						boolean rememberMeValue = checkBoxRememberMe.isChecked();
						Prefs.setLoginUserName(loginName.getText().toString());
						Prefs.setLoginPassword(loginPassword.getText().toString());
						if (rememberMeValue) {
							Prefs.setLoginRememberMe(true);
						} else {
							Prefs.setLoginRememberMe(false);
						}
						
						// Get GCM registration id
						if (loginProgressDialog != null) {
							loginProgressDialog.dismiss();
						}
						
						loginProgressDialog = ProgressDialog.show(LoginActivity.this, "", getString(R.string.logging_in_please_wait),
								true);
						loginProgressDialog.setCanceledOnTouchOutside(true);
						loginProgressDialog.setOnCancelListener(new OnCancelListener() {
							
							@Override
							public void onCancel(DialogInterface dialog) {
								if (doRemoteLogin != null) {
									doRemoteLogin.cancel(true);
								}
							}
						});
						loginProgressDialog.show();
						
						registerWithGCM();
						
					}
				} else {
					Toast.makeText(LoginActivity.this, R.string.no_network, Toast.LENGTH_LONG).show();
				}
			}
		});
		
//		loginHelpButton.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
////				final LinearLayout loginHelp = (LinearLayout) findViewById(R.id.loginHelp);
////				loginHelp.setVisibility(View.VISIBLE);
////				loginHelp.setOnClickListener(new OnClickListener() {
////					
////					@Override
////					public void onClick(View v) {
////						loginHelp.setVisibility(View.INVISIBLE);
////					}
////				});
//			}
//		});
		if (Prefs.getLoginRememberMe()) {
			loginName.setText(Prefs.getLoginUserName());
			loginPassword.setText(Prefs.getLoginPassword());
			checkBoxRememberMe.setChecked(true);
		}
		
		try {
			pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String versionName = pinfo.versionName;
			// versionTv.setText("v" + versionName); // FIXME
			versionTv.setText("v"); // FIXME
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		checkForBarcodeScanner();
	}
	
	private boolean checkForBarcodeScanner() {
		Intent intentScan = new Intent("com.google.zxing.client.android.SCAN");
		if (!isCallable(intentScan)) {
			// IntentIntegrator integrator = new
			// IntentIntegrator(LoginActivity.this);
			IntentIntegrator.showDownloadDialog(this, getString(R.string.login_install_barcode_scanner),
					getString(R.string.login_install_barcode_scanner_txt),
					getString(R.string.login_install_barcode_scanner_connect_to_google_play), getString(android.R.string.no));
			return false;
		} else {
			return true;
		}
	}
	
	private boolean isCallable(Intent intent) {
		List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}
	
	@Override
	protected void onStart() {
		super.onStart();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
	}
	
	@Override
	protected void onPause() {
		if (mHandleMessageReceiver != null) {
			try {
				unregisterReceiver(mHandleMessageReceiver);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (doRemoteLogin != null)
			doRemoteLogin.cancel(true);
	}
	
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		startActivity(intent);
	}
	
	public void onResume() {
		super.onResume();
		// set to false in order to force login in own server
		Utils.setRegisteredOnServer(LoginActivity.this, false);
		
//		positionTracker.stopTracking();
		registerReceiver(mHandleMessageReceiver, new IntentFilter(TGConstants.INTENT_GCM_SERVICE_MESSAGE));
		// registerReceiver(mRegisteredOnGCMReceiver, new IntentFilter("com.google.android.c2dm.intent.REGISTRATION"));
		Globals.getInstance().setDisplayScale(LoginActivity.this.getResources().getDisplayMetrics().density);
		Globals.getInstance().setDisplayDensity(LoginActivity.this.getResources().getDisplayMetrics().densityDpi);
	}
	
	// AsyncTasks
	private class DoRemoteLogin extends AsyncTask<NameValuePair, Void, String> {
		boolean _running = true;
		
		// 1st parameter is username 2nd parameter is password 3rd parameter isgcm_regId
		@Override
		protected String doInBackground(NameValuePair... params) {
			Thread.currentThread().setName("DoXMPPLogin");
			String response = AUTH_LOGIN_FAILED;
			try {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(params.length);
				for (NameValuePair nvpair : params) {
					nameValuePairs.add(nvpair);
				}
				
				HttpPost httppost = new HttpPost(TGConstants.LOGIN_SERVER_URL);
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				
				HttpParams httpParameters = new BasicHttpParams();
				DefaultHttpClient httpclient = new DefaultHttpClient(httpParameters);
				
				HttpResponse responsePost = httpclient.execute(httppost);
				HttpEntity resEntityPost = responsePost.getEntity();
				if (resEntityPost != null) {
					try {
						response = EntityUtils.toString(resEntityPost);
					} catch (Exception e) {
						Log.e(TAG, "3 " + e.toString());
					}
				}
			} catch (ClientProtocolException cpe) {
				response = AUTH_LOGIN_FAILED;
				Log.e(TAG, "1 " + cpe.toString());
			} catch (IOException ioe) {
				response = AUTH_LOGIN_FAILED;
				Log.e(TAG, "2 " + ioe.toString());
			}
			return response;
		}
		
		@Override
		protected void onPostExecute(String result) {
			if (_running) {
				try {
					JSONObject jsonObject = new JSONObject(result);
					if (jsonObject.getString("result").equals(AUTH_LOGIN_SUCCESS) || jsonObject.getString("result").equals(AUTH_USER_CREATED) ) {
						Player player = new Player(jsonObject);
						
						GameHandler.getInstance().addMyPlayer(player);
						Utils.setRegisteredOnServer(LoginActivity.this, true);
						registerWithGCM();
					} else {
						String failReason = jsonObject.getString("result");
						if (failReason.equals(AUTH_LOGOUT_FIRST)) {
							loginFailed(getString(R.string.login_failed_logout_first));
						} else if (failReason.equals(AUTH_GCM_REGISTRATION_FAILED)) {
							loginFailed(getString(R.string.login_failed_gcm_failed));
						} else if (failReason.equals(AUTH_GAME_NOT_STARTED)) {
							loginFailed(getString(R.string.login_failed_game_not_started));
						} else {
							loginFailed(getString(R.string.login_failed));
						}
					}
				} catch (JSONException e) {
					Log.d(TAG, "jsonString: " + result);
					loginFailed(getString(R.string.login_failed));
				}
			} else {
				loginFailed(getString(R.string.login_canceled));
			}
		}
		
		private void loginFailed(String result) {
			Log.w(TAG, "Login failed: " + result);
			loginProgressDialog.dismiss();
			if (result.equals(AUTH_LOGIN_FAILED))
				Toast.makeText(getApplicationContext(), R.string.login_wrong_password, Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
		}
		
		@Override
		protected void onCancelled() {
			_running = false;
			loginFailed(getString(R.string.login_canceled));
		}
	}
	
	private void doPrepareLogin(final String regId) {
		// hide soft keyboard
		if (doRemoteLogin != null) {
			doRemoteLogin.cancel(true);
		}
		doRemoteLogin = new DoRemoteLogin();
		InputMethodManager mgr = (InputMethodManager) getSystemService(LoginActivity.this.INPUT_METHOD_SERVICE);
		mgr.hideSoftInputFromWindow(loginName.getWindowToken(), 0);
		NameValuePair[] nameValuePairs = new NameValuePair[4];
		nameValuePairs[0] = new BasicNameValuePair(TGConstants._FIELD_USERNAME, loginName.getText().toString());
		nameValuePairs[1] = new BasicNameValuePair(TGConstants._FIELD_PASSWORD, loginPassword.getText().toString());
		nameValuePairs[2] = new BasicNameValuePair(TGConstants._FIELD_REGID, regId);
		nameValuePairs[3] = new BasicNameValuePair(TGConstants._FIELD_ANDROIDID, android_id);
		doRemoteLogin.execute(nameValuePairs);
	}
	
	// // Asyntask
	// AsyncTask<Void, Void, Void> mRegisterTask;
	//
	// private void doGCMRegistration(final String regId) {
	//
	// // Check if regid already presents
	// if (regId.equals("")) {
	// // Registration is not present, register now with GCM
	// GCMRegistrar.register(this, TGConstants.SENDER_ID);
	// }
	// // Device is already registered on GCM
	// else if (GCMRegistrar.isRegisteredOnServer(this)) {
	// // Skips registration.
	// // Toast.makeText(getApplicationContext(),
	// // "Already registered with GCM", Toast.LENGTH_LONG).show();
	// doPrepareLogin(regId);
	// } else {
	// // Try to register again, but not in the UI thread.
	// // It's also necessary to cancel the thread onDestroy(),
	// // hence the use of AsyncTask instead of a raw thread.
	// final Context context = this;
	// mRegisterTask = new AsyncTask<Void, Void, Void>() {
	//
	// @Override
	// protected Void doInBackground(Void... params) {
	// // Register on our server
	// // On server creates a new user
	// ServerUtilities.register(context, Prefs.getLoginUserName(), Prefs.getLoginUserName(), regId);
	// return null;
	// }
	//
	// @Override
	// protected void onPostExecute(Void result) {
	// mRegisterTask = null;
	// // /
	// // Globals.getInstance().setXMPPUsername(Utils.usernameToSha1(loginName.getText().toString()));
	// // /doPrepareLogin(regId);
	// }
	// };
	// mRegisterTask.execute(null, null, null);
	// }
	// }
	
	private void startSessionActivity() {
		new Handler().postDelayed(new Runnable() {
			public void run() {
				Log.i(TGConstants.LOG_TAG, "LoginActivity:logged-in:" + loginName.getText().toString());
				Intent startActivityIntent = new Intent();
				startActivityIntent.setClass(LoginActivity.this, SessionActivity.class);
				startActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				LoginActivity.this.startActivity(startActivityIntent);
				LoginActivity.this.finish();
//				overridePendingTransition(R.anim.activityfadein, R.anim.splashfadeout);
				if (loginProgressDialog != null)
					loginProgressDialog.dismiss();
			}
		}, TGConstants.SPLASH_DISPLAY_TIME);
	}
	
	/*
	 * Receiving push messages
	 */
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(getApplicationContext());
			Bundle extras = intent.getExtras();
			if (extras != null) {
				Set<String> ks = extras.keySet();
				Iterator iterator = ks.iterator();
				while (iterator.hasNext()) {
					Log.d("KEY", (String) iterator.next());
				}
			}
			if ((intent.getExtras().getString(TGConstants._GCM_ON_REGISTERED_MESSAGE) != null)) {
				Log.i("TAG", "got a registration_id:" + intent.getExtras().getString(TGConstants._GCM_ON_REGISTERED_MESSAGE));
				registerWithGCM();
				return;
			}
			if ((intent.getExtras().getString(TGConstants._GCM_ON_UNREGISTERED_MESSAGE) != null)) {
				return;
			}
			if ((intent.getExtras().getString(TGConstants._GCM_ON_ERROR) != null)) {
				if (intent.getExtras().getString(TGConstants._GCM_ON_ERROR).equals("ACCOUNT_MISSING")) {
					Toast.makeText(getApplicationContext(), getString(R.string.account_missing), Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getApplicationContext(), "New Message: " + intent.getExtras().getString(TGConstants._GCM_ON_ERROR),
							Toast.LENGTH_LONG).show();
				}
				if (loginProgressDialog != null)
					loginProgressDialog.dismiss();
				return;
			}
			// Releasing wake lock
			WakeLocker.release();
		}
	};
	
	// private final BroadcastReceiver mRegisteredOnGCMReceiver = new BroadcastReceiver() {
	// @Override
	// public void onReceive(Context context, Intent intent) {
	// // RegistrationIntentService.runIntentInService(context, intent);
	// // setResult(Activity.RESULT_OK, null, null);
	// // //
	// // Log.i(TAG, gcmRegid);
	// // if (GCMRegistrar.isRegisteredOnServer(LoginActivity.this)) {
	// //
	// // // }
	// // Bundle extras = intent.getExtras();
	// // if (extras != null) {
	// // Set<String> ks = extras.keySet();
	// // Iterator iterator = ks.iterator();
	// // while (iterator.hasNext()) {
	// // Log.d("KEY", (String) iterator.next());
	// // }
	// // }
	// // // see http://androidsnippets.com/how-to-discover-extras-from-intent
	// // if ((intent.getExtras().getString("registration_id") != null)) {
	// // gcmRegid = intent.getExtras().getString("registration_id");
	// // doPrepareLogin(gcmRegid);
	// // Log.i("TAG", "registration_id:" + intent.getExtras().getString("registration_id"));
	// // return;
	// // }
	// // // see http://androidsnippets.com/how-to-discover-extras-from-intent
	// // if ((intent.getExtras().getString("unregistered") != null)) {
	// // Log.i("TAG", "unregistered:" + intent.getExtras().getString("unregistered"));
	// // return;
	// // }
	// // doGCMRegistration(gcmRegid);
	// }
	// };
	
	private void registerWithGCM() {
		
		if (gcmRegid.equals("")) {
			gcmRegid = GCMRegistrar.getRegistrationId(LoginActivity.this);
		}
		
		if (gcmRegid.equals("")) {
			// Register with GCM
			Intent registrationIntent = new Intent("com.google.android.c2dm.intent.REGISTER");
			// sets the app name in the intent
			registrationIntent.putExtra("app", PendingIntent.getBroadcast(this, 0, new Intent(), 0));
			registrationIntent.putExtra("sender", TGConstants.SENDER_ID);
			startService(registrationIntent);
		} else if (GCMRegistrar.isRegisteredOnServer(this)) {
			startSessionActivity();
		} else {
			// Try to register again, but not in the UI thread.
			// It's also necessary to cancel the thread onDestroy(),
			// hence the use of AsyncTask instead of a raw thread.
			doPrepareLogin(gcmRegid);
		}
		
	}
	
	// private void registerWithServer() {
	// final Context context = this;
	// mRegisterTask = new AsyncTask<Void, Void, Void>() {
	//
	// @Override
	// protected Void doInBackground(Void... params) {
	// // Register on our server
	// ServerUtilities.register(context, Prefs.getLoginUserName(), Prefs.getLoginUserName(), gcmRegid);
	// return null;
	// }
	//
	// @Override
	// protected void onPostExecute(Void result) {
	// mRegisterTask = null;
	// }
	// };
	// mRegisterTask.execute(null, null, null);
	// }
}