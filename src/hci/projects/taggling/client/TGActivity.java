package hci.projects.taggling.client;

import hci.projects.taggling.GlobalState;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.google.android.maps.GeoPoint;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import hci.projects.taggling.client.csmessage.GameMessage;
import hci.projects.taggling.client.csmessage.PositionMessage;
import hci.projects.taggling.client.csmessage.ControlMessage;
import hci.projects.taggling.gps.IGPSActivity;
import hci.projects.taggling.gps.ParcelableGeoPoint;
import hci.projects.taggling.gps.PositionTracker;
import hci.projects.taggling.client.services.SendToServer;
import hci.projects.taggling.utils.TGConstants;
import hci.projects.taggling.utils.Prefs;
import hci.projects.taggling.R;
import hci.projects.tagglinglib.GameHandler;

public class TGActivity extends Activity {// implements IGPSActivity {
	/** Messenger for communicating with the service. */
	// Messenger xmppMessengerService = null;
	// GCMIntentService xmppClientService;
	/** Flag indicating whether we have called bind on the service. */
	protected GlobalState csApplication;
//	protected PositionTracker positionTracker;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//		mLockScreenRotation();
		csApplication = (GlobalState) this.getApplication();
//		positionTracker = new PositionTracker(this);
	}
	
	@Override
	protected void onStart() {
		keepScreenOn();
		super.onStart();
	}
	

	
	@Override
	protected void onStop() {
//		positionTracker.stopTracking();
		stopKeepingScreenOn();
		super.onStop();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	protected void mLockScreenRotation() {
		// Stop the screen orientation changing during an event
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	
	// protected void showScoreDialog(Context ctx) {
	// ScoreDialog scoreDialog = new ScoreDialog(ctx);
	// scoreDialog.show();
	// }
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == TGConstants.REQUEST_CODE_GAME_FINISH) {
			Intent startActivityIntent = new Intent();
			startActivityIntent.setClass(TGActivity.this, LoginActivity.class);
			startActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(startActivityIntent);
			finish();
		}
	}
	
	void keepScreenOn() {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
	
	// Clears the flag that keeps the screen on.
	void stopKeepingScreenOn() {
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
	
//	@Override
//	public void locationChanged(double longitude, double latitude, float accuracy) {
//		if (!GameHandler.getInstance().getMyPlayer().getPlayerName().equals(TGConstants._NO_PLAYER_NAME)) {
//			PositionMessage newPositionMessage = new PositionMessage(GameHandler.getInstance().getMyPlayer().getPlayerName(), new ParcelableGeoPoint(
//					new GeoPoint((int) (latitude * 1e6), (int) (longitude * 1e6))), accuracy);
//			
//			NameValuePair[] nameValuePairs = new NameValuePair[1];
//			nameValuePairs[0] = new BasicNameValuePair(TGConstants._TAG_XMPP_OUTBOUND_POSITIONMESSAGE, newPositionMessage.toXMPPMessage());
//			SendToServer sendToServer = new SendToServer();
//			sendToServer.execute(nameValuePairs);
//		}
//	}
//	
//	@Override
//	public void displayGPSSettingsDialog() {
//		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//		startActivity(intent);
//	}
//	
}