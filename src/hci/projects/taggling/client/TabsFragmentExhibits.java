package hci.projects.taggling.client;

import hci.projects.taggling.ListLoaders.ExhibitsListLoader;
import hci.projects.taggling.R;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class TabsFragmentExhibits extends Fragment implements OnTabChangeListener {

	private static final String TAG = "FLOOR_TAB_PRESSED";
	public static final String TAB_FLOOR_1 = "orofos1";
	public static final String TAB_FLOOR_2 = "orofos2";
	public static final String TAB_FLOOR_3 = "orofos3";

	private View mRoot;
	private TabHost mTabHost;
	private int mCurrentTab;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{
		mRoot = inflater.inflate(R.layout.tabs_fragment, null);
		mRoot.findViewById(R.id.player_info).setVisibility(View.GONE);
		mTabHost = (TabHost) mRoot.findViewById(android.R.id.tabhost);
		
		getActivity().getActionBar().setTitle(R.string.frag_name_exhibits);
		
		setupTabs();
		updateTab(TAB_FLOOR_1, R.id.tab_1);
		
		return mRoot;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);

		mTabHost.setOnTabChangedListener(this);
		mTabHost.setCurrentTab(mCurrentTab);
		
//		// Set the info text
//		((TextView) getActivity().findViewById(R.id.info_text_2))
//				.setText(R.string.info_exhibit);
		
		updateTab(TAB_FLOOR_1, R.id.tab_1);
	}

	private void setupTabs() {
		mTabHost.setup(); // important!
		mTabHost.addTab(newTab(TAB_FLOOR_1, R.string.tab_orofos_1, R.id.tab_1));
		mTabHost.addTab(newTab(TAB_FLOOR_2, R.string.tab_orofos_2, R.id.tab_2));
		//mTabHost.addTab(newTab(TAB_FLOOR_3, R.string.tab_orofos_3, R.id.tab_3));
	}

	/** Create the new tab by indicating a tag, a View
	 *  and a corresponding View Id
	 **/
	private TabSpec newTab(String tag, int labelId, int tabContentId) {
		View indicator = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab,
				(ViewGroup) mRoot.findViewById(android.R.id.tabs), false);
		((TextView) indicator.findViewById(R.id.text)).setText(labelId);

		TabSpec tabSpec = mTabHost.newTabSpec(tag);
		tabSpec.setIndicator(indicator);
		tabSpec.setContent(tabContentId);
		return tabSpec;
	}

	@Override
	public void onTabChanged(String tabId) {
		if (TAB_FLOOR_1.equals(tabId)) {
			updateTab(tabId, R.id.tab_1);
			mCurrentTab = 0;
			return;
		}
		if (TAB_FLOOR_2.equals(tabId)) {
			updateTab(tabId, R.id.tab_2);
			mCurrentTab = 1;
			return;
		}
		if (TAB_FLOOR_3.equals(tabId)) {
			updateTab(tabId, R.id.tab_3);
			mCurrentTab = 2;
			return;
		}
	}

	private void updateTab(String tabId, int placeholder) {
		FragmentManager fm = getFragmentManager();
		
		ExhibitsListLoader listLoader = new ExhibitsListLoader();
		
		Bundle args = new Bundle();
		args.putString(TAG, tabId);
		listLoader.setArguments(args);

		fm.beginTransaction().replace(placeholder, listLoader, tabId)
				.commitAllowingStateLoss();
	}

}
