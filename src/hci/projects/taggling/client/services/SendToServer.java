package hci.projects.taggling.client.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.util.Log;
import hci.projects.taggling.utils.TGConstants;

public class SendToServer extends AsyncTask<NameValuePair, Void, String> {
	private static final String TAG = "SendToServer";
	private static final String REMOTE_REQUEST_FAILED = "RemoteRequestFailed";
	boolean result = false;
	private String response = "";
	
	@Override
	protected String doInBackground(NameValuePair... params) {
		Thread.currentThread().setName("SendToServer");
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(params.length);
			for (NameValuePair nvpair : params) {
				nameValuePairs.add(nvpair);
			}
			
			HttpPost httppost = new HttpPost(TGConstants.SERVER_URL);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			// FIXME: timeout
			HttpParams httpParameters = new BasicHttpParams();
			DefaultHttpClient httpclient = new DefaultHttpClient(httpParameters);
			
			HttpResponse responsePost = httpclient.execute(httppost);
			HttpEntity resEntityPost = responsePost.getEntity();
			if (resEntityPost != null) {
				try {
					response = EntityUtils.toString(resEntityPost);
				} catch (Exception e) {
					Log.e(TAG, "3 " + e.toString());
				}
			}
		} catch (ClientProtocolException cpe) {
			response = REMOTE_REQUEST_FAILED;
			Log.e(TAG, "1 " + cpe.toString());
		} catch (IOException ioe) {
			response = REMOTE_REQUEST_FAILED;
			Log.e(TAG, "2 " + ioe.toString());
		}
		return response;
	}
	
	@Override
	protected void onPostExecute(String result) {
		if (result.equals(REMOTE_REQUEST_FAILED)) {
			//Toast.makeText(getApplicationContext(), getString(R.string.xmppclientservice_not_connected), Toast.LENGTH_SHORT).show();
		}
	}
}