package hci.projects.taggling.client;

import hci.projects.taggling.R;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentLogin extends Fragment implements View.OnClickListener{
	
	public interface Listener {
        public void onSignInButtonClicked();
        public void onSignOutButtonClicked();
        public void onPlayButtonClicked();
        public void onExitGameClicked();
        //public void lockDrawerClosed(boolean locked);
        public void askForPlayerName();
		public void onOfflineSoloGameClicked();
    }

    Listener mListener = null;
    boolean showSignIn = true;
    String playerName;
    TextView welcomeText;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        
        welcomeText = (TextView) v.findViewById(R.id.welcome_player);
        
        welcomeText.setText(
        		getString(R.string.welcome_player) + " " +playerName + "!");
        
        final int[] CLICKABLES = new int[] {
                R.id.button_play, R.id.exit_game, R.id.button_enter_name,
                R.id.sign_in_button, R.id.sign_out_button, R.id.solo_game
        };
        for (int i : CLICKABLES) {
            v.findViewById(i).setOnClickListener(this);
        }
        // hide the Drawer and the ActionBar for now
        //mListener.lockDrawerClosed(true);
        getActivity().getActionBar().hide();
        
        return v;
	}
	
	public void setListener(Listener l) {
        mListener = l;
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUi();
    }

    void updateUi() {
        if (getActivity() == null) return;
        
        getActivity().findViewById(R.id.screen_sign_in).setVisibility(showSignIn ?
                View.VISIBLE : View.GONE);
        getActivity().findViewById(R.id.screen_enter_menu).setVisibility(showSignIn ?
                View.GONE : View.VISIBLE);
        
        welcomeText.setText(
        		getString(R.string.welcome_player) + " " +playerName + "!");
        
        Log.d(null, "UI updated! to " + Boolean.toString(showSignIn));
    }
    
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
        case R.id.sign_in_button:
            mListener.onSignInButtonClicked();
            break;
        case R.id.sign_out_button:
            mListener.onSignOutButtonClicked();
            break;
        case R.id.button_play:
        	mListener.onPlayButtonClicked();
        	break;
        case R.id.exit_game:
        	mListener.onExitGameClicked();
        	break;
        case R.id.solo_game:
        	mListener.onOfflineSoloGameClicked();
        	break;
        case R.id.button_enter_name:
        	mListener.askForPlayerName();
        }
	}
	
	void setShowSignIn(boolean signedIn) {
		showSignIn = signedIn;
		updateUi();
	}
	
	void setPlayerName(String plName) {
		playerName = plName;
		updateUi();
	}

	protected void removeProgressBar() {
		getActivity().findViewById(R.id.text_check_version).setVisibility(View.GONE);
		getActivity().findViewById(R.id.text_download_new_content).setVisibility(View.GONE);
		getActivity().findViewById(R.id.progress_bar).setVisibility(View.GONE);
		
	}
	
}
