package hci.projects.taggling;

import static hci.projects.taggling.utils.TGConstants.SENDER_ID;
import static hci.projects.taggling.utils.Utils.broadcastMessage;
import hci.projects.taggling.client.csmessage.ControlMessage;
import hci.projects.taggling.client.csmessage.queue.QueueMessage;
import hci.projects.taggling.utils.TGConstants;
import hci.projects.taggling.utils.Utils;
import hci.projects.tagglinglib.GameHandler;
import hci.projects.tagglinglib.Messages.MessageExhibitLock;
import hci.projects.tagglinglib.Messages.MessageExhibitUnlock;
import hci.projects.tagglinglib.Messages.MessageExhibitUpdate;
import hci.projects.tagglinglib.Messages.MessageTaggling.MessageType;

import java.util.Iterator;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {
	
	private static final String TAG = "GCMIntentService";
	
	public GCMIntentService() {
		super(SENDER_ID);
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	protected void onRegistered(Context context, String registrationId) {
		Utils.broadcastMessage(context, TGConstants._GCM_ON_REGISTERED_MESSAGE, registrationId);
	}
	
	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Utils.setRegisteredOnServer(context, false);
		Utils.broadcastMessage(context, TGConstants._GCM_ON_UNREGISTERED_MESSAGE, registrationId);
	}
	
	/**
	 * @param context
	 * @param intent
	 */
	/**
	 * @param context
	 * @param intent
	 */
	/**
	 * @param context
	 * @param intent
	 */
	@Override
	protected void onMessage(Context context, Intent intent) {
		String incomingMessage;
		
		if (intent.getExtras().getString(MessageType.EXHIBIT_UPDATE.toString()) != null) {
			incomingMessage = intent.getExtras().getString(MessageType.EXHIBIT_UPDATE.toString());
			try {
				JSONObject jsonString = new JSONObject(incomingMessage);
				MessageExhibitUpdate incomingMessageExhibitUpdate = new MessageExhibitUpdate(jsonString);
				// messageKey is in fact the id of the exhibit. The messageKey is used to find the correct message in the messageQueue
				int messageKey = incomingMessageExhibitUpdate.getObjectId();
				boolean res = GlobalState.messageQueue.containsKey(messageKey);
				if (res) {
					QueueMessage csQueueMessage = (QueueMessage) GlobalState.messageQueue.get(messageKey);
					// check if we are still waiting for the response. If yes, then mark the message as returned (it does not really check, just marks it as
					// returned->success
					if (csQueueMessage.getPlayerId() == incomingMessageExhibitUpdate.getPlayerId()) {
						GlobalState.messageQueue.put(messageKey, new QueueMessage(incomingMessageExhibitUpdate,
								QueueMessage.HAS_RETURNED));
					} else {
						// this is a response for the same key but by another player this means that the key was taken before, but i didn't find out until
						// now remove my entry from csMessageQueue
						GlobalState.messageQueue.remove(messageKey);
					}
				}
				broadcastMessage(context, incomingMessageExhibitUpdate);
			} catch (JSONException e) {
				Log.e(TAG, "onMessage(): " + incomingMessage + ", e:" + e.toString());
			}
			// } else if (intent.getExtras().getString(TGConstants._GAME_INITIALIZATIONMESSAGE) != null) {
			// incomingMessage = intent.getExtras().getString(TGConstants._GAME_INITIALIZATIONMESSAGE);
			//
			// MessageInitialization incomingInitializationMessage = new MessageInitialization(incomingMessage);
			//
			// broadcastMessage(context, incomingInitializationMessage);
			
		} else if (intent.getExtras().getString(MessageType.EXHIBIT_UNLOCK.toString()) != null) {
			incomingMessage = intent.getExtras().getString(MessageType.EXHIBIT_UNLOCK.toString());
			try {
				JSONObject jsonString = new JSONObject(incomingMessage);
				MessageExhibitUnlock incomingUnlockMessage = new MessageExhibitUnlock(jsonString);
				// messageKey is in fact the id of the exhibit. The messageKey is used to find the correct message in the messageQueue
				int messageKey = incomingUnlockMessage.getObjectId();
				boolean res = GlobalState.messageQueue.containsKey(messageKey);
				if (res) {
					QueueMessage csQueueMessage = (QueueMessage) GlobalState.messageQueue.get(messageKey);
					// check if we are still waiting for the response. If yes, then mark the message as returned (it does not really check, just marks it as
					// returned->success
					if (csQueueMessage.getPlayerId() == incomingUnlockMessage.getPlayerId()) {
						GlobalState.messageQueue.put(messageKey, new QueueMessage(incomingUnlockMessage, QueueMessage.HAS_RETURNED));
					} else {
						// this is a response for the same key but by another player this means that the key was taken before, but i didn't find out until
						// now remove my entry from csMessageQueue
						GlobalState.messageQueue.remove(messageKey);
					}
				}
				broadcastMessage(context, incomingUnlockMessage);
			} catch (JSONException e) {
				Log.e(TAG, "onMessage(): " + incomingMessage + ", e:" + e.toString());
			}
			
			// }
			// else if (intent.getExtras().getString(TGConstants._GAME_SCANMESSAGE) != null) {
			//
			// incomingMessage = intent.getExtras().getString(TGConstants._GAME_SCANMESSAGE);
			// JSONObject jsonObject;
			// try {
			// jsonObject = new JSONObject(incomingMessage);
			//
			// ScanMessage incomingScanMessage = new ScanMessage(jsonObject);
			//
			// broadcastMessage(context, incomingScanMessage);
			// } catch (JSONException e) {
			// e.printStackTrace();
			// }
			//
		} else if (intent.getExtras().getString(MessageType.EXHIBIT_LOCK.toString()) != null) {
			incomingMessage = intent.getExtras().getString(MessageType.EXHIBIT_LOCK.toString());
			try {
				JSONObject jsonString = new JSONObject(incomingMessage);
				MessageExhibitLock incomingLockMessage = new MessageExhibitLock(jsonString);
				// messageKey is in fact the id of the exhibit. The messageKey is used to find the correct message in the messageQueue
				int messageKey = incomingLockMessage.getObjectId();
				boolean res = GlobalState.messageQueue.containsKey(messageKey);
				if (res) {
					int playerId = incomingLockMessage.getPlayerId();
					QueueMessage csQueueMessage = (QueueMessage) GlobalState.messageQueue.get(messageKey);
					if (playerId == GameHandler.getInstance().getPlayer().getPlayerId()) {
						// check if we are still waiting for the response. If yes, then mark the message as returned (it does not really check, just marks
						// it as
						// returned->success
						if (csQueueMessage.getPlayerId() == incomingLockMessage.getPlayerId()) {
							GlobalState.messageQueue.put(messageKey, new QueueMessage(incomingLockMessage,
									QueueMessage.HAS_RETURNED));
						} else {
							// this is a response for the same key but by another player this means that the key was taken before, but i didn't find out
							// until
							// now remove my entry from csMessageQueue
							GlobalState.messageQueue.remove(messageKey);
						}
					}
				}
				broadcastMessage(context, incomingLockMessage);
			} catch (JSONException e) {
				Log.e(TAG, "onMessage(): " + incomingMessage + ", e:" + e.toString());
			}
			
			// }
			// else if (intent.getExtras().getString(TGConstants._GAME_SCANMESSAGE) != null) {
			//
			// incomingMessage = intent.getExtras().getString(TGConstants._GAME_SCANMESSAGE);
			// JSONObject jsonObject;
			// try {
			// jsonObject = new JSONObject(incomingMessage);
			//
			// ScanMessage incomingScanMessage = new ScanMessage(jsonObject);
			//
			// broadcastMessage(context, incomingScanMessage);
			// } catch (JSONException e) {
			// e.printStackTrace();
			// }
			//
		} else if (intent.getExtras().getString(TGConstants._GAME_CONTROLMESSAGE) != null) {
			incomingMessage = intent.getExtras().getString(TGConstants._GAME_CONTROLMESSAGE);
			ControlMessage incomingControlMessage = new ControlMessage(incomingMessage);
			broadcastMessage(context, incomingControlMessage);
		}
		// else if (intent.getExtras().getString(TGConstants._GAME_POINTSMESSAGE) != null) {
		// incomingMessage = intent.getExtras().getString(TGConstants._GAME_POINTSMESSAGE);
		// String pointsJson = "";
		// try {
		// pointsJson = intent.getExtras().getString(TGConstants._GAME_POINTSMESSAGE);
		// } catch (Exception e) {
		// }
		// PointsMessage incomingPointsMessage = new PointsMessage(pointsJson);
		// broadcastMessage(context, incomingPointsMessage);
		// }
		Bundle extras = intent.getExtras();
		if (extras != null) {
			Set<String> ks = extras.keySet();
			Iterator iterator = ks.iterator();
			while (iterator.hasNext()) {
				Log.d("KEY", (String) iterator.next());
			}
		}
	}
	
	/**
	 * Method called on receiving a deleted message
	 */
	@Override
	protected void onDeletedMessages(Context context, int total) {
		Log.e(TAG, "onDeletedMessages: Received deleted messages notification" + String.valueOf(total));
		broadcastMessage(context, TGConstants._GCM_ON_DELETED_MESSAGES, String.valueOf(total));
	}
	
	/**
	 * Method called on Error
	 */
	@Override
	public void onError(Context context, String errorId) {
		Log.e(TAG, "Received error: " + errorId);
		broadcastMessage(context, TGConstants._GCM_ON_ERROR, errorId);
	}
	
	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		Log.e(TAG, "Received recoverable error: " + errorId);
		// broadcastMessage(context, getString(R.string.gcm_recoverable_error,
		// errorId));
		return super.onRecoverableError(context, errorId);
	}
	
	// /**
	// * Issues a notification to inform the user that server has sent a message.
	// */
	// private static void generateNotification(Context context, String message) {
	// int icon = R.drawable.launcher;
	// long when = System.currentTimeMillis();
	// NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	// Notification notification = new Notification(icon, message, when);
	//
	// String title = context.getString(R.string.app_name);
	//
	// Intent notificationIntent = new Intent(context, CityScrabbleActivity.class); // FIXME
	// // set intent so it does not start a new activity
	// notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	// PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
	// notification.setLatestEventInfo(context, title, message, intent);
	// notification.flags |= Notification.FLAG_AUTO_CANCEL;
	//
	// // Play default notification sound
	// notification.defaults |= Notification.DEFAULT_SOUND;
	//
	// // notification.sound = Uri.parse("android.resource://" +
	// // context.getPackageName() + "your_sound_file_name.mp3");
	//
	// // Vibrate if vibrate is enabled
	// notification.defaults |= Notification.DEFAULT_VIBRATE;
	// notificationManager.notify(0, notification);
	// }
	
}
