package hci.projects.tagglinglib;

import hci.projects.tagglinglib.GameElements.Exhibit;
import hci.projects.tagglinglib.GameElements.ExhibitList;
import hci.projects.tagglinglib.GameElements.Player;
import hci.projects.tagglinglib.GameElements.Tag;
import hci.projects.tagglinglib.GameElements.TagCategoryList;
import hci.projects.tagglinglib.GameElements.TagList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import android.util.Log;

public class GameHandler {
	private static final String DEBUG_TAG = "GameHandler";
	
	private static GameHandler instance;
	
	private Player mPlayer;
	private ExhibitList gameExhibits;
	private TagCategoryList gameTagCategories;
	private TagList gameTags;
	
	public MyColors colors;
	
	public static synchronized GameHandler getInstance() {
		if (instance == null) {
			instance = new GameHandler();
		}
		return instance;
	}
	
	public GameHandler() {
		this.gameExhibits = new ExhibitList();
		this.gameTagCategories = new TagCategoryList();
		this.gameTags = new TagList();
		this.colors = new MyColors();
	}
	
	public void addMyPlayer(Player myPlayer) {
		this.mPlayer = myPlayer;
	}
	
	/*
	 * * * GETTERS * * * * * * * * * * * * * * * * * *
	 * *
	 * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	
	public ExhibitList getGameExhibits() {
		return gameExhibits;
	}
	
	public TagList getGameTags() {
		return gameTags;
	}
	
	public TagCategoryList getGameTagCategories() {
		return gameTagCategories;
	}
	
	public Player getPlayer() {
		return mPlayer;
	}
	
	/*
	 * * * MESSAGE HELPER - UPDATERS * * * * * * * * * * * * * * * * *
	 * Updating game elements as instructed by the messages received *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	
	/*
	 * Update the tags in an ExhibitSlot with the new tags it should hold
	 * according to the indexes sent by a MessageUpdate
	 */
	public void setExhibitTags(int exhibitId, Integer[] iTags) {
		Exhibit exhibit = gameExhibits.getExhibitById(exhibitId);
		TagList newTagList = new TagList();
		for (int tagId : iTags) {
			if (tagId == Tag._NO_TAG_ID) {
				newTagList.add(Tag._NO_TAG);
			} else {
				newTagList.add(gameTags.getTagById(tagId));
			}
		}
		exhibit.setTags(newTagList);
		Log.d(DEBUG_TAG, "Exhibit: " + Integer.toString(exhibitId) + " iTags: " + Arrays.toString(iTags));
	}
	
	/*
	 * Update the Player Inventory with the new tags it should hold
	 * according to the indexes sent by a MessageUpdate
	 */
	public void setPlayerTags(Integer[] iTags) {
		// Log.d(DEBUG_TAG, mPlayer.getPlayerInventory().toString());
		Log.d(DEBUG_TAG, Arrays.toString(iTags));
		// valid check
		if (iTags.length > mPlayer.getPlayerInventory().size()) {
			Log.e(DEBUG_TAG, "Int array NOT VALID: Size > Inventory");
			return;
		}
		TagList newTagList = new TagList();
		
		// clear inventory
		mPlayer.getPlayerInventory().clear();// FIXME: Γιατί;
		// parse iTags and add
		for (int i : iTags) {
			newTagList.add(gameTags.getTagById(i));
		}
		mPlayer.clearAndAddToInventory(newTagList);
	}
	
	/*
	 * Sets tags to the exhibit slots as specified by the initialization message on every game session started
	 */
	public void initExhibitSlots(HashMap<Integer, ArrayList<Integer>> multiArr) {
		//FIXME: Δεν πρέπει να κάνει και clear πρώτα σε ΟΛΑ τα exhibits;
		for (HashMap.Entry<Integer, ArrayList<Integer>> entry : multiArr.entrySet()) {
			for (Integer tagId : entry.getValue()) {
				gameExhibits.getExhibitById(entry.getKey()).attachTag(gameTags.getTagById(tagId));
			}
		}
	}
	
	public void initPlayerSlots(ArrayList<Integer> inventoryIndexes) {
		TagList newTagList = new TagList();
		for (int tagId : inventoryIndexes) {
			newTagList.add(gameTags.getTagById(tagId));
		}
		mPlayer.clearAndAddToInventory(newTagList);
	}
	
	/*
	 * * * GENERAL UTILITIES * * * * * * * * * * * * * * * * * *
	 * Functions that help with some core functions of the game *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 */
	
	/**
	 * Is it the END of the game?
	 * Check if the ALL the exhibits in the game are completed
	 * 
	 * @return true/false
	 */
	public boolean isGameCompleted() {
		for (Exhibit e : gameExhibits) {
			if (!e.isCompleted()) {
				return false;
			}
		}
		return true;
	}
}
