package hci.projects.tagglinglib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;

public class LoadData {
	private final static String DEBUG_TAG = "Taggling LoadData";
	
	private static String URL_VERSION_CONTROL = "http://www.taggling.info/game/version_control.json";
	private static String URL_CONTENT_JSON = "http://www.taggling.info/game/game.json";
	private static String FILENAME_DATA_JSON = "gamedata.json";
	
	public static void local(Context ctx, GameHandler gH) {
		String locale = ctx.getResources().getConfiguration().locale.getISO3Language();
		
		// Open file in assets and load stream in json string
		
		StringBuilder json = new StringBuilder();
		try {
			InputStream iBuf;
			iBuf = ctx.getAssets().open(locale + "_" + FILENAME_DATA_JSON);
			BufferedReader in = new BufferedReader(new InputStreamReader(iBuf));
			String str;
			while ((str = in.readLine()) != null) {
				json.append(str);
			}
			in.close();
		} catch (IOException e) {
			Log.e(DEBUG_TAG, "gamedata.json NOT READ correctly");
			e.printStackTrace();
		}
		
		// Append json elements to game objects
		
		JsonHelper jHelper = new JsonHelper();
		jHelper.append(json.toString(), gH);
	}
	
	public static void external(GameHandler gH) {
		String jsonString = fetchJsonString(URL_CONTENT_JSON);
		Log.d(DEBUG_TAG, jsonString);
		
		// Append json elements to game objects
		//
		JsonHelper jHelper = new JsonHelper();
		jHelper.append(jsonString, gH);
		
	}
	
	public static String version() {
		return fetchJsonString(URL_VERSION_CONTROL);
	}
	
	/**
	 * Retrieve the json string from the server
	 * 
	 * @param The
	 *                file URL
	 * @return json string representation
	 */
	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	private static String fetchJsonString(String url) {
		
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				Log.e("NET_ERR", "Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}
}
