package hci.projects.tagglinglib.GameElements;

import java.util.ArrayList;

public class TagList extends ArrayList<Tag> {
	private static final long serialVersionUID = -1323964411035358393L;
	
	private int capacity;
	
	public TagList() {
		this.clear();
	}
	
	public TagList(int slotsCapacity) {
		super(slotsCapacity);
		this.capacity = slotsCapacity;
	}
	
	/**
	 * clears this and adds new Tags
	 * 
	 * @param tagList
	 * @return
	 */
	public boolean clearAndAddAll(TagList tagList) {
		if (tagList.size() > capacity) {
			return false;
		}
		this.clear();
		for (Tag tag : tagList) {
			if ((tag != null) && !(tag.isNull())) {
				this.add(tag);
			}
			else {
				this.add(Tag._NO_TAG);
			}
		}
		return true;
	}
	
	/**
	 * Gets the tag with the specific id
	 * 
	 * @return Tag or null if it doesn't exist
	 */
	public Tag getTagById(int tagId) {
		for (Tag tag : this) {
			if (tag.getTagId() == tagId) {
				return tag;
			}
		}
		return null;
	}
	
}
