package hci.projects.tagglinglib.GameElements;

public class Team {

	/**
	 * The name of the team
	 */
	private String teamName;

	/**
	 * The color of the team
	 */
	private int teamColor;
	
	/**
	 * Total points for the team
	 */
	private int teamPoints;

	/**
	 * @param teamName
	 * @param teamColor
	 */
	public Team(String teamName, int teamColor) {
		this.teamName = teamName;
		this.teamColor = teamColor;
		this.teamPoints = 0;
	}

	/**
	 * @return the teamName
	 */
	public String getTeamName() {
		return teamName;
	}

	/**
	 * @return the teamColor
	 */
	public int getTeamColor() {
		return teamColor;
	}
	
	public boolean addPoints(Player fromPlayer, int pointsToAdd) {
		if (fromPlayer.getPlayerTeam().equals(teamName)) {
			teamPoints = teamPoints + pointsToAdd;
			return true;
		} else {
			return false;
		}
	}

}
