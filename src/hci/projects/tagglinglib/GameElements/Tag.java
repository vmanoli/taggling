package hci.projects.tagglinglib.GameElements;

import java.util.ArrayList;

public class Tag {
	public static final String _NO_TAG_NAME = "";
	public static final int _NO_TAG_ID = -1;
	public static final int _NO_TAG_POINTS = 0;
	public static final Tag _NO_TAG = new Tag(Tag._NO_TAG_ID, Tag._NO_TAG_NAME, Tag._NO_TAG_POINTS, new ArrayList<Exhibit>(), TagCategory._NO_CATEGORY,
			Hint._NO_HINT);
	
	/**
	 * The name of the tag
	 */
	private String tagName = _NO_TAG_NAME;
	
	/**
	 * The id of the tag
	 */
	private int tagId = _NO_TAG_ID;
	
	/**
	 * The points that this tag is worth. Points are awarded only if the tag has been attached an exhibit in the {@link #tagValidExhibitsList}
	 */
	private int tagPoints = _NO_TAG_POINTS;
	
	/**
	 * The list of exhibits this tag can be attached to and give points to the player
	 */
	private ArrayList<Exhibit> tagValidExhibitsList;
	
	/**
	 * The {@link TagCategory} that this tag belongs to
	 */
	private TagCategory tagCategory;
	
	/**
	 * The hint to display
	 */
	private Hint hint;
	
	/**
	 * Full constructor
	 * 
	 * @param tagId
	 * @param tagName
	 * @param tagPoints
	 * @param tagValidExhibitsList
	 * @param tagCategory
	 */
	public Tag(int tagId, String tagName, int tagPoints, ArrayList<Exhibit> tagValidExhibitsList, TagCategory tagCategory, Hint tagHint) {
		this.tagId = tagId;
		this.tagName = tagName;
		this.tagPoints = tagPoints;
		this.tagValidExhibitsList = tagValidExhibitsList;
		this.tagCategory = tagCategory;
		this.hint = tagHint;
	}
	
	public String getTagName() {
		return tagName;
	}
	
	public int getTagId() {
		return tagId;
	}
	
	public int getTagPoints() {
		return tagPoints;
	}
	
	public TagCategory getTagCategory() {
		return tagCategory;
	}
	
	public Hint getHint() {
		return hint;
	}
	
	public boolean hasHint() {
		return hint.isAvailable();
	}
	
	public boolean isNull() {
		if (tagId == _NO_TAG_ID) {
			return true;
		} else {
			return false;
		}
	}
	
	@SuppressWarnings("unused")
	private ArrayList<Exhibit> getTagValidExhibitsList() {
		return tagValidExhibitsList;
	}
	
	public boolean addValidExhibit(Exhibit exhibit) {
		for (Exhibit e : tagValidExhibitsList) {
			if (exhibit.equals(e))
				return false;
		}
		return this.tagValidExhibitsList.add(exhibit);
	}
	
	/**
	 * Converts Color object of tagCategory to an integer
	 * 
	 * @return An integer
	 */
	public int getTagColor() {
		int tagColorValue;
		tagColorValue = tagCategory.getTagCategoryColor();
		return tagColorValue;
	}
	
	/**
	 * Checks if this tag is valid for the exhibitToCheck.
	 * 
	 * @param exhibitToCheck
	 * @return true/false
	 */
	public boolean isValidForExhibit(Exhibit exhibitToCheck) {
		for (Exhibit exhibit : tagValidExhibitsList) {
			if (exhibitToCheck.equals(exhibit))
				return true;
		}
		return false;
	}
	
	public String toString() {
		return tagName + "(" + String.valueOf(tagId) + ");";
	}
	
}
