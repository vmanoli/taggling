package hci.projects.tagglinglib.GameElements;

import android.graphics.Color;
import hci.projects.tagglinglib.GameElements.Hint.HintType;

public class TagCategory {
	private static final String _NO_TAG_CATEGORY_NAME = "No name";
	private static final int _NO_TAG_CATEGORY_COLOR = Color.LTGRAY;
	private static final String _NO_TAG_CATEGORY_DESCRIPTION = "No category description";
	public static final TagCategory _NO_CATEGORY = new TagCategory(_NO_TAG_CATEGORY_NAME, _NO_TAG_CATEGORY_COLOR, _NO_TAG_CATEGORY_DESCRIPTION);
	
	/**
	 * The name of the tag category
	 */
	private String tagCategoryName;
	
	/**
	 * The color of the tag category
	 */
	private int tagCategoryColor;
	
	/**
	 * A description of the tag category
	 */
	private String tagCategoryDescription;
	
	/**
	 * @param tagCategoryName
	 * @param tagCategoryColor
	 */
	public TagCategory(String tagCategoryName, int tagCategoryColor, String tagCategoryDescription) {
		this.tagCategoryName = tagCategoryName;
		this.tagCategoryColor = tagCategoryColor;
		this.tagCategoryDescription = tagCategoryDescription;
	}
	
	/**
	 * @return the tagCategoryName
	 */
	public String getTagCategoryName() {
		return tagCategoryName;
	}
	
	/**
	 * @return the tagCategoryDescription
	 */
	public String getTagCategoryDescription() {
		return tagCategoryDescription;
	}
	
	/**
	 * @param tagCategoryDescription
	 *                the tagCategoryDescription to set
	 */
	public void setTagCategoryDescription(String tagCategoryDescription) {
		this.tagCategoryDescription = tagCategoryDescription;
	}
	
	/**
	 * @return the tagCategoryColor
	 */
	public int getTagCategoryColor() {
		return tagCategoryColor;
	}
	
}
