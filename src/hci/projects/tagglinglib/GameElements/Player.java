/**
 * 
 */
package hci.projects.tagglinglib.GameElements;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.json.JSONObject;

import android.graphics.Color;
import android.util.Log;

/**
 * @author vicky
 */
public class Player {
	public static final int __INVENTORY_MAXSLOTS__ = 7;
	public static final int _NO_PLAYER_ID = -1;
	
	private int playerId;
	private String playerName;
	private Team playerTeam;
	private Inventory inventory;
	private int _playerTotalPoints;
	// private PointsTable pointTable;
	
	private final String TAG = "Player";
	
	/**
	 * constructor with team setter
	 * 
	 * @param playerId
	 * @param playerName
	 * @param playerTotalPoints
	 * @param playerTeam
	 */
	
	public Player(JSONObject jsonString) {
		try {
			this.playerId = jsonString.getInt("playerId");
			this.playerName = jsonString.getString("playerName");
			this.playerTeam = new Team(jsonString.getString("playerTeam"), Color.parseColor(jsonString.getString("teamColor")));
			this.inventory = new Inventory(__INVENTORY_MAXSLOTS__);
			// this.pointTable = new PointsTable();
		} catch (Exception e) { // FIXME: Test what happens if the parsing is aborted
			Log.e(TAG, "Couldn't parse player JSON string: " + jsonString.toString());
		}
	}
	
	/**
	 * @return the playerId
	 */
	public int getPlayerId() {
		return playerId;
	}
	
	/**
	 * @return the playerName
	 */
	public String getPlayerName() {
		return playerName;
	}
	
	public int getMaxSlots() {
		return __INVENTORY_MAXSLOTS__;
	}
	
	/**
	 * @return the Player total points
	 */
	public int getPlayerTotalPoints() {
		return _playerTotalPoints;
	}
	
	/**
	 * Get the player's points gained only out of the specific exhibit
	 * 
	 * @param the
	 *                exhibit name
	 * @return the playerPoints
	 */
	// public int getPlayerPoints(String exhibitName) {
	// int playerPoints = 0;
	// for (PointsExhibitPair pair : pointTable) {
	// if (pair.exhibitName.equals(exhibitName)) {
	// playerPoints = playerPoints + pair.points;
	// }
	// }
	// return playerPoints;
	// }
	
	private int getPlayerPoints() {
		return this._playerTotalPoints;
	}
	
	/**
	 * @return the playerTeam
	 */
	public Team getPlayerTeam() {
		return playerTeam;
	}
	
	/**
	 * Adding points gained to the specific exhibit in the table
	 * 
	 * @param name
	 * @param p
	 * @return False if a new pair was created, True if the points were added to
	 *         an existing one
	 */
	// public boolean addPoints(String name, int p) {
	// if (!pointTable.isEmpty()) {
	// for (PointsExhibitPair pair : pointTable) {
	// if (pair.exhibitName.equals(name)) {
	// pair.addPointsGained(p);
	// return true;
	// }
	// }
	// }
	// pointTable.add(new PointsExhibitPair(name, p));
	// return false;
	// }
	
	public void setPlayerTotalPoints(int newTotalPoints) {
		this._playerTotalPoints = newTotalPoints;
	}
	
	/**
	 * Gets the {@link Player:player's} {@link Inventory}
	 * 
	 * @return
	 */
	public Inventory getPlayerInventory() {
		return this.inventory;
	}
	
	public void clearAndAddToInventory(TagList newTags) {
		this.inventory.clearAndAddAll(newTags);
	}
	
	public int countInventoryEmptySlots() {
		return this.inventory.countEmptySlots();
	}
	
	public Tag popTagFromInventory(int indexOfTagToPop) {
		Tag tag = this.inventory.get(indexOfTagToPop);
		this.inventory.remove(indexOfTagToPop);
		return tag;
	}
//	
//	public PointsTable getPointsTable() {
//		return pointTable;
//	}
	
	public class Inventory extends TagList {
		private static final long serialVersionUID = -304703180557917885L;
		private int capacity = Player.__INVENTORY_MAXSLOTS__;
		
		/**
		 * Constructor that instantiates a TagList with initial capacity and initializes it with empty Tags
		 * 
		 * @param slotsCapacity
		 * @return
		 */
		public Inventory(int slotsCapacity) {
			super(slotsCapacity);
			this.capacity = slotsCapacity;
			for (int i = 0; i < slotsCapacity; i++) {
				super.add(Tag._NO_TAG); // use the super.add method because the local method is overridden
			}
		}
		
		/**
		 * Adds a Tag at the first empty spot of this Inventory or returns false if no more empty spots are available
		 * 
		 * @param tag
		 * @return
		 */
		@Override
		public boolean add(Tag tag) {
			if (this.contains(tag))
			{
				return true;
			}
			if (this.countEmptySlots() > 0) {
				this.set(capacity - countEmptySlots(), tag);
				return true;
				// throw new IllegalStateException("The Inventory is full");
			} else {
				return false;
			}
		}
		
		/**
		 * replaces a tag
		 * 
		 * @param index
		 * @param newTag
		 * @return
		 */
		public Tag replace(int index, Tag newTag) {
			return this.set(index, newTag);
		}
		
		@Override
		public boolean remove(Object tag) {
			this.set(this.indexOf(tag), Tag._NO_TAG);
			return true;
		}
		
		@Override
		public Tag remove(int index) {
			Tag tagToRemove = get(index);
			this.set(index, Tag._NO_TAG);
			return tagToRemove;
		}
		
		@Override
		public void clear() {
			for (Tag tag : this) {
				this.remove(tag);
			}
		}
		
		private int countEmptySlots() {
			int c = 0;
			for (Tag tag : this) {
				if (tag.isNull()) {
					c++;
				}
			}
			return c;
		}
		
		/**
		 * returns the contents of the inventory in the form of an array of integer in order to transmit it to the remote server
		 * 
		 * @return
		 */
		public Integer[] getTagsArray() {
			Integer[] iTags = new Integer[this.size()];
			int i = 0;
			for (Tag tag : this) {
				iTags[i] = tag.getTagId();
				i++;
			}
			return iTags;
		}
	}
//	
//	/**
//	 * Class PointsTable for storing pairs
//	 */
//	@SuppressWarnings("serial")
//	public class PointsTable extends ArrayList<PointsExhibitPair> {
//		private int getTotalPoints() {
//			int totalPoints = 0;
//			if (this.isEmpty()) {
//				return 0;
//			} else {
//				for (PointsExhibitPair pep : this) {
//					totalPoints += pep.points;
//				}
//			}
//			return totalPoints;
//		}
//	}
//	
//	/**
//	 * Class of "Exhibit - Points gained" pairs
//	 * 
//	 * @param String
//	 *                exhibitName
//	 * @param int points
//	 */
//	public class PointsExhibitPair {
//		public String exhibitName;
//		public int points;
//		
//		public PointsExhibitPair(String name, int p) {
//			this.exhibitName = name;
//			this.points = p;
//		}
//		
//		public void addPointsGained(int gainedPoints) {
//			points = points + gainedPoints;
//		}
//	}
//	
}
