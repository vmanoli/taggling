package hci.projects.tagglinglib.GameElements;

public class Hint {
	private static final String _NO_HINT_VALUE = "";
	public static final Hint _NO_HINT = new Hint(_NO_HINT_VALUE, HintType.NOT_AVAILABLE);
	
	public enum HintType {
		TEXT, PICTURE, VIDEO, SOUND, NOT_AVAILABLE;
	}
	
	private String value;
	private HintType type;
	
	public Hint(String value, HintType type) {
		super();
		this.value = value;
		this.type = type;
	}
	
	public String getValue() {
		return value;
	}
	
	public HintType getType() {
		return type;
	}
	
	public boolean isAvailable() {
		if (this.type == HintType.NOT_AVAILABLE) {
			return false;
		} else {
			return true;
		}
	}
}
