package hci.projects.tagglinglib.GameElements;

import java.util.ArrayList;

public class ExhibitList extends ArrayList<Exhibit> {
	private static final long serialVersionUID = -3608361250088883105L;
	
	public ExhibitList() {
		this.clear();
	}
	
	public Exhibit getExhibitById(int exhibitId) {
		for (Exhibit exhibit : this) {
			if (exhibit.getExhibitId() == exhibitId) {
				return exhibit;
			}
		}
		return Exhibit._NO_EXHIBIT;
	}
	
	/**
	 * Gets the exhibit with the specific HexCode
	 * 
	 * @return Exhibit or null if it doesn't exist
	 */
	public Exhibit getExhibitByHex(String hexCode) {
		for (Exhibit exhibit : this) {
			if (exhibit.isExhibitHexCode(hexCode)) {
				return exhibit;
			}
		}
		return null;
	}	
}
