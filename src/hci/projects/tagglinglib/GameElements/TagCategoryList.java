package hci.projects.tagglinglib.GameElements;

import java.util.ArrayList;

public class TagCategoryList extends ArrayList<TagCategory>{
	private static final long serialVersionUID = 1657234275394231740L;
	
	public TagCategoryList() {
		this.clear();
	}
}
