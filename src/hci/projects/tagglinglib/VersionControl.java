package hci.projects.tagglinglib;

import org.json.JSONObject;

public class VersionControl {
	// version numbers
	private int myCurrentVersion, serverCurrrentVersion;

	// JSON Node names - Version
	private static final String JSON_TAG_CONTROL_OBJECT = "content_version_control";
	private static final String JSON_TAG_VERSION = "version";

	public VersionControl() {
		super();
		this.myCurrentVersion = -1;
		this.serverCurrrentVersion = -1;
	}
	
	public int getMyCurrentVersion() {
		return myCurrentVersion;
	}

	public void setMyCurrentVersion(int myCurrentVersion) {
		this.myCurrentVersion = myCurrentVersion;
	}

	/**
	 * Gets the version number from the json object
	 * @param json in String representation
	 * @return int version
	 * @category json
	 */
	public void setServerVersion(String jsonString) {
		int vNum = -1;
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONObject versionControl = jsonObject
					.getJSONObject(JSON_TAG_CONTROL_OBJECT);
			vNum = versionControl.getInt(JSON_TAG_VERSION);
		} catch (Exception e) {
			e.printStackTrace();
		}
		serverCurrrentVersion = vNum;
	}
	
	public int getServerCurrrentVersion() {
		return serverCurrrentVersion;
	}

	/*
	 * Checks if the current version downloaded is "older" than
	 * the version on the server
	 * @param 
	 * @return true if client has an older version and has to download new
	 */
	public boolean check() {
		if (myCurrentVersion < serverCurrrentVersion) {
			return true;
		} else {
			return false;
		}
	}
}
