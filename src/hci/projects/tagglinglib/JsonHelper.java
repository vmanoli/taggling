package hci.projects.tagglinglib;

import hci.projects.tagglinglib.GameElements.Exhibit;
import hci.projects.tagglinglib.GameElements.ExhibitList;
import hci.projects.tagglinglib.GameElements.Hint;
import hci.projects.tagglinglib.GameElements.Tag;
import hci.projects.tagglinglib.GameElements.TagCategory;
import hci.projects.tagglinglib.GameElements.Hint.HintType;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.util.Log;

public class JsonHelper {
	private static final String DEBUG_TAG = "JsonHelper";
	
	// JSON Node names - Data Arrays
	// private static final String TAG_PLAYERS = "players";
	private static final String LABEL_EXHIBITS = "exhibits";
	private static final String LABEL_TAGLINES = "taglines";
	private static final String LABEL_CATEGORIES = "categories";
	
	// JSON Node names - Exhibit
	private static final String LABEL_ID = "id";
	private static final String LABEL_NAME = "name";
	private static final String LABEL_ARTIST = "artist";
	private static final String LABEL_THUMBNAIL = "thumbnail";
	private static final String LABEL_SLOTSIZE = "tagSlotsSize";
	private static final String LABEL_FLOOR = "floor";
	private static final String LABEL_IDENTITY= "identity";
	
	// JSON Node names - Category
	// private static final String LABEL_NAME = "name";
	private static final String LABEL_COLOR = "color";
	private static final String LABEL_DESCRIPTION = "description";
	
	// JSON Node names - Tagline
	// private static final String TAG_NAME = "name";
	private static final String LABEL_POINTS = "points";
	private static final String LABEL_VALEXHIBIS = "validExhibits";
	private static final String LABEL_CATEGORY = "category";
	private static final String LABEL_TAG_HINT = "hint";
	
	public JsonHelper() {
		super();
	}
	
	public void readExhibits(String json, GameHandler gH) {
		int eSlotSize;
		String eThumbnail;
		int eFloor;
		String eName;
		String eArtist;
		String eIdentity;
		int eId = Exhibit._NO_EXHIBIT_ID;
		
		try {
			JSONObject jsonGameObject = new JSONObject(json);
			JSONArray jsonExhibitsArray = jsonGameObject.getJSONArray(LABEL_EXHIBITS);
			
			for (int i = 0; i < jsonExhibitsArray.length(); i++) {
				JSONObject jsonExhibitObject = jsonExhibitsArray.getJSONObject(i);
				eId = jsonExhibitObject.getInt(LABEL_ID);
				eName = jsonExhibitObject.getString(LABEL_NAME);
				eArtist = jsonExhibitObject.getString(LABEL_ARTIST);
				eThumbnail = jsonExhibitObject.getString(LABEL_THUMBNAIL);
				eSlotSize = jsonExhibitObject.getInt(LABEL_SLOTSIZE);
				eFloor = jsonExhibitObject.getInt(LABEL_FLOOR);
				eIdentity = jsonExhibitObject.getString(LABEL_IDENTITY);
				
				// FIXME
				if (eName == null || eThumbnail == null || eSlotSize <= 0 || eFloor < 1 || eFloor > 3) { // floor range = 1, 2, 3
					continue;
				} else {
					GameHandler.getInstance().getGameExhibits().add(new Exhibit(eId, eName, eArtist, eThumbnail, eSlotSize, eFloor, eIdentity));
				}
				Log.d("JSON", "Exhibt OK " + eName);
			}
		} catch (Exception e) {
			Log.e(DEBUG_TAG, "Exception while reading exhibits: last Id: " + String.valueOf(eId) + ", e: " + e.toString());
		}
	}
	
	public void readTaglines(String json, GameHandler gH) {
		String tName, tHint;
		int tId = Tag._NO_TAG_ID;
		int tPoints, tCategory;
		Integer[] tValidExhibits;
		
		try {
			JSONObject jsonGameObject = new JSONObject(json);
			JSONArray jsonTaglinesArray = jsonGameObject.getJSONArray(LABEL_TAGLINES);
			
			for (int i = 0; i < jsonTaglinesArray.length(); i++) {
				
				JSONObject jsonTaglineObject = jsonTaglinesArray.getJSONObject(i);
				tId = jsonTaglineObject.getInt(LABEL_ID);
				tName = jsonTaglineObject.getString(LABEL_NAME);
				tPoints = jsonTaglineObject.getInt(LABEL_POINTS);
				tCategory = jsonTaglineObject.getInt(LABEL_CATEGORY);
				tHint = jsonTaglineObject.getString(LABEL_TAG_HINT);
				tValidExhibits = getIntArray(jsonTaglineObject, LABEL_VALEXHIBIS);
				
				Hint hint;
				
				if (tHint.isEmpty()) {
					hint = Hint._NO_HINT;
				} else {
					hint = new Hint(tHint, HintType.TEXT);
				}
				if (tValidExhibits.length == 1) {
					ArrayList<Exhibit> exhibitList = new ArrayList<Exhibit>();
					exhibitList.add(GameHandler.getInstance().getGameExhibits().getExhibitById(tValidExhibits[0]));
					TagCategory gameTagCategory = GameHandler.getInstance().getGameTagCategories().get(tCategory);// FIXME: do it by id
					GameHandler.getInstance().getGameTags().add(new Tag(tId, tName, tPoints, exhibitList, gameTagCategory, hint)); // FIXME Get the right
																			// category by Id
				} else {
					// Use constructor with list of exhibits
					ExhibitList tempExhibitsList = new ExhibitList();
					for (int j : tValidExhibits) {
						tempExhibitsList.add(GameHandler.getInstance().getGameExhibits().getExhibitById(j));
					}
				}
				
				Log.d("JSON", "Tagline OK" + tName);
			}
		} catch (Exception e) {
			Log.e(DEBUG_TAG, "Exception while reading tags: last Id: " + String.valueOf(tId) + ", e: " + e.toString());
		}
	}
	
	public void readCategories(String json, GameHandler gH) {
		String cName = "", cColor, cDescription;
		
		try {
			JSONObject jsonGameObject = new JSONObject(json);
			JSONArray jsonCategoriesArray = jsonGameObject.getJSONArray(LABEL_CATEGORIES);
			
			for (int i = 0; i < jsonCategoriesArray.length(); i++) {
				
				JSONObject jsonCategoryObject = jsonCategoriesArray.getJSONObject(i);
				cName = jsonCategoryObject.getString(LABEL_NAME);
				cColor = jsonCategoryObject.getString(LABEL_COLOR);
				cDescription = jsonCategoryObject.getString(LABEL_DESCRIPTION);
				
				GameHandler.getInstance().getGameTagCategories().add(new TagCategory(cName, Color.BLACK, cDescription));
				Log.d("JSON", "Category OK" + cName);
			}
		} catch (Exception e) {
			Log.e(DEBUG_TAG, "Exception while reading tags: last category: " + cName + ", e: " + e.toString());
		}
	}
	
	public void append(String json, GameHandler gH) {
		// clear game exhibits list if any now
		// gH.clear();
		// in this particular order
		readExhibits(json, gH);
		readCategories(json, gH);
		readTaglines(json, gH);
	}
	
	public Integer[] getIntArray(JSONObject jsonObj, String tag) throws JSONException {
		ArrayList<Integer> al = new ArrayList<Integer>();
		// int[] intArray = {};
		JSONArray jArray = jsonObj.getJSONArray(tag);
		for (int k = 0; k < jArray.length(); k++) {
			al.add(k, jArray.getInt(k));
		}
		
		return al.toArray(new Integer[0]);
	}
	
}
