package hci.projects.tagglinglib.Messages;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Messages {
	
	// Server sending
		public static final byte DECLARE_MASTER_ID = 1;
		public static final byte INITIALIZATION_OBJECT = 3;
		public static final byte BEGIN_GAME_ACTIVITY = 5;
		public static final byte GAME_OVER_ALL = 11;
		
		// Client sending
		public static final byte ADD_ME_TO_PLAYER_LIST = 2;
		public static final byte READY_TO_BEGIN = 4;
		public static final byte LOCKED_EXHIBIT = 6;
		public static final byte UPDATING_EXHIBIT = 8;
		public static final byte GUESSING_GAME_OVER = 10;
		
		/**
		 * For debugging purposes
		 * @param flag
		 * @return the literal representation of the message byte
		 */
		public static String toString(byte flag) {
			switch (flag) {
			case 1: return "DECLARE_MASTER_ID";
			case 3: return "INITIAL_JSON_OBJECT";
			case 5: return "BEGIN_GAME_ACTIVITY";
			case 11: return "GAME_OVER_ALL";
			case 2: return "ADD_ME_TO_PLAYER_LIST";
			case 4: return "READY_TO_BEGIN";
			case 6: return "LOCKED_EXHIBIT";
			case 8: return "UPDATING_EXHIBIT";
			case 10: return "GUESSING_GAME_OVER";
			}
			return "NotValidFlag";
		}
		
		
	private static final String BUNDLE_NAME = "hci.projects.taggling.messages"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private Messages() {
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
