package hci.projects.tagglinglib.Messages;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/***
 * Represents a game message message is in the format playerid@objectid
 * 
 * @author viky
 */
public class MessageExhibitLock implements MessageTaggling, Parcelable {
	private static final String TAG = "LockMessage";
	
	private static final String LABEL_EXHIBITLOCK_PLAYERID = "playerId";
	private static final String LABEL_EXHIBITLOCK_OBJECTID = "objectId";
	private static final String LABEL_EXHIBITLOCK_ACTION = "action";
	private static final String LABEL_EXHIBITLOCK_SESSIONID = "sessionId";
	
	private int _fromPlayerId;
	private int _objectId;
	private int _sessionId = 0;
	public static final String PLAYER_SEPARATOR = "@";
	private final MessageType _messageType = MessageType.EXHIBIT_LOCK;
	
	/**
	 * Create a Message from a JSON array string
	 * 
	 * @param JSONString
	 */
	public MessageExhibitLock(JSONObject jsonObject) {
		try {
			_fromPlayerId = jsonObject.getInt(LABEL_EXHIBITLOCK_PLAYERID);
			_objectId = jsonObject.getInt(LABEL_EXHIBITLOCK_OBJECTID);
		} catch (JSONException e) {
			Log.e(TAG, "Couldn't parse JSON string " + e.toString());
		}
	}
	
	public MessageExhibitLock(String message) {
		String[] splitMsg = message.split(PLAYER_SEPARATOR);
		_fromPlayerId = Integer.valueOf(splitMsg[0]);
		_objectId = Integer.valueOf(splitMsg[1]);
	}
	
	/***
	 * Create a Message
	 * 
	 * @param fromPlayerId
	 * @param objectId
	 */
	public MessageExhibitLock(int fromPlayerId, int objectId) {
		_fromPlayerId = fromPlayerId;
		_objectId = objectId;
	}
	
	public int getObjectId() {
		return _objectId;
	}
	
	public int getPlayerId() {
		return _fromPlayerId;
	}
	
	public MessageExhibitLock(Parcel parcel) {
		_fromPlayerId = parcel.readInt();
		_objectId = parcel.readInt();
	}
	
	public static final Parcelable.Creator<MessageExhibitLock> CREATOR = new Parcelable.Creator<MessageExhibitLock>() {
		public MessageExhibitLock createFromParcel(Parcel in) {
			return new MessageExhibitLock(in);
		}
		
		public MessageExhibitLock[] newArray(int size) {
			return new MessageExhibitLock[size];
		}
	};
	
	@Override
	public int describeContents() {
		return toString().hashCode();
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(_fromPlayerId);
		dest.writeInt(_objectId);
	}
	
	/**
	 * @return playerName@MESSAGE_TYPE@exhibitId
	 */
	public String toString() {
		JSONObject object = new JSONObject();
		try {
			object.put(LABEL_EXHIBITLOCK_PLAYERID, _fromPlayerId);
			object.put(LABEL_EXHIBITLOCK_ACTION, String.valueOf(this._messageType));
			object.put(LABEL_EXHIBITLOCK_OBJECTID, String.valueOf(_objectId));
			object.put(LABEL_EXHIBITLOCK_SESSIONID, String.valueOf(_sessionId));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		System.out.println(object);
		return object.toString();
		// return _fromPlayerId + TGConstants._CSMESSAGE_DELIMETER + String.valueOf(this._messageType) + TGConstants._CSMESSAGE_DELIMETER
		// + String.valueOf(_objectId);
	}
	
	@Override
	public MessageType getMessageType() {
		return this._messageType;
	}
	
	@Override
	public int getMessageKey() {
		return this.getObjectId();
	}
	
	@Override
	public int getSessionId() {
		return _sessionId;
	}
}
