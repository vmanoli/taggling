package hci.projects.tagglinglib.Messages;

import java.util.HashMap;
import java.util.Map;
import android.annotation.SuppressLint;
import android.os.Parcelable;

/**
 * @author chris
 *
 */
public interface MessageTaggling extends Parcelable {
	
	public static final String PLAYER_SEPARATOR = "@";
	public static final String TAGS_SEPARATOR = ":";
	public static final String COMMA = ",";
	
	public static final String LABEL_MSG_PLAYERID = "playerId";
	public static final String LABEL_MSGK_OBJECTID = "objectId";
	public static final String LABEL_MSG_ACTION = "action";
	public static final String LABEL_MSG_SESSIONID = "sessionId";
	
	public static final String LABEL_MSG_NEW_TAGIDS_IN_EXHIBIT = "newIds";
	public static final String LABEL_MSG_NEW_TAGIDS_IN_INVENTORY = "newIdsInInventory";
	
	public static final String LABEL_MSG_INIT_TAGIDS_IN_EXHIBIT = "newExhibitIds";
	public static final String LABEL_MSG_INIT_TAGIDS_IN_INVENTORY = "newIdsInInventory";
	public static final String LABEL_NEW_TOTAL_POINTS_FOR_PLAYER =  "newTotalPointsForPlayer";
	
	
	
	public enum MessageType {
		EXHIBIT_LOCK, EXHIBIT_UNLOCK, EXHIBIT_UPDATE;
		@SuppressLint("UseSparseArrays")
		static Map<Integer, MessageType> intMap = new HashMap<Integer, MessageType>(2);
		static {
			for (MessageType e : MessageType.values()) {
				intMap.put(e.ordinal(), e);
			}
		}
		
		public static MessageType intValue(int key) {
			return intMap.get(key);
		}
	}
	
	String toString();
	
	int getPlayerId();
	int getSessionId();
	
	/**
	 * The messageKey is used to differentiate between messages in the {@link MessageQueue}
	 * @return
	 */
	int getMessageKey();
	
	MessageType getMessageType();
}
