package hci.projects.tagglinglib.Messages;

import org.json.JSONArray;
import org.json.JSONException;

import android.os.Parcel;
import android.os.Parcelable;

public class PointsMessage  implements Parcelable {
	

	
	
	public JSONArray getJSONArray(Integer[] intArray) throws JSONException {
		
		JSONArray jArray = new JSONArray();
		for (int k = 0; k < intArray.length; k++) {
			jArray.put(intArray[k]);
		}
		
		return jArray;
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
	}
	
}
