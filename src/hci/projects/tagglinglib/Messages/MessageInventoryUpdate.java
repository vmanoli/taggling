package hci.projects.tagglinglib.Messages;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class MessageInventoryUpdate implements Parcelable {
	private int _id = 0;
	private String _fromPlayerId;
	private String _fromTeamId;
	
	public static final String TAGS_SEPARATOR = ":";
	public static final String COMMA = ",";
	
	String inventoryOwner;
	Integer[] newInventoryTagIndexes; // the indexes(by gameTags) of the tags in the inventory
	String message; // the raw message string
	
	public MessageInventoryUpdate(String message) {
		this.message = message;
		
		String[] strArr = message.split(TAGS_SEPARATOR);
		inventoryOwner = strArr[0];
		
		String[] tagsArr = strArr[1].split(COMMA);
		Integer[] indxs = new Integer[tagsArr.length];
		int i=0;
		for (String s : tagsArr) {
			indxs[i] = Integer.valueOf(s);
			i++;
		}
		newInventoryTagIndexes = indxs;
	}
	
	public MessageInventoryUpdate(Parcel in) {
		this(in.readString());
	}
	
	public MessageInventoryUpdate(JSONObject jsonObject) {
		try {
			_id = jsonObject.getInt("messageId");
			_fromPlayerId = jsonObject.getString("playerId");
			newInventoryTagIndexes = getIntArray(jsonObject, "newIdsInInventory");
			
		} catch (JSONException e) {
			Log.e("MessageInventoryUpdate", "Couldn't parse JSON string " + e.toString());
		}
		
	}
	
	public MessageInventoryUpdate(String owner, Integer[] tagIndx) {
		this.inventoryOwner = owner;
		this.newInventoryTagIndexes = tagIndx;
	}
	
	public String toString() {
		if (inventoryOwner == null || newInventoryTagIndexes == null) { return null; }
		
		StringBuilder sb = new StringBuilder();
		sb.append(inventoryOwner);
		sb.append(TAGS_SEPARATOR);
		for (int index : newInventoryTagIndexes) {
			sb.append(index);
			sb.append(COMMA);
		}
		sb.deleteCharAt(sb.length()-1); // delete last useless comma
		
		return sb.toString();
	}
	
	public Integer[] getIntArray(JSONObject jsonObj, String tag) throws JSONException {
		ArrayList<Integer> al = new ArrayList<Integer>();
		// int[] intArray = {};
		JSONArray jArray = jsonObj.getJSONArray(tag);
		for (int k = 0; k < jArray.length(); k++) {
			al.add(k, jArray.getInt(k));
		}
		
		return al.toArray(new Integer[0]);
	}
	
	public String getOwner() {
		return inventoryOwner;
	}
	
	public Integer[] getTagIndexes() {
		return newInventoryTagIndexes;

	}

	public static final Parcelable.Creator<MessageInventoryUpdate> CREATOR = new Parcelable.Creator<MessageInventoryUpdate>() {
		public MessageInventoryUpdate createFromParcel(Parcel in) {
			return new MessageInventoryUpdate(in);
		}
		
		public MessageInventoryUpdate[] newArray(int size) {
			return new MessageInventoryUpdate[size];
		}
	};
	
	@Override
	public int describeContents() {
		return this.toString().hashCode();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.toString());
		
	}

}
